/***************************************************************
 * Project: DDrawWrap
 * File: winclude.h
 * Copyright � learn_more
 */

#ifndef __WINCLUDE_H__
#define __WINCLUDE_H__

#include "config.h"
#include <windows.h>
#include <ddraw.h>
#include <stdio.h>
void __cdecl add_log( char* arg, ... );
void __cdecl warning( char* arg, ... );
char* VA( char* file, int line, char* arg, ... );

#define va(...)		VA( __FILE__, __LINE__, __VA_ARGS__ )

void SetMinidumpFilter( HMODULE hMod );

#define PRINT_DEV		this

void Device_HookVMT( IDirectDraw* ptr );
void Device_HookVMT( IDirectDraw2* ptr );
void Device_HookVMT( IDirectDraw4* ptr );
void Device_HookVMT( IDirectDraw7* ptr );

void Device_HookVMT( IDirectDrawSurface* ptr );
void Device_HookVMT( IDirectDrawSurface2* ptr );
void Device_HookVMT( IDirectDrawSurface3* ptr );
void Device_HookVMT( IDirectDrawSurface4* ptr );
void Device_HookVMT( IDirectDrawSurface7* ptr );

void Device_HookVMT( IDirectDrawPalette* ptr );
void Device_HookVMT( IDirectDrawClipper* ptr );
void Device_HookVMT( IDirectDrawColorControl* ptr );
void Device_HookVMT( IDirectDrawGammaControl* ptr );


void init_ddhooks();


#define HOOK_VTABLE(n,i)	HOOK_VTABLE2(n,i,HOOK_PREFIX)
#define HOOK_VTABLE2(n,i,h)	HOOK_VTABLE3(n,i,h)
#define HOOK_VTABLE3(n,i,h)	\
	if( h##_vtable[i] != (VFPTR)&h::n ) {	\
	*(VFPTR*)&o##n = (VFPTR)h##_vtable[i]; \
	h##_vtable[i] = (VFPTR)&h::n; \
	if( !first ) warning( "Rehooking: "#h"::"#n );	\
}

#define SAVE_STATE	DWORD lastErr = GetLastError(); \
	__asm pushad \
	__asm pushfd 

#define RESTORE_STATE	SetLastError( lastErr );\
		__asm popfd \
		__asm popad


void FormatGUID( REFIID iid, char* buf, size_t bufsize );
void FormatCooperativeFlags( DWORD dwFlags, char* buf, size_t bufsize );
void FormatLockFlags( DWORD dwFlags, char* buf, size_t bufsize );
void FormatColorkeyFlags( DWORD dwFlags, char* buf, size_t bufsize );
void FormatSurfaceDesc( LPDDSURFACEDESC lpSurf, char* buf, size_t bufsize );
void FormatSurfaceDesc2( LPDDSURFACEDESC2 lpSurf, char* buf, size_t bufsize );
void LogCaps( LPVOID obj, char* devname, HRESULT hRet, LPDDCAPS lpDDDriverCaps, LPDDCAPS lpDDHELCaps );



#ifdef LOG_ALL_CALLS
#define ADD_LOG(...) add_log( __VA_ARGS__ )
#else
#define ADD_LOG(...)
#endif


#endif //__WINCLUDE_H__