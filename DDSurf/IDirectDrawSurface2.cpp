/***************************************************************
 * Project: DDrawWrap
 * File: IDirectDrawSurface2.cpp
 * Copyright � learn_more
 */

#include "../winclude.h"

class CDirectDrawSurface2 {
public:
/*** IUnknown methods ***/
HRESULT __stdcall QueryInterface( REFIID riid, LPVOID* ppvObj );
ULONG __stdcall AddRef();
ULONG __stdcall Release();
/*** IDirectDraw methods ***/
HRESULT __stdcall AddAttachedSurface( LPDIRECTDRAWSURFACE2 lpDDSAttachedSurface );
HRESULT __stdcall AddOverlayDirtyRect( LPRECT lpRect );
HRESULT __stdcall Blt( LPRECT lpDestRect, LPDIRECTDRAWSURFACE2 lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwFlags, LPDDBLTFX lpDDBltFx );
HRESULT __stdcall BltBatch( LPDDBLTBATCH lpDDBltBatch, DWORD dwCount, DWORD dwFlags );
HRESULT __stdcall BltFast( DWORD dwX, DWORD dwY, LPDIRECTDRAWSURFACE2 lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwTrans );
HRESULT __stdcall DeleteAttachedSurface( DWORD dwFlags, LPDIRECTDRAWSURFACE2 lpDDSAttachedSurface );
HRESULT __stdcall EnumAttachedSurfaces( LPVOID lpContext, LPDDENUMSURFACESCALLBACK lpEnumSurfacesCallback );
HRESULT __stdcall EnumOverlayZOrders( DWORD dwFlags, LPVOID lpContext, LPDDENUMSURFACESCALLBACK lpfnCallback );
HRESULT __stdcall Flip( LPDIRECTDRAWSURFACE2 lpDDSurfaceTargetOverride, DWORD dwFlags );
HRESULT __stdcall GetAttachedSurface( LPDDSCAPS lpDDSCaps, LPDIRECTDRAWSURFACE2* lplpDDAttachedSurface );
HRESULT __stdcall GetBltStatus( DWORD dwFlags );
HRESULT __stdcall GetCaps( LPDDSCAPS lpDDSCaps );
HRESULT __stdcall GetClipper( LPDIRECTDRAWCLIPPER* lplpDDClipper );
HRESULT __stdcall GetColorKey( DWORD dwFlags, LPDDCOLORKEY lpDDColorKey );
HRESULT __stdcall GetDC( HDC* lphDC );
HRESULT __stdcall GetFlipStatus( DWORD dwFlags );
HRESULT __stdcall GetOverlayPosition( LPLONG lplX, LPLONG lplY );
HRESULT __stdcall GetPalette( LPDIRECTDRAWPALETTE* lplpDDPalette );
HRESULT __stdcall GetPixelFormat( LPDDPIXELFORMAT lpDDPixelFormat );
HRESULT __stdcall GetSurfaceDesc( LPDDSURFACEDESC lpDDSurfaceDesc );
HRESULT __stdcall Initialize( LPDIRECTDRAW lpDD, LPDDSURFACEDESC lpDDSurfaceDesc );
HRESULT __stdcall IsLost();
HRESULT __stdcall Lock( LPRECT lpDestRect, LPDDSURFACEDESC lpDDSurfaceDesc, DWORD dwFlags, HANDLE hEvent );
HRESULT __stdcall ReleaseDC( HDC hDC );
HRESULT __stdcall Restore();
HRESULT __stdcall SetClipper( LPDIRECTDRAWCLIPPER lpDDClipper );
HRESULT __stdcall SetColorKey( DWORD dwFlags, LPDDCOLORKEY lpDDColorKey );
HRESULT __stdcall SetOverlayPosition( LONG lX, LONG lY );
HRESULT __stdcall SetPalette( LPDIRECTDRAWPALETTE lpDDPalette );
HRESULT __stdcall Unlock( LPVOID lpRect );
HRESULT __stdcall UpdateOverlay( LPRECT lpSrcRect, LPDIRECTDRAWSURFACE2 lpDDDestSurface, LPRECT lpDestRect, DWORD dwFlags, LPDDOVERLAYFX lpDDOverlayFx );
HRESULT __stdcall UpdateOverlayDisplay( DWORD dwFlags );
HRESULT __stdcall UpdateOverlayZOrder( DWORD dwFlags, LPDIRECTDRAWSURFACE2 lpDDSReference );
/*** Added in the V2 Interface ***/
HRESULT __stdcall GetDDInterface( LPVOID* lplpDD );
HRESULT __stdcall PageLock( DWORD dwFlags );
HRESULT __stdcall PageUnlock( DWORD dwFlags );
};


/*** IUnknown methods ***/
HRESULT (__stdcall CDirectDrawSurface2::*oQueryInterface)( REFIID riid, LPVOID* ppvObj );
ULONG (__stdcall CDirectDrawSurface2::*oAddRef)();
ULONG (__stdcall CDirectDrawSurface2::*oRelease)();
/*** IDirectDraw methods ***/
HRESULT (__stdcall CDirectDrawSurface2::*oAddAttachedSurface)( LPDIRECTDRAWSURFACE2 lpDDSAttachedSurface );
HRESULT (__stdcall CDirectDrawSurface2::*oAddOverlayDirtyRect)( LPRECT lpRect );
HRESULT (__stdcall CDirectDrawSurface2::*oBlt)( LPRECT lpDestRect, LPDIRECTDRAWSURFACE2 lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwFlags, LPDDBLTFX lpDDBltFx );
HRESULT (__stdcall CDirectDrawSurface2::*oBltBatch)( LPDDBLTBATCH lpDDBltBatch, DWORD dwCount, DWORD dwFlags );
HRESULT (__stdcall CDirectDrawSurface2::*oBltFast)( DWORD dwX, DWORD dwY, LPDIRECTDRAWSURFACE2 lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwTrans );
HRESULT (__stdcall CDirectDrawSurface2::*oDeleteAttachedSurface)( DWORD dwFlags, LPDIRECTDRAWSURFACE2 lpDDSAttachedSurface );
HRESULT (__stdcall CDirectDrawSurface2::*oEnumAttachedSurfaces)( LPVOID lpContext, LPDDENUMSURFACESCALLBACK lpEnumSurfacesCallback );
HRESULT (__stdcall CDirectDrawSurface2::*oEnumOverlayZOrders)( DWORD dwFlags, LPVOID lpContext, LPDDENUMSURFACESCALLBACK lpfnCallback );
HRESULT (__stdcall CDirectDrawSurface2::*oFlip)( LPDIRECTDRAWSURFACE2 lpDDSurfaceTargetOverride, DWORD dwFlags );
HRESULT (__stdcall CDirectDrawSurface2::*oGetAttachedSurface)( LPDDSCAPS lpDDSCaps, LPDIRECTDRAWSURFACE2* lplpDDAttachedSurface );
HRESULT (__stdcall CDirectDrawSurface2::*oGetBltStatus)( DWORD dwFlags );
HRESULT (__stdcall CDirectDrawSurface2::*oGetCaps)( LPDDSCAPS lpDDSCaps );
HRESULT (__stdcall CDirectDrawSurface2::*oGetClipper)( LPDIRECTDRAWCLIPPER* lplpDDClipper );
HRESULT (__stdcall CDirectDrawSurface2::*oGetColorKey)( DWORD dwFlags, LPDDCOLORKEY lpDDColorKey );
HRESULT (__stdcall CDirectDrawSurface2::*oGetDC)( HDC* lphDC );
HRESULT (__stdcall CDirectDrawSurface2::*oGetFlipStatus)( DWORD dwFlags );
HRESULT (__stdcall CDirectDrawSurface2::*oGetOverlayPosition)( LPLONG lplX, LPLONG lplY );
HRESULT (__stdcall CDirectDrawSurface2::*oGetPalette)( LPDIRECTDRAWPALETTE* lplpDDPalette );
HRESULT (__stdcall CDirectDrawSurface2::*oGetPixelFormat)( LPDDPIXELFORMAT lpDDPixelFormat );
HRESULT (__stdcall CDirectDrawSurface2::*oGetSurfaceDesc)( LPDDSURFACEDESC lpDDSurfaceDesc );
HRESULT (__stdcall CDirectDrawSurface2::*oInitialize)( LPDIRECTDRAW lpDD, LPDDSURFACEDESC lpDDSurfaceDesc );
HRESULT (__stdcall CDirectDrawSurface2::*oIsLost)();
HRESULT (__stdcall CDirectDrawSurface2::*oLock)( LPRECT lpDestRect, LPDDSURFACEDESC lpDDSurfaceDesc, DWORD dwFlags, HANDLE hEvent );
HRESULT (__stdcall CDirectDrawSurface2::*oReleaseDC)( HDC hDC );
HRESULT (__stdcall CDirectDrawSurface2::*oRestore)();
HRESULT (__stdcall CDirectDrawSurface2::*oSetClipper)( LPDIRECTDRAWCLIPPER lpDDClipper );
HRESULT (__stdcall CDirectDrawSurface2::*oSetColorKey)( DWORD dwFlags, LPDDCOLORKEY lpDDColorKey );
HRESULT (__stdcall CDirectDrawSurface2::*oSetOverlayPosition)( LONG lX, LONG lY );
HRESULT (__stdcall CDirectDrawSurface2::*oSetPalette)( LPDIRECTDRAWPALETTE lpDDPalette );
HRESULT (__stdcall CDirectDrawSurface2::*oUnlock)( LPVOID lpRect );
HRESULT (__stdcall CDirectDrawSurface2::*oUpdateOverlay)( LPRECT lpSrcRect, LPDIRECTDRAWSURFACE2 lpDDDestSurface, LPRECT lpDestRect, DWORD dwFlags, LPDDOVERLAYFX lpDDOverlayFx );
HRESULT (__stdcall CDirectDrawSurface2::*oUpdateOverlayDisplay)( DWORD dwFlags );
HRESULT (__stdcall CDirectDrawSurface2::*oUpdateOverlayZOrder)( DWORD dwFlags, LPDIRECTDRAWSURFACE2 lpDDSReference );
/*** Added in the V2 Interface ***/
HRESULT (__stdcall CDirectDrawSurface2::*oGetDDInterface)( LPVOID* lplpDD );
HRESULT (__stdcall CDirectDrawSurface2::*oPageLock)( DWORD dwFlags );
HRESULT (__stdcall CDirectDrawSurface2::*oPageUnlock)( DWORD dwFlags );


typedef void (__stdcall CDirectDrawSurface2::*VFPTR)();
VFPTR* CDirectDrawSurface2_vtable = 0;

#define HOOK_PREFIX	CDirectDrawSurface2



HRESULT CDirectDrawSurface2::QueryInterface( REFIID riid, LPVOID* ppvObj )
{
	char buf[100];
	FormatGUID( riid, buf, sizeof(buf) );
	ADD_LOG( "IDirectDrawSurface2(%p)::QueryInterface( %s )", PRINT_DEV, buf );
	return (this->*oQueryInterface)( riid, ppvObj );
}

ULONG CDirectDrawSurface2::AddRef()
{
	ULONG pRef = (this->*oAddRef)();
	ADD_LOG( "IDirectDrawSurface2(%p)::AddRef( %i )", PRINT_DEV, pRef );
	return pRef;
}

ULONG CDirectDrawSurface2::Release()
{
	ULONG uRet = (this->*oRelease)();
	ADD_LOG( "IDirectDrawSurface2(%p)::Release( %i )", PRINT_DEV, uRet );
	return uRet;
}

HRESULT CDirectDrawSurface2::AddAttachedSurface( LPDIRECTDRAWSURFACE2 lpDDSAttachedSurface )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::AddAttachedSurface()", PRINT_DEV );
	return (this->*oAddAttachedSurface)( lpDDSAttachedSurface );
}

HRESULT CDirectDrawSurface2::AddOverlayDirtyRect( LPRECT lpRect )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::AddOverlayDirtyRect()", PRINT_DEV );
	return (this->*oAddOverlayDirtyRect)( lpRect );
}

HRESULT CDirectDrawSurface2::Blt( LPRECT lpDestRect, LPDIRECTDRAWSURFACE2 lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwFlags, LPDDBLTFX lpDDBltFx )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::Blt()", PRINT_DEV );
	return (this->*oBlt)( lpDestRect, lpDDSrcSurface, lpSrcRect, dwFlags, lpDDBltFx );
}

HRESULT CDirectDrawSurface2::BltBatch( LPDDBLTBATCH lpDDBltBatch, DWORD dwCount, DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::BltBatch()", PRINT_DEV );
	return (this->*oBltBatch)( lpDDBltBatch, dwCount, dwFlags );
}

HRESULT CDirectDrawSurface2::BltFast( DWORD dwX, DWORD dwY, LPDIRECTDRAWSURFACE2 lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwTrans )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::BltFast()", PRINT_DEV );
	return (this->*oBltFast)( dwX, dwY, lpDDSrcSurface, lpSrcRect, dwTrans );
}

HRESULT CDirectDrawSurface2::DeleteAttachedSurface( DWORD dwFlags, LPDIRECTDRAWSURFACE2 lpDDSAttachedSurface )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::DeleteAttachedSurface()", PRINT_DEV );
	return (this->*oDeleteAttachedSurface)( dwFlags, lpDDSAttachedSurface );
}

HRESULT CDirectDrawSurface2::EnumAttachedSurfaces( LPVOID lpContext, LPDDENUMSURFACESCALLBACK lpEnumSurfacesCallback )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::EnumAttachedSurfaces()", PRINT_DEV );
	return (this->*oEnumAttachedSurfaces)( lpContext, lpEnumSurfacesCallback );
}

HRESULT CDirectDrawSurface2::EnumOverlayZOrders( DWORD dwFlags, LPVOID lpContext, LPDDENUMSURFACESCALLBACK lpfnCallback )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::EnumOverlayZOrders()", PRINT_DEV );
	return (this->*oEnumOverlayZOrders)( dwFlags, lpContext, lpfnCallback );
}

HRESULT CDirectDrawSurface2::Flip( LPDIRECTDRAWSURFACE2 lpDDSurfaceTargetOverride, DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::Flip()", PRINT_DEV );
	return (this->*oFlip)( lpDDSurfaceTargetOverride, dwFlags );
}

HRESULT CDirectDrawSurface2::GetAttachedSurface( LPDDSCAPS lpDDSCaps, LPDIRECTDRAWSURFACE2* lplpDDAttachedSurface )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::GetAttachedSurface()", PRINT_DEV );
	return (this->*oGetAttachedSurface)( lpDDSCaps, lplpDDAttachedSurface );
}

HRESULT CDirectDrawSurface2::GetBltStatus( DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::GetBltStatus()", PRINT_DEV );
	return (this->*oGetBltStatus)( dwFlags );
}

HRESULT CDirectDrawSurface2::GetCaps( LPDDSCAPS lpDDSCaps )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::GetCaps()", PRINT_DEV );
	return (this->*oGetCaps)( lpDDSCaps );
}

HRESULT CDirectDrawSurface2::GetClipper( LPDIRECTDRAWCLIPPER* lplpDDClipper )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::GetClipper()", PRINT_DEV );
	return (this->*oGetClipper)( lplpDDClipper );
}

HRESULT CDirectDrawSurface2::GetColorKey( DWORD dwFlags, LPDDCOLORKEY lpDDColorKey )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::GetColorKey()", PRINT_DEV );
	return (this->*oGetColorKey)( dwFlags, lpDDColorKey );
}

HRESULT CDirectDrawSurface2::GetDC( HDC* lphDC )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::GetDC()", PRINT_DEV );
	return (this->*oGetDC)( lphDC );
}

HRESULT CDirectDrawSurface2::GetFlipStatus( DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::GetFlipStatus()", PRINT_DEV );
	return (this->*oGetFlipStatus)( dwFlags );
}

HRESULT CDirectDrawSurface2::GetOverlayPosition( LPLONG lplX, LPLONG lplY )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::GetOverlayPosition()", PRINT_DEV );
	return (this->*oGetOverlayPosition)( lplX, lplY );
}

HRESULT CDirectDrawSurface2::GetPalette( LPDIRECTDRAWPALETTE* lplpDDPalette )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::GetPalette()", PRINT_DEV );
	return (this->*oGetPalette)( lplpDDPalette );
}

HRESULT CDirectDrawSurface2::GetPixelFormat( LPDDPIXELFORMAT lpDDPixelFormat )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::GetPixelFormat()", PRINT_DEV );
	return (this->*oGetPixelFormat)( lpDDPixelFormat );
}

HRESULT CDirectDrawSurface2::GetSurfaceDesc( LPDDSURFACEDESC lpDDSurfaceDesc )
{
	HRESULT hRet = (this->*oGetSurfaceDesc)( lpDDSurfaceDesc );
	char szSurfDesc[1024];
	FormatSurfaceDesc( lpDDSurfaceDesc, szSurfDesc, sizeof(szSurfDesc) );
	ADD_LOG( "IDirectDrawSurface2(%p)::GetSurfaceDesc( %s ) = 0x%x", PRINT_DEV, szSurfDesc, hRet );
	return hRet;
}

HRESULT CDirectDrawSurface2::Initialize( LPDIRECTDRAW lpDD, LPDDSURFACEDESC lpDDSurfaceDesc )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::Initialize()", PRINT_DEV );
	return (this->*oInitialize)( lpDD, lpDDSurfaceDesc );
}

HRESULT CDirectDrawSurface2::IsLost()
{
	ADD_LOG( "IDirectDrawSurface2(%p)::IsLost()", PRINT_DEV );
	return (this->*oIsLost)();
}

HRESULT CDirectDrawSurface2::Lock( LPRECT lpDestRect, LPDDSURFACEDESC lpDDSurfaceDesc, DWORD dwFlags, HANDLE hEvent )
{
	char* pszRect = "{entire surface}";
	char pszFlags[512] = {0};
	if( lpDestRect ) {
		pszRect = va( "{%i,%i,%i,%i}", lpDestRect->left, lpDestRect->top, lpDestRect->right, lpDestRect->bottom );
	}
	if( dwFlags ) {
		FormatLockFlags( dwFlags, pszFlags, sizeof(pszFlags) );
	}
	ADD_LOG( "IDirectDrawSurface2(%p)::Lock( %s, %s )", PRINT_DEV, pszRect, pszFlags );
	return (this->*oLock)( lpDestRect, lpDDSurfaceDesc, dwFlags, hEvent );
}

HRESULT CDirectDrawSurface2::ReleaseDC( HDC hDC )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::ReleaseDC()", PRINT_DEV );
	return (this->*oReleaseDC)( hDC );
}

HRESULT CDirectDrawSurface2::Restore()
{
	ADD_LOG( "IDirectDrawSurface2(%p)::Restore()", PRINT_DEV );
	return (this->*oRestore)();
}

HRESULT CDirectDrawSurface2::SetClipper( LPDIRECTDRAWCLIPPER lpDDClipper )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::SetClipper()", PRINT_DEV );
	return (this->*oSetClipper)( lpDDClipper );
}

HRESULT CDirectDrawSurface2::SetColorKey( DWORD dwFlags, LPDDCOLORKEY lpDDColorKey )
{
	char pszColorFlag[512] = {0};
	char* pszColor = "{0}";
	if( lpDDColorKey ) {
		pszColor = va( "{0x%x,0x%x}", lpDDColorKey->dwColorSpaceLowValue, lpDDColorKey->dwColorSpaceHighValue );
	}
	FormatColorkeyFlags( dwFlags, pszColorFlag, sizeof(pszColorFlag) );
	ADD_LOG( "IDirectDrawSurface2(%p)::SetColorKey( %s, %s )", PRINT_DEV, pszColorFlag, pszColor );
	return (this->*oSetColorKey)( dwFlags, lpDDColorKey );
}

HRESULT CDirectDrawSurface2::SetOverlayPosition( LONG lX, LONG lY )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::SetOverlayPosition()", PRINT_DEV );
	return (this->*oSetOverlayPosition)( lX, lY );
}

HRESULT CDirectDrawSurface2::SetPalette( LPDIRECTDRAWPALETTE lpDDPalette )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::SetPalette()", PRINT_DEV );
	return (this->*oSetPalette)( lpDDPalette );
}

HRESULT CDirectDrawSurface2::Unlock( LPVOID lpVoid )
{
	LPRECT lpRect = (LPRECT)lpVoid;
	char* pszRect = "{entire surface}";
//	char pszFlags[512] = {0};
	if( lpRect ) {
		pszRect = va( "{%i,%i,%i,%i}", lpRect->left, lpRect->top, lpRect->right, lpRect->bottom );
	}
	ADD_LOG( "IDirectDrawSurface2(%p)::Unlock( %s )", PRINT_DEV, pszRect );
	return (this->*oUnlock)( lpRect );
}

HRESULT CDirectDrawSurface2::UpdateOverlay( LPRECT lpSrcRect, LPDIRECTDRAWSURFACE2 lpDDDestSurface, LPRECT lpDestRect, DWORD dwFlags, LPDDOVERLAYFX lpDDOverlayFx )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::UpdateOverlay()", PRINT_DEV );
	return (this->*oUpdateOverlay)( lpSrcRect, lpDDDestSurface, lpDestRect, dwFlags, lpDDOverlayFx );
}

HRESULT CDirectDrawSurface2::UpdateOverlayDisplay( DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::UpdateOverlayDisplay()", PRINT_DEV );
	return (this->*oUpdateOverlayDisplay)( dwFlags );
}

HRESULT CDirectDrawSurface2::UpdateOverlayZOrder( DWORD dwFlags, LPDIRECTDRAWSURFACE2 lpDDSReference )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::UpdateOverlayZOrder()", PRINT_DEV );
	return (this->*oUpdateOverlayZOrder)( dwFlags, lpDDSReference );
}

HRESULT CDirectDrawSurface2::GetDDInterface( LPVOID* lplpDD )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::GetDDInterface()", PRINT_DEV );
	return (this->*oGetDDInterface)( lplpDD );
}

HRESULT CDirectDrawSurface2::PageLock( DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::PageLock()", PRINT_DEV );
	return (this->*oPageLock)( dwFlags );
}

HRESULT CDirectDrawSurface2::PageUnlock( DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface2(%p)::PageUnlock()", PRINT_DEV );
	return (this->*oPageUnlock)( dwFlags );
}


void Device_HookVMTSurf2( bool first )
{
	DWORD dwOld = 0;
	VirtualProtect( CDirectDrawSurface2_vtable, 39*4, PAGE_EXECUTE_READWRITE, &dwOld );

	HOOK_VTABLE(QueryInterface,0);
	HOOK_VTABLE(AddRef,1);
	HOOK_VTABLE(Release,2);

	HOOK_VTABLE(AddAttachedSurface,3);
	HOOK_VTABLE(AddOverlayDirtyRect,4);
	HOOK_VTABLE(Blt,5);
	HOOK_VTABLE(BltBatch,6);
	HOOK_VTABLE(BltFast,7);
	HOOK_VTABLE(DeleteAttachedSurface,8);
	HOOK_VTABLE(EnumAttachedSurfaces,9);
	HOOK_VTABLE(EnumOverlayZOrders,10);
	HOOK_VTABLE(Flip,11);
	HOOK_VTABLE(GetAttachedSurface,12);
	HOOK_VTABLE(GetBltStatus,13);
	HOOK_VTABLE(GetCaps,14);
	HOOK_VTABLE(GetClipper,15);
	HOOK_VTABLE(GetColorKey,16);
	HOOK_VTABLE(GetDC,17);
	HOOK_VTABLE(GetFlipStatus,18);
	HOOK_VTABLE(GetOverlayPosition,19);
	HOOK_VTABLE(GetPalette,20);
	HOOK_VTABLE(GetPixelFormat,21);
	HOOK_VTABLE(GetSurfaceDesc,22);
	HOOK_VTABLE(Initialize,23);
	HOOK_VTABLE(IsLost,24);
	HOOK_VTABLE(Lock,25);
	HOOK_VTABLE(ReleaseDC,26);
	HOOK_VTABLE(Restore,27);
	HOOK_VTABLE(SetClipper,28);
	HOOK_VTABLE(SetColorKey,29);
	HOOK_VTABLE(SetOverlayPosition,30);
	HOOK_VTABLE(SetPalette,31);
	HOOK_VTABLE(Unlock,32);
	HOOK_VTABLE(UpdateOverlay,33);
	HOOK_VTABLE(UpdateOverlayDisplay,34);
	HOOK_VTABLE(UpdateOverlayZOrder,35);

	HOOK_VTABLE(GetDDInterface,36);
	HOOK_VTABLE(PageLock,37);
	HOOK_VTABLE(PageUnlock,38);


	VirtualProtect( CDirectDrawSurface2_vtable, 39*4, dwOld, &dwOld );
}


void Device_HookVMT( IDirectDrawSurface2* ptr )
{
	if( ptr && ptr != INVALID_HANDLE_VALUE ) {
		CDirectDrawSurface2_vtable = *(VFPTR**)ptr;
		Device_HookVMTSurf2( true );
	} else {
		warning( "Device_HookVMT::IDirectDrawSurface2 invalid PTR: 0x%x", ptr );
	}
}
