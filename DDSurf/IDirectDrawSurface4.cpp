/***************************************************************
 * Project: DDrawWrap
 * File: IDirectDrawSurface4.cpp
 * Copyright � learn_more
 */

#include "../winclude.h"


class CDirectDrawSurface4 {
public:
/*** IUnknown methods ***/
HRESULT __stdcall QueryInterface( REFIID riid, LPVOID* ppvObj );
ULONG __stdcall AddRef();
ULONG __stdcall Release();
/*** IDirectDraw methods ***/
HRESULT __stdcall AddAttachedSurface( LPDIRECTDRAWSURFACE4 lpDDSAttachedSurface );
HRESULT __stdcall AddOverlayDirtyRect( LPRECT lpRect );
HRESULT __stdcall Blt( LPRECT lpDestRect, LPDIRECTDRAWSURFACE4 lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwFlags, LPDDBLTFX lpDDBltFx );
HRESULT __stdcall BltBatch( LPDDBLTBATCH lpDDBltBatch, DWORD dwCount, DWORD dwFlags );
HRESULT __stdcall BltFast( DWORD dwX, DWORD dwY, LPDIRECTDRAWSURFACE4 lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwTrans );
HRESULT __stdcall DeleteAttachedSurface( DWORD dwFlags, LPDIRECTDRAWSURFACE4 lpDDSAttachedSurface );
HRESULT __stdcall EnumAttachedSurfaces( LPVOID lpContext, LPDDENUMSURFACESCALLBACK2 lpEnumSurfacesCallback );
HRESULT __stdcall EnumOverlayZOrders( DWORD dwFlags, LPVOID lpContext, LPDDENUMSURFACESCALLBACK2 lpfnCallback );
HRESULT __stdcall Flip( LPDIRECTDRAWSURFACE4 lpDDSurfaceTargetOverride, DWORD dwFlags );
HRESULT __stdcall GetAttachedSurface( LPDDSCAPS2 lpDDSCaps, LPDIRECTDRAWSURFACE4* lplpDDAttachedSurface );
HRESULT __stdcall GetBltStatus( DWORD dwFlags );
HRESULT __stdcall GetCaps( LPDDSCAPS2 lpDDSCaps );
HRESULT __stdcall GetClipper( LPDIRECTDRAWCLIPPER* lplpDDClipper );
HRESULT __stdcall GetColorKey( DWORD dwFlags, LPDDCOLORKEY lpDDColorKey );
HRESULT __stdcall GetDC( HDC* lphDC );
HRESULT __stdcall GetFlipStatus( DWORD dwFlags );
HRESULT __stdcall GetOverlayPosition( LPLONG lplX, LPLONG lplY );
HRESULT __stdcall GetPalette( LPDIRECTDRAWPALETTE* lplpDDPalette );
HRESULT __stdcall GetPixelFormat( LPDDPIXELFORMAT lpDDPixelFormat );
HRESULT __stdcall GetSurfaceDesc( LPDDSURFACEDESC2 lpDDSurfaceDesc );
HRESULT __stdcall Initialize( LPDIRECTDRAW lpDD, LPDDSURFACEDESC2 lpDDSurfaceDesc );
HRESULT __stdcall IsLost();
HRESULT __stdcall Lock( LPRECT lpDestRect, LPDDSURFACEDESC2 lpDDSurfaceDesc, DWORD dwFlags, HANDLE hEvent );
HRESULT __stdcall ReleaseDC( HDC hDC );
HRESULT __stdcall Restore();
HRESULT __stdcall SetClipper( LPDIRECTDRAWCLIPPER lpDDClipper );
HRESULT __stdcall SetColorKey( DWORD dwFlags, LPDDCOLORKEY lpDDColorKey );
HRESULT __stdcall SetOverlayPosition( LONG lX, LONG lY );
HRESULT __stdcall SetPalette( LPDIRECTDRAWPALETTE lpDDPalette );
HRESULT __stdcall Unlock( LPRECT lpRect );
HRESULT __stdcall UpdateOverlay( LPRECT lpSrcRect, LPDIRECTDRAWSURFACE4 lpDDDestSurface, LPRECT lpDestRect, DWORD dwFlags, LPDDOVERLAYFX lpDDOverlayFx );
HRESULT __stdcall UpdateOverlayDisplay( DWORD dwFlags );
HRESULT __stdcall UpdateOverlayZOrder( DWORD dwFlags, LPDIRECTDRAWSURFACE4 lpDDSReference );
/*** Added in the V2 Interface ***/
HRESULT __stdcall GetDDInterface( LPVOID* lplpDD );
HRESULT __stdcall PageLock( DWORD dwFlags );
HRESULT __stdcall PageUnlock( DWORD dwFlags );
/*** Added in the V3 Interface ***/
HRESULT __stdcall SetSurfaceDesc( LPDDSURFACEDESC2 lpDDSurfaceDesc, DWORD dwFlags );
/*** Added in the V4 Interface ***/
HRESULT __stdcall SetPrivateData( REFGUID guidTag, LPVOID lpData, DWORD cbSize, DWORD dwFlags );
HRESULT __stdcall GetPrivateData( REFGUID guidTag, LPVOID lpBuffer, LPDWORD lpcbBufferSize );
HRESULT __stdcall FreePrivateData( REFGUID guidTag );
HRESULT __stdcall GetUniquenessValue( LPDWORD lpValue );
HRESULT __stdcall ChangeUniquenessValue();
};


/*** IUnknown methods ***/
HRESULT (__stdcall CDirectDrawSurface4::*oQueryInterface)( REFIID riid, LPVOID* ppvObj );
ULONG (__stdcall CDirectDrawSurface4::*oAddRef)();
ULONG (__stdcall CDirectDrawSurface4::*oRelease)();
/*** IDirectDraw methods ***/
HRESULT (__stdcall CDirectDrawSurface4::*oAddAttachedSurface)( LPDIRECTDRAWSURFACE4 lpDDSAttachedSurface );
HRESULT (__stdcall CDirectDrawSurface4::*oAddOverlayDirtyRect)( LPRECT lpRect );
HRESULT (__stdcall CDirectDrawSurface4::*oBlt)( LPRECT lpDestRect, LPDIRECTDRAWSURFACE4 lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwFlags, LPDDBLTFX lpDDBltFx );
HRESULT (__stdcall CDirectDrawSurface4::*oBltBatch)( LPDDBLTBATCH lpDDBltBatch, DWORD dwCount, DWORD dwFlags );
HRESULT (__stdcall CDirectDrawSurface4::*oBltFast)( DWORD dwX, DWORD dwY, LPDIRECTDRAWSURFACE4 lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwTrans );
HRESULT (__stdcall CDirectDrawSurface4::*oDeleteAttachedSurface)( DWORD dwFlags, LPDIRECTDRAWSURFACE4 lpDDSAttachedSurface );
HRESULT (__stdcall CDirectDrawSurface4::*oEnumAttachedSurfaces)( LPVOID lpContext, LPDDENUMSURFACESCALLBACK2 lpEnumSurfacesCallback );
HRESULT (__stdcall CDirectDrawSurface4::*oEnumOverlayZOrders)( DWORD dwFlags, LPVOID lpContext, LPDDENUMSURFACESCALLBACK2 lpfnCallback );
HRESULT (__stdcall CDirectDrawSurface4::*oFlip)( LPDIRECTDRAWSURFACE4 lpDDSurfaceTargetOverride, DWORD dwFlags );
HRESULT (__stdcall CDirectDrawSurface4::*oGetAttachedSurface)( LPDDSCAPS2 lpDDSCaps, LPDIRECTDRAWSURFACE4* lplpDDAttachedSurface );
HRESULT (__stdcall CDirectDrawSurface4::*oGetBltStatus)( DWORD dwFlags );
HRESULT (__stdcall CDirectDrawSurface4::*oGetCaps)( LPDDSCAPS2 lpDDSCaps );
HRESULT (__stdcall CDirectDrawSurface4::*oGetClipper)( LPDIRECTDRAWCLIPPER* lplpDDClipper );
HRESULT (__stdcall CDirectDrawSurface4::*oGetColorKey)( DWORD dwFlags, LPDDCOLORKEY lpDDColorKey );
HRESULT (__stdcall CDirectDrawSurface4::*oGetDC)( HDC* lphDC );
HRESULT (__stdcall CDirectDrawSurface4::*oGetFlipStatus)( DWORD dwFlags );
HRESULT (__stdcall CDirectDrawSurface4::*oGetOverlayPosition)( LPLONG lplX, LPLONG lplY );
HRESULT (__stdcall CDirectDrawSurface4::*oGetPalette)( LPDIRECTDRAWPALETTE* lplpDDPalette );
HRESULT (__stdcall CDirectDrawSurface4::*oGetPixelFormat)( LPDDPIXELFORMAT lpDDPixelFormat );
HRESULT (__stdcall CDirectDrawSurface4::*oGetSurfaceDesc)( LPDDSURFACEDESC2 lpDDSurfaceDesc );
HRESULT (__stdcall CDirectDrawSurface4::*oInitialize)( LPDIRECTDRAW lpDD, LPDDSURFACEDESC2 lpDDSurfaceDesc );
HRESULT (__stdcall CDirectDrawSurface4::*oIsLost)();
HRESULT (__stdcall CDirectDrawSurface4::*oLock)( LPRECT lpDestRect, LPDDSURFACEDESC2 lpDDSurfaceDesc, DWORD dwFlags, HANDLE hEvent );
HRESULT (__stdcall CDirectDrawSurface4::*oReleaseDC)( HDC hDC );
HRESULT (__stdcall CDirectDrawSurface4::*oRestore)();
HRESULT (__stdcall CDirectDrawSurface4::*oSetClipper)( LPDIRECTDRAWCLIPPER lpDDClipper );
HRESULT (__stdcall CDirectDrawSurface4::*oSetColorKey)( DWORD dwFlags, LPDDCOLORKEY lpDDColorKey );
HRESULT (__stdcall CDirectDrawSurface4::*oSetOverlayPosition)( LONG lX, LONG lY );
HRESULT (__stdcall CDirectDrawSurface4::*oSetPalette)( LPDIRECTDRAWPALETTE lpDDPalette );
HRESULT (__stdcall CDirectDrawSurface4::*oUnlock)( LPRECT lpRect );
HRESULT (__stdcall CDirectDrawSurface4::*oUpdateOverlay)( LPRECT lpSrcRect, LPDIRECTDRAWSURFACE4 lpDDDestSurface, LPRECT lpDestRect, DWORD dwFlags, LPDDOVERLAYFX lpDDOverlayFx );
HRESULT (__stdcall CDirectDrawSurface4::*oUpdateOverlayDisplay)( DWORD dwFlags );
HRESULT (__stdcall CDirectDrawSurface4::*oUpdateOverlayZOrder)( DWORD dwFlags, LPDIRECTDRAWSURFACE4 lpDDSReference );
/*** Added in the V2 Interface ***/
HRESULT (__stdcall CDirectDrawSurface4::*oGetDDInterface)( LPVOID* lplpDD );
HRESULT (__stdcall CDirectDrawSurface4::*oPageLock)( DWORD dwFlags );
HRESULT (__stdcall CDirectDrawSurface4::*oPageUnlock)( DWORD dwFlags );
/*** Added in the V3 Interface ***/
HRESULT (__stdcall CDirectDrawSurface4::*oSetSurfaceDesc)( LPDDSURFACEDESC2 lpDDSurfaceDesc, DWORD dwFlags );
/*** Added in the V4 Interface ***/
HRESULT (__stdcall CDirectDrawSurface4::*oSetPrivateData)( REFGUID guidTag, LPVOID lpData, DWORD cbSize, DWORD dwFlags );
HRESULT (__stdcall CDirectDrawSurface4::*oGetPrivateData)( REFGUID guidTag, LPVOID lpBuffer, LPDWORD lpcbBufferSize );
HRESULT (__stdcall CDirectDrawSurface4::*oFreePrivateData)( REFGUID guidTag );
HRESULT (__stdcall CDirectDrawSurface4::*oGetUniquenessValue)( LPDWORD lpValue );
HRESULT (__stdcall CDirectDrawSurface4::*oChangeUniquenessValue)();


typedef void (__stdcall CDirectDrawSurface4::*VFPTR)();
VFPTR* CDirectDrawSurface4_vtable = 0;

#define HOOK_PREFIX	CDirectDrawSurface4



HRESULT CDirectDrawSurface4::QueryInterface( REFIID riid, LPVOID* ppvObj )
{
	char buf[100];
	FormatGUID( riid, buf, sizeof(buf) );
	ADD_LOG( "IDirectDrawSurface4(%p)::QueryInterface( %s )", PRINT_DEV, buf );
	return (this->*oQueryInterface)( riid, ppvObj );
}

ULONG CDirectDrawSurface4::AddRef()
{
	ULONG pRef = (this->*oAddRef)();
	ADD_LOG( "IDirectDrawSurface4(%p)::AddRef( %i )", PRINT_DEV, pRef );
	return pRef;
}

ULONG CDirectDrawSurface4::Release()
{
	ULONG uRet = (this->*oRelease)();
	ADD_LOG( "IDirectDrawSurface4(%p)::Release( %i )", PRINT_DEV, uRet );
	return uRet;
}

HRESULT CDirectDrawSurface4::AddAttachedSurface( LPDIRECTDRAWSURFACE4 lpDDSAttachedSurface )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::AddAttachedSurface()", PRINT_DEV );
	return (this->*oAddAttachedSurface)( lpDDSAttachedSurface );
}

HRESULT CDirectDrawSurface4::AddOverlayDirtyRect( LPRECT lpRect )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::AddOverlayDirtyRect()", PRINT_DEV );
	return (this->*oAddOverlayDirtyRect)( lpRect );
}

HRESULT CDirectDrawSurface4::Blt( LPRECT lpDestRect, LPDIRECTDRAWSURFACE4 lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwFlags, LPDDBLTFX lpDDBltFx )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::Blt()", PRINT_DEV );
	return (this->*oBlt)( lpDestRect, lpDDSrcSurface, lpSrcRect, dwFlags, lpDDBltFx );
}

HRESULT CDirectDrawSurface4::BltBatch( LPDDBLTBATCH lpDDBltBatch, DWORD dwCount, DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::BltBatch()", PRINT_DEV );
	return (this->*oBltBatch)( lpDDBltBatch, dwCount, dwFlags );
}

HRESULT CDirectDrawSurface4::BltFast( DWORD dwX, DWORD dwY, LPDIRECTDRAWSURFACE4 lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwTrans )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::BltFast()", PRINT_DEV );
	return (this->*oBltFast)( dwX, dwY, lpDDSrcSurface, lpSrcRect, dwTrans );
}

HRESULT CDirectDrawSurface4::DeleteAttachedSurface( DWORD dwFlags, LPDIRECTDRAWSURFACE4 lpDDSAttachedSurface )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::DeleteAttachedSurface()", PRINT_DEV );
	return (this->*oDeleteAttachedSurface)( dwFlags, lpDDSAttachedSurface );
}

HRESULT CDirectDrawSurface4::EnumAttachedSurfaces( LPVOID lpContext, LPDDENUMSURFACESCALLBACK2 lpEnumSurfacesCallback )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::EnumAttachedSurfaces()", PRINT_DEV );
	return (this->*oEnumAttachedSurfaces)( lpContext, lpEnumSurfacesCallback );
}

HRESULT CDirectDrawSurface4::EnumOverlayZOrders( DWORD dwFlags, LPVOID lpContext, LPDDENUMSURFACESCALLBACK2 lpfnCallback )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::EnumOverlayZOrders()", PRINT_DEV );
	return (this->*oEnumOverlayZOrders)( dwFlags, lpContext, lpfnCallback );
}

HRESULT CDirectDrawSurface4::Flip( LPDIRECTDRAWSURFACE4 lpDDSurfaceTargetOverride, DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::Flip()", PRINT_DEV );
	return (this->*oFlip)( lpDDSurfaceTargetOverride, dwFlags );
}

HRESULT CDirectDrawSurface4::GetAttachedSurface( LPDDSCAPS2 lpDDSCaps2, LPDIRECTDRAWSURFACE4* lplpDDAttachedSurface )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::GetAttachedSurface()", PRINT_DEV );
	return (this->*oGetAttachedSurface)( lpDDSCaps2, lplpDDAttachedSurface );
}

HRESULT CDirectDrawSurface4::GetBltStatus( DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::GetBltStatus()", PRINT_DEV );
	return (this->*oGetBltStatus)( dwFlags );
}

HRESULT CDirectDrawSurface4::GetCaps( LPDDSCAPS2 lpDDSCaps2 )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::GetCaps()", PRINT_DEV );
	return (this->*oGetCaps)( lpDDSCaps2 );
}

HRESULT CDirectDrawSurface4::GetClipper( LPDIRECTDRAWCLIPPER* lplpDDClipper )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::GetClipper()", PRINT_DEV );
	return (this->*oGetClipper)( lplpDDClipper );
}

HRESULT CDirectDrawSurface4::GetColorKey( DWORD dwFlags, LPDDCOLORKEY lpDDColorKey )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::GetColorKey()", PRINT_DEV );
	return (this->*oGetColorKey)( dwFlags, lpDDColorKey );
}

HRESULT CDirectDrawSurface4::GetDC( HDC* lphDC )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::GetDC()", PRINT_DEV );
	return (this->*oGetDC)( lphDC );
}

HRESULT CDirectDrawSurface4::GetFlipStatus( DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::GetFlipStatus()", PRINT_DEV );
	return (this->*oGetFlipStatus)( dwFlags );
}

HRESULT CDirectDrawSurface4::GetOverlayPosition( LPLONG lplX, LPLONG lplY )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::GetOverlayPosition()", PRINT_DEV );
	return (this->*oGetOverlayPosition)( lplX, lplY );
}

HRESULT CDirectDrawSurface4::GetPalette( LPDIRECTDRAWPALETTE* lplpDDPalette )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::GetPalette()", PRINT_DEV );
	return (this->*oGetPalette)( lplpDDPalette );
}

HRESULT CDirectDrawSurface4::GetPixelFormat( LPDDPIXELFORMAT lpDDPixelFormat )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::GetPixelFormat()", PRINT_DEV );
	return (this->*oGetPixelFormat)( lpDDPixelFormat );
}

HRESULT CDirectDrawSurface4::GetSurfaceDesc( LPDDSURFACEDESC2 lpDDSurfaceDesc )
{
	HRESULT hRet = (this->*oGetSurfaceDesc)( lpDDSurfaceDesc );
	char szSurfDesc[1024];
	FormatSurfaceDesc2( lpDDSurfaceDesc, szSurfDesc, sizeof(szSurfDesc) );
	ADD_LOG( "IDirectDrawSurface4(%p)::GetSurfaceDesc( %s ) = 0x%x", PRINT_DEV, szSurfDesc, hRet );
	return hRet;
}

HRESULT CDirectDrawSurface4::Initialize( LPDIRECTDRAW lpDD, LPDDSURFACEDESC2 lpDDSurfaceDesc )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::Initialize()", PRINT_DEV );
	return (this->*oInitialize)( lpDD, lpDDSurfaceDesc );
}

HRESULT CDirectDrawSurface4::IsLost()
{
	ADD_LOG( "IDirectDrawSurface4(%p)::IsLost()", PRINT_DEV );
	return (this->*oIsLost)();
}

HRESULT CDirectDrawSurface4::Lock( LPRECT lpDestRect, LPDDSURFACEDESC2 lpDDSurfaceDesc, DWORD dwFlags, HANDLE hEvent )
{
	char* pszRect = "{entire surface}";
	char pszFlags[512] = {0};
	if( lpDestRect ) {
		pszRect = va( "{%i,%i,%i,%i}", lpDestRect->left, lpDestRect->top, lpDestRect->right, lpDestRect->bottom );
	}
	if( dwFlags ) {
		FormatLockFlags( dwFlags, pszFlags, sizeof(pszFlags) );
	}
	ADD_LOG( "IDirectDrawSurface4(%p)::Lock( %s, %s )", PRINT_DEV, pszRect, pszFlags );
	return (this->*oLock)( lpDestRect, lpDDSurfaceDesc, dwFlags, hEvent );
}

HRESULT CDirectDrawSurface4::ReleaseDC( HDC hDC )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::ReleaseDC()", PRINT_DEV );
	return (this->*oReleaseDC)( hDC );
}

HRESULT CDirectDrawSurface4::Restore()
{
	ADD_LOG( "IDirectDrawSurface4(%p)::Restore()", PRINT_DEV );
	return (this->*oRestore)();
}

HRESULT CDirectDrawSurface4::SetClipper( LPDIRECTDRAWCLIPPER lpDDClipper )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::SetClipper()", PRINT_DEV );
	return (this->*oSetClipper)( lpDDClipper );
}

HRESULT CDirectDrawSurface4::SetColorKey( DWORD dwFlags, LPDDCOLORKEY lpDDColorKey )
{
	char pszColorFlag[512] = {0};
	char* pszColor = "{0}";
	if( lpDDColorKey ) {
		pszColor = va( "{0x%x,0x%x}", lpDDColorKey->dwColorSpaceLowValue, lpDDColorKey->dwColorSpaceHighValue );
	}
	FormatColorkeyFlags( dwFlags, pszColorFlag, sizeof(pszColorFlag) );
	ADD_LOG( "IDirectDrawSurface4(%p)::SetColorKey( %s, %s )", PRINT_DEV, pszColorFlag, pszColor );
	return (this->*oSetColorKey)( dwFlags, lpDDColorKey );
}

HRESULT CDirectDrawSurface4::SetOverlayPosition( LONG lX, LONG lY )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::SetOverlayPosition()", PRINT_DEV );
	return (this->*oSetOverlayPosition)( lX, lY );
}

HRESULT CDirectDrawSurface4::SetPalette( LPDIRECTDRAWPALETTE lpDDPalette )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::SetPalette()", PRINT_DEV );
	return (this->*oSetPalette)( lpDDPalette );
}

HRESULT CDirectDrawSurface4::Unlock( LPRECT lpRect )
{
	char* pszRect = "{entire surface}";
//	char pszFlags[512] = {0};
	if( lpRect ) {
		pszRect = va( "{%i,%i,%i,%i}", lpRect->left, lpRect->top, lpRect->right, lpRect->bottom );
	}
	ADD_LOG( "IDirectDrawSurface4(%p)::Unlock( %s )", PRINT_DEV, pszRect );
	return (this->*oUnlock)( lpRect );
}

HRESULT CDirectDrawSurface4::UpdateOverlay( LPRECT lpSrcRect, LPDIRECTDRAWSURFACE4 lpDDDestSurface, LPRECT lpDestRect, DWORD dwFlags, LPDDOVERLAYFX lpDDOverlayFx )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::UpdateOverlay()", PRINT_DEV );
	return (this->*oUpdateOverlay)( lpSrcRect, lpDDDestSurface, lpDestRect, dwFlags, lpDDOverlayFx );
}

HRESULT CDirectDrawSurface4::UpdateOverlayDisplay( DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::UpdateOverlayDisplay()", PRINT_DEV );
	return (this->*oUpdateOverlayDisplay)( dwFlags );
}

HRESULT CDirectDrawSurface4::UpdateOverlayZOrder( DWORD dwFlags, LPDIRECTDRAWSURFACE4 lpDDSReference )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::UpdateOverlayZOrder()", PRINT_DEV );
	return (this->*oUpdateOverlayZOrder)( dwFlags, lpDDSReference );
}

HRESULT CDirectDrawSurface4::GetDDInterface( LPVOID* lplpDD )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::GetDDInterface()", PRINT_DEV );
	return (this->*oGetDDInterface)( lplpDD );
}

HRESULT CDirectDrawSurface4::PageLock( DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::PageLock()", PRINT_DEV );
	return (this->*oPageLock)( dwFlags );
}

HRESULT CDirectDrawSurface4::PageUnlock( DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::PageUnlock()", PRINT_DEV );
	return (this->*oPageUnlock)( dwFlags );
}

HRESULT CDirectDrawSurface4::SetSurfaceDesc( LPDDSURFACEDESC2 lpDDSurfaceDesc, DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::PageUnlock()", PRINT_DEV );
	return (this->*oSetSurfaceDesc)( lpDDSurfaceDesc, dwFlags );
}

HRESULT CDirectDrawSurface4::SetPrivateData( REFGUID guidTag, LPVOID lpData, DWORD cbSize, DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::SetPrivateData()", PRINT_DEV );
	return (this->*oSetPrivateData)( guidTag, lpData, cbSize, dwFlags );
}

HRESULT CDirectDrawSurface4::GetPrivateData( REFGUID guidTag, LPVOID lpBuffer, LPDWORD lpcbBufferSize )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::GetPrivateData()", PRINT_DEV );
	return (this->*oGetPrivateData)( guidTag, lpBuffer, lpcbBufferSize );
}

HRESULT CDirectDrawSurface4::FreePrivateData( REFGUID guidTag )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::FreePrivateData()", PRINT_DEV );
	return (this->*oFreePrivateData)( guidTag );
}

HRESULT CDirectDrawSurface4::GetUniquenessValue( LPDWORD lpValue )
{
	ADD_LOG( "IDirectDrawSurface4(%p)::GetUniquenessValue()", PRINT_DEV );
	return (this->*oGetUniquenessValue)( lpValue );
}

HRESULT CDirectDrawSurface4::ChangeUniquenessValue()
{
	ADD_LOG( "IDirectDrawSurface4(%p)::ChangeUniquenessValue()", PRINT_DEV );
	return (this->*oChangeUniquenessValue)();
}



void Device_HookVMTSurf4( bool first )
{
	DWORD dwOld = 0;
	VirtualProtect( CDirectDrawSurface4_vtable, 45*4, PAGE_EXECUTE_READWRITE, &dwOld );

	HOOK_VTABLE(QueryInterface,0);
	HOOK_VTABLE(AddRef,1);
	HOOK_VTABLE(Release,2);

	HOOK_VTABLE(AddAttachedSurface,3);
	HOOK_VTABLE(AddOverlayDirtyRect,4);
	HOOK_VTABLE(Blt,5);
	HOOK_VTABLE(BltBatch,6);
	HOOK_VTABLE(BltFast,7);
	HOOK_VTABLE(DeleteAttachedSurface,8);
	HOOK_VTABLE(EnumAttachedSurfaces,9);
	HOOK_VTABLE(EnumOverlayZOrders,10);
	HOOK_VTABLE(Flip,11);
	HOOK_VTABLE(GetAttachedSurface,12);
	HOOK_VTABLE(GetBltStatus,13);
	HOOK_VTABLE(GetCaps,14);
	HOOK_VTABLE(GetClipper,15);
	HOOK_VTABLE(GetColorKey,16);
	HOOK_VTABLE(GetDC,17);
	HOOK_VTABLE(GetFlipStatus,18);
	HOOK_VTABLE(GetOverlayPosition,19);
	HOOK_VTABLE(GetPalette,20);
	HOOK_VTABLE(GetPixelFormat,21);
	HOOK_VTABLE(GetSurfaceDesc,22);
	HOOK_VTABLE(Initialize,23);
	HOOK_VTABLE(IsLost,24);
	HOOK_VTABLE(Lock,25);
	HOOK_VTABLE(ReleaseDC,26);
	HOOK_VTABLE(Restore,27);
	HOOK_VTABLE(SetClipper,28);
	HOOK_VTABLE(SetColorKey,29);
	HOOK_VTABLE(SetOverlayPosition,30);
	HOOK_VTABLE(SetPalette,31);
	HOOK_VTABLE(Unlock,32);
	HOOK_VTABLE(UpdateOverlay,33);
	HOOK_VTABLE(UpdateOverlayDisplay,34);
	HOOK_VTABLE(UpdateOverlayZOrder,35);

	HOOK_VTABLE(GetDDInterface,36);
	HOOK_VTABLE(PageLock,37);
	HOOK_VTABLE(PageUnlock,38);

	HOOK_VTABLE(SetSurfaceDesc,39);

	HOOK_VTABLE(SetPrivateData,40);
	HOOK_VTABLE(GetPrivateData,41);
	HOOK_VTABLE(FreePrivateData,42);
	HOOK_VTABLE(GetUniquenessValue,43);
	HOOK_VTABLE(ChangeUniquenessValue,44);

	VirtualProtect( CDirectDrawSurface4_vtable, 45*4, dwOld, &dwOld );
}


void Device_HookVMT( IDirectDrawSurface4* ptr )
{
	if( ptr && ptr != INVALID_HANDLE_VALUE ) {
		CDirectDrawSurface4_vtable = *(VFPTR**)ptr;
		Device_HookVMTSurf4( true );
	} else {
		warning( "Device_HookVMT::IDirectDrawSurface4 invalid PTR: 0x%x", ptr );
	}
}
