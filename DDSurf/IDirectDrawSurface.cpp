/***************************************************************
 * Project: DDrawWrap
 * File: IDirectDrawSurface.cpp
 * Copyright � learn_more
 */

#include "../winclude.h"

class CDirectDrawSurface {
public:
/*** IUnknown methods ***/
HRESULT __stdcall QueryInterface( REFIID riid, LPVOID* ppvObj );
ULONG __stdcall AddRef();
ULONG __stdcall Release();
/*** IDirectDraw methods ***/
HRESULT __stdcall AddAttachedSurface( LPDIRECTDRAWSURFACE lpDDSAttachedSurface );
HRESULT __stdcall AddOverlayDirtyRect( LPRECT lpRect );
HRESULT __stdcall Blt( LPRECT lpDestRect, LPDIRECTDRAWSURFACE lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwFlags, LPDDBLTFX lpDDBltFx );
HRESULT __stdcall BltBatch( LPDDBLTBATCH lpDDBltBatch, DWORD dwCount, DWORD dwFlags );
HRESULT __stdcall BltFast( DWORD dwX, DWORD dwY, LPDIRECTDRAWSURFACE lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwTrans );
HRESULT __stdcall DeleteAttachedSurface( DWORD dwFlags, LPDIRECTDRAWSURFACE lpDDSAttachedSurface );
HRESULT __stdcall EnumAttachedSurfaces( LPVOID lpContext, LPDDENUMSURFACESCALLBACK lpEnumSurfacesCallback );
HRESULT __stdcall EnumOverlayZOrders( DWORD dwFlags, LPVOID lpContext, LPDDENUMSURFACESCALLBACK lpfnCallback );
HRESULT __stdcall Flip( LPDIRECTDRAWSURFACE lpDDSurfaceTargetOverride, DWORD dwFlags );
HRESULT __stdcall GetAttachedSurface( LPDDSCAPS lpDDSCaps, LPDIRECTDRAWSURFACE* lplpDDAttachedSurface );
HRESULT __stdcall GetBltStatus( DWORD dwFlags );
HRESULT __stdcall GetCaps( LPDDSCAPS lpDDSCaps );
HRESULT __stdcall GetClipper( LPDIRECTDRAWCLIPPER* lplpDDClipper );
HRESULT __stdcall GetColorKey( DWORD dwFlags, LPDDCOLORKEY lpDDColorKey );
HRESULT __stdcall GetDC( HDC* lphDC );
HRESULT __stdcall GetFlipStatus( DWORD dwFlags );
HRESULT __stdcall GetOverlayPosition( LPLONG lplX, LPLONG lplY );
HRESULT __stdcall GetPalette( LPDIRECTDRAWPALETTE* lplpDDPalette );
HRESULT __stdcall GetPixelFormat( LPDDPIXELFORMAT lpDDPixelFormat );
HRESULT __stdcall GetSurfaceDesc( LPDDSURFACEDESC lpDDSurfaceDesc );
HRESULT __stdcall Initialize( LPDIRECTDRAW lpDD, LPDDSURFACEDESC lpDDSurfaceDesc );
HRESULT __stdcall IsLost();
HRESULT __stdcall Lock( LPRECT lpDestRect, LPDDSURFACEDESC lpDDSurfaceDesc, DWORD dwFlags, HANDLE hEvent );
HRESULT __stdcall ReleaseDC( HDC hDC );
HRESULT __stdcall Restore();
HRESULT __stdcall SetClipper( LPDIRECTDRAWCLIPPER lpDDClipper );
HRESULT __stdcall SetColorKey( DWORD dwFlags, LPDDCOLORKEY lpDDColorKey );
HRESULT __stdcall SetOverlayPosition( LONG lX, LONG lY );
HRESULT __stdcall SetPalette( LPDIRECTDRAWPALETTE lpDDPalette );
HRESULT __stdcall Unlock( LPVOID lpRect );
HRESULT __stdcall UpdateOverlay( LPRECT lpSrcRect, LPDIRECTDRAWSURFACE lpDDDestSurface, LPRECT lpDestRect, DWORD dwFlags, LPDDOVERLAYFX lpDDOverlayFx );
HRESULT __stdcall UpdateOverlayDisplay( DWORD dwFlags );
HRESULT __stdcall UpdateOverlayZOrder( DWORD dwFlags, LPDIRECTDRAWSURFACE lpDDSReference );
};

HRESULT (__stdcall CDirectDrawSurface::*oQueryInterface)( REFIID riid, LPVOID* ppvObj );
ULONG (__stdcall CDirectDrawSurface::*oAddRef)();
ULONG (__stdcall CDirectDrawSurface::*oRelease)();
/*** IDirectDraw methods ***/
HRESULT (__stdcall CDirectDrawSurface::*oAddAttachedSurface)( LPDIRECTDRAWSURFACE lpDDSAttachedSurface );
HRESULT (__stdcall CDirectDrawSurface::*oAddOverlayDirtyRect)( LPRECT lpRect );
HRESULT (__stdcall CDirectDrawSurface::*oBlt)( LPRECT lpDestRect, LPDIRECTDRAWSURFACE lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwFlags, LPDDBLTFX lpDDBltFx );
HRESULT (__stdcall CDirectDrawSurface::*oBltBatch)( LPDDBLTBATCH lpDDBltBatch, DWORD dwCount, DWORD dwFlags );
HRESULT (__stdcall CDirectDrawSurface::*oBltFast)( DWORD dwX, DWORD dwY, LPDIRECTDRAWSURFACE lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwTrans );
HRESULT (__stdcall CDirectDrawSurface::*oDeleteAttachedSurface)( DWORD dwFlags, LPDIRECTDRAWSURFACE lpDDSAttachedSurface );
HRESULT (__stdcall CDirectDrawSurface::*oEnumAttachedSurfaces)( LPVOID lpContext, LPDDENUMSURFACESCALLBACK lpEnumSurfacesCallback );
HRESULT (__stdcall CDirectDrawSurface::*oEnumOverlayZOrders)( DWORD dwFlags, LPVOID lpContext, LPDDENUMSURFACESCALLBACK lpfnCallback );
HRESULT (__stdcall CDirectDrawSurface::*oFlip)( LPDIRECTDRAWSURFACE lpDDSurfaceTargetOverride, DWORD dwFlags );
HRESULT (__stdcall CDirectDrawSurface::*oGetAttachedSurface)( LPDDSCAPS lpDDSCaps, LPDIRECTDRAWSURFACE* lplpDDAttachedSurface );
HRESULT (__stdcall CDirectDrawSurface::*oGetBltStatus)( DWORD dwFlags );
HRESULT (__stdcall CDirectDrawSurface::*oGetCaps)( LPDDSCAPS lpDDSCaps );
HRESULT (__stdcall CDirectDrawSurface::*oGetClipper)( LPDIRECTDRAWCLIPPER* lplpDDClipper );
HRESULT (__stdcall CDirectDrawSurface::*oGetColorKey)( DWORD dwFlags, LPDDCOLORKEY lpDDColorKey );
HRESULT (__stdcall CDirectDrawSurface::*oGetDC)( HDC* lphDC );
HRESULT (__stdcall CDirectDrawSurface::*oGetFlipStatus)( DWORD dwFlags );
HRESULT (__stdcall CDirectDrawSurface::*oGetOverlayPosition)( LPLONG lplX, LPLONG lplY );
HRESULT (__stdcall CDirectDrawSurface::*oGetPalette)( LPDIRECTDRAWPALETTE* lplpDDPalette );
HRESULT (__stdcall CDirectDrawSurface::*oGetPixelFormat)( LPDDPIXELFORMAT lpDDPixelFormat );
HRESULT (__stdcall CDirectDrawSurface::*oGetSurfaceDesc)( LPDDSURFACEDESC lpDDSurfaceDesc );
HRESULT (__stdcall CDirectDrawSurface::*oInitialize)( LPDIRECTDRAW lpDD, LPDDSURFACEDESC lpDDSurfaceDesc );
HRESULT (__stdcall CDirectDrawSurface::*oIsLost)();
HRESULT (__stdcall CDirectDrawSurface::*oLock)( LPRECT lpDestRect, LPDDSURFACEDESC lpDDSurfaceDesc, DWORD dwFlags, HANDLE hEvent );
HRESULT (__stdcall CDirectDrawSurface::*oReleaseDC)( HDC hDC );
HRESULT (__stdcall CDirectDrawSurface::*oRestore)();
HRESULT (__stdcall CDirectDrawSurface::*oSetClipper)( LPDIRECTDRAWCLIPPER lpDDClipper );
HRESULT (__stdcall CDirectDrawSurface::*oSetColorKey)( DWORD dwFlags, LPDDCOLORKEY lpDDColorKey );
HRESULT (__stdcall CDirectDrawSurface::*oSetOverlayPosition)( LONG lX, LONG lY );
HRESULT (__stdcall CDirectDrawSurface::*oSetPalette)( LPDIRECTDRAWPALETTE lpDDPalette );
HRESULT (__stdcall CDirectDrawSurface::*oUnlock)( LPVOID lpRect );
HRESULT (__stdcall CDirectDrawSurface::*oUpdateOverlay)( LPRECT lpSrcRect, LPDIRECTDRAWSURFACE lpDDDestSurface, LPRECT lpDestRect, DWORD dwFlags, LPDDOVERLAYFX lpDDOverlayFx );
HRESULT (__stdcall CDirectDrawSurface::*oUpdateOverlayDisplay)( DWORD dwFlags );
HRESULT (__stdcall CDirectDrawSurface::*oUpdateOverlayZOrder)( DWORD dwFlags, LPDIRECTDRAWSURFACE lpDDSReference );


typedef void (__stdcall CDirectDrawSurface::*VFPTR)();
VFPTR* CDirectDrawSurface_vtable = 0;

#define HOOK_PREFIX	CDirectDrawSurface


HRESULT CDirectDrawSurface::QueryInterface( REFIID riid, LPVOID* ppvObj )
{
	char buf[100];
	FormatGUID( riid, buf, sizeof(buf) );
	ADD_LOG( "IDirectDrawSurface (%p)::QueryInterface( %s )", PRINT_DEV, buf );
	return (this->*oQueryInterface)( riid, ppvObj );
}

ULONG CDirectDrawSurface::AddRef()
{
	ULONG pRef = (this->*oAddRef)();
	ADD_LOG( "IDirectDrawSurface (%p)::AddRef( %i )", PRINT_DEV, pRef );
	return pRef;
}

ULONG CDirectDrawSurface::Release()
{
	ULONG uRet = (this->*oRelease)();
	ADD_LOG( "IDirectDrawSurface (%p)::Release( %i )", PRINT_DEV, uRet );
	return uRet;
}

HRESULT CDirectDrawSurface::AddAttachedSurface( LPDIRECTDRAWSURFACE lpDDSAttachedSurface )
{
	ADD_LOG( "IDirectDrawSurface (%p)::AddAttachedSurface()", PRINT_DEV );
	return (this->*oAddAttachedSurface)( lpDDSAttachedSurface );
}

HRESULT CDirectDrawSurface::AddOverlayDirtyRect( LPRECT lpRect )
{
	ADD_LOG( "IDirectDrawSurface (%p)::AddOverlayDirtyRect()", PRINT_DEV );
	return (this->*oAddOverlayDirtyRect)( lpRect );
}

HRESULT CDirectDrawSurface::Blt( LPRECT lpDestRect, LPDIRECTDRAWSURFACE lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwFlags, LPDDBLTFX lpDDBltFx )
{
	ADD_LOG( "IDirectDrawSurface (%p)::Blt()", PRINT_DEV );
	return (this->*oBlt)( lpDestRect, lpDDSrcSurface, lpSrcRect, dwFlags, lpDDBltFx );
}

HRESULT CDirectDrawSurface::BltBatch( LPDDBLTBATCH lpDDBltBatch, DWORD dwCount, DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface (%p)::BltBatch()", PRINT_DEV );
	return (this->*oBltBatch)( lpDDBltBatch, dwCount, dwFlags );
}

HRESULT CDirectDrawSurface::BltFast( DWORD dwX, DWORD dwY, LPDIRECTDRAWSURFACE lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwTrans )
{
	ADD_LOG( "IDirectDrawSurface (%p)::BltFast()", PRINT_DEV );
	return (this->*oBltFast)( dwX, dwY, lpDDSrcSurface, lpSrcRect, dwTrans );
}

HRESULT CDirectDrawSurface::DeleteAttachedSurface( DWORD dwFlags, LPDIRECTDRAWSURFACE lpDDSAttachedSurface )
{
	ADD_LOG( "IDirectDrawSurface (%p)::DeleteAttachedSurface()", PRINT_DEV );
	return (this->*oDeleteAttachedSurface)( dwFlags, lpDDSAttachedSurface );
}

HRESULT CDirectDrawSurface::EnumAttachedSurfaces( LPVOID lpContext, LPDDENUMSURFACESCALLBACK lpEnumSurfacesCallback )
{
	ADD_LOG( "IDirectDrawSurface (%p)::EnumAttachedSurfaces()", PRINT_DEV );
	return (this->*oEnumAttachedSurfaces)( lpContext, lpEnumSurfacesCallback );
}

HRESULT CDirectDrawSurface::EnumOverlayZOrders( DWORD dwFlags, LPVOID lpContext, LPDDENUMSURFACESCALLBACK lpfnCallback )
{
	ADD_LOG( "IDirectDrawSurface (%p)::EnumOverlayZOrders()", PRINT_DEV );
	return (this->*oEnumOverlayZOrders)( dwFlags, lpContext, lpfnCallback );
}

HRESULT CDirectDrawSurface::Flip( LPDIRECTDRAWSURFACE lpDDSurfaceTargetOverride, DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface (%p)::Flip()", PRINT_DEV );
	return (this->*oFlip)( lpDDSurfaceTargetOverride, dwFlags );
}

HRESULT CDirectDrawSurface::GetAttachedSurface( LPDDSCAPS lpDDSCaps, LPDIRECTDRAWSURFACE* lplpDDAttachedSurface )
{
	ADD_LOG( "IDirectDrawSurface (%p)::GetAttachedSurface()", PRINT_DEV );
	return (this->*oGetAttachedSurface)( lpDDSCaps, lplpDDAttachedSurface );
}

HRESULT CDirectDrawSurface::GetBltStatus( DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface (%p)::GetBltStatus()", PRINT_DEV );
	return (this->*oGetBltStatus)( dwFlags );
}

HRESULT CDirectDrawSurface::GetCaps( LPDDSCAPS lpDDSCaps )
{
	ADD_LOG( "IDirectDrawSurface (%p)::GetCaps()", PRINT_DEV );
	return (this->*oGetCaps)( lpDDSCaps );
}

HRESULT CDirectDrawSurface::GetClipper( LPDIRECTDRAWCLIPPER* lplpDDClipper )
{
	ADD_LOG( "IDirectDrawSurface (%p)::GetClipper()", PRINT_DEV );
	return (this->*oGetClipper)( lplpDDClipper );
}

HRESULT CDirectDrawSurface::GetColorKey( DWORD dwFlags, LPDDCOLORKEY lpDDColorKey )
{
	ADD_LOG( "IDirectDrawSurface (%p)::GetColorKey()", PRINT_DEV );
	return (this->*oGetColorKey)( dwFlags, lpDDColorKey );
}

HRESULT CDirectDrawSurface::GetDC( HDC* lphDC )
{
	ADD_LOG( "IDirectDrawSurface (%p)::GetDC()", PRINT_DEV );
	return (this->*oGetDC)( lphDC );
}

HRESULT CDirectDrawSurface::GetFlipStatus( DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface (%p)::GetFlipStatus()", PRINT_DEV );
	return (this->*oGetFlipStatus)( dwFlags );
}

HRESULT CDirectDrawSurface::GetOverlayPosition( LPLONG lplX, LPLONG lplY )
{
	ADD_LOG( "IDirectDrawSurface (%p)::GetOverlayPosition()", PRINT_DEV );
	return (this->*oGetOverlayPosition)( lplX, lplY );
}

HRESULT CDirectDrawSurface::GetPalette( LPDIRECTDRAWPALETTE* lplpDDPalette )
{
	ADD_LOG( "IDirectDrawSurface (%p)::GetPalette()", PRINT_DEV );
	return (this->*oGetPalette)( lplpDDPalette );
}

HRESULT CDirectDrawSurface::GetPixelFormat( LPDDPIXELFORMAT lpDDPixelFormat )
{
	ADD_LOG( "IDirectDrawSurface (%p)::GetPixelFormat()", PRINT_DEV );
	return (this->*oGetPixelFormat)( lpDDPixelFormat );
}

HRESULT CDirectDrawSurface::GetSurfaceDesc( LPDDSURFACEDESC lpDDSurfaceDesc )
{
	HRESULT hRet = (this->*oGetSurfaceDesc)( lpDDSurfaceDesc );
	char szSurfDesc[1024];
	FormatSurfaceDesc( lpDDSurfaceDesc, szSurfDesc, sizeof(szSurfDesc) );
	ADD_LOG( "IDirectDrawSurface (%p)::GetSurfaceDesc( %s ) = 0x%x", PRINT_DEV, szSurfDesc, hRet );
	return hRet;
}

HRESULT CDirectDrawSurface::Initialize( LPDIRECTDRAW lpDD, LPDDSURFACEDESC lpDDSurfaceDesc )
{
	ADD_LOG( "IDirectDrawSurface (%p)::Initialize()", PRINT_DEV );
	return (this->*oInitialize)( lpDD, lpDDSurfaceDesc );
}

HRESULT CDirectDrawSurface::IsLost()
{
	ADD_LOG( "IDirectDrawSurface (%p)::IsLost()", PRINT_DEV );
	return (this->*oIsLost)();
}

HRESULT CDirectDrawSurface::Lock( LPRECT lpDestRect, LPDDSURFACEDESC lpDDSurfaceDesc, DWORD dwFlags, HANDLE hEvent )
{
	char* pszRect = "{entire surface}";
	char pszFlags[512] = {0};
	if( lpDestRect ) {
		pszRect = va( "{%i,%i,%i,%i}", lpDestRect->left, lpDestRect->top, lpDestRect->right, lpDestRect->bottom );
	}
	if( dwFlags ) {
		FormatLockFlags( dwFlags, pszFlags, sizeof(pszFlags) );
	}
	ADD_LOG( "IDirectDrawSurface (%p)::Lock( %s, %s )", PRINT_DEV, pszRect, pszFlags );
	return (this->*oLock)( lpDestRect, lpDDSurfaceDesc, dwFlags, hEvent );
}

HRESULT CDirectDrawSurface::ReleaseDC( HDC hDC )
{
	ADD_LOG( "IDirectDrawSurface (%p)::ReleaseDC()", PRINT_DEV );
	return (this->*oReleaseDC)( hDC );
}

HRESULT CDirectDrawSurface::Restore()
{
	ADD_LOG( "IDirectDrawSurface (%p)::Restore()", PRINT_DEV );
	return (this->*oRestore)();
}

HRESULT CDirectDrawSurface::SetClipper( LPDIRECTDRAWCLIPPER lpDDClipper )
{
	ADD_LOG( "IDirectDrawSurface (%p)::SetClipper()", PRINT_DEV );
	return (this->*oSetClipper)( lpDDClipper );
}

HRESULT CDirectDrawSurface::SetColorKey( DWORD dwFlags, LPDDCOLORKEY lpDDColorKey )
{
	char pszColorFlag[512] = {0};
	char* pszColor = "{0}";
	if( lpDDColorKey ) {
		pszColor = va( "{0x%x,0x%x}", lpDDColorKey->dwColorSpaceLowValue, lpDDColorKey->dwColorSpaceHighValue );
	}
	FormatColorkeyFlags( dwFlags, pszColorFlag, sizeof(pszColorFlag) );
	ADD_LOG( "IDirectDrawSurface (%p)::SetColorKey( %s, %s )", PRINT_DEV, pszColorFlag, pszColor );
	return (this->*oSetColorKey)( dwFlags, lpDDColorKey );
}

HRESULT CDirectDrawSurface::SetOverlayPosition( LONG lX, LONG lY )
{
	ADD_LOG( "IDirectDrawSurface (%p)::SetOverlayPosition()", PRINT_DEV );
	return (this->*oSetOverlayPosition)( lX, lY );
}

HRESULT CDirectDrawSurface::SetPalette( LPDIRECTDRAWPALETTE lpDDPalette )
{
	ADD_LOG( "IDirectDrawSurface (%p)::SetPalette()", PRINT_DEV );
	return (this->*oSetPalette)( lpDDPalette );
}

HRESULT CDirectDrawSurface::Unlock( LPVOID lpVoid )
{
	LPRECT lpRect = (LPRECT)lpVoid;
	char* pszRect = "{entire surface}";
//	char pszFlags[512] = {0};
	if( lpRect ) {
		pszRect = va( "{%i,%i,%i,%i}", lpRect->left, lpRect->top, lpRect->right, lpRect->bottom );
	}
	ADD_LOG( "IDirectDrawSurface (%p)::Unlock( %s )", PRINT_DEV, pszRect );
	return (this->*oUnlock)( lpRect );
}

HRESULT CDirectDrawSurface::UpdateOverlay( LPRECT lpSrcRect, LPDIRECTDRAWSURFACE lpDDDestSurface, LPRECT lpDestRect, DWORD dwFlags, LPDDOVERLAYFX lpDDOverlayFx )
{
	ADD_LOG( "IDirectDrawSurface (%p)::UpdateOverlay()", PRINT_DEV );
	return (this->*oUpdateOverlay)( lpSrcRect, lpDDDestSurface, lpDestRect, dwFlags, lpDDOverlayFx );
}

HRESULT CDirectDrawSurface::UpdateOverlayDisplay( DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface (%p)::UpdateOverlayDisplay()", PRINT_DEV );
	return (this->*oUpdateOverlayDisplay)( dwFlags );
}

HRESULT CDirectDrawSurface::UpdateOverlayZOrder( DWORD dwFlags, LPDIRECTDRAWSURFACE lpDDSReference )
{
	ADD_LOG( "IDirectDrawSurface (%p)::UpdateOverlayZOrder()", PRINT_DEV );
	return (this->*oUpdateOverlayZOrder)( dwFlags, lpDDSReference );
}


void Device_HookVMTSurf( bool first )
{
	DWORD dwOld = 0;
	VirtualProtect( CDirectDrawSurface_vtable, 36*4, PAGE_EXECUTE_READWRITE, &dwOld );

	HOOK_VTABLE(QueryInterface,0);
	HOOK_VTABLE(AddRef,1);
	HOOK_VTABLE(Release,2);

	HOOK_VTABLE(AddAttachedSurface,3);
	HOOK_VTABLE(AddOverlayDirtyRect,4);
	HOOK_VTABLE(Blt,5);
	HOOK_VTABLE(BltBatch,6);
	HOOK_VTABLE(BltFast,7);
	HOOK_VTABLE(DeleteAttachedSurface,8);
	HOOK_VTABLE(EnumAttachedSurfaces,9);
	HOOK_VTABLE(EnumOverlayZOrders,10);
	HOOK_VTABLE(Flip,11);
	HOOK_VTABLE(GetAttachedSurface,12);
	HOOK_VTABLE(GetBltStatus,13);
	HOOK_VTABLE(GetCaps,14);
	HOOK_VTABLE(GetClipper,15);
	HOOK_VTABLE(GetColorKey,16);
	HOOK_VTABLE(GetDC,17);
	HOOK_VTABLE(GetFlipStatus,18);
	HOOK_VTABLE(GetOverlayPosition,19);
	HOOK_VTABLE(GetPalette,20);
	HOOK_VTABLE(GetPixelFormat,21);
	HOOK_VTABLE(GetSurfaceDesc,22);
	HOOK_VTABLE(Initialize,23);
	HOOK_VTABLE(IsLost,24);
	HOOK_VTABLE(Lock,25);
	HOOK_VTABLE(ReleaseDC,26);
	HOOK_VTABLE(Restore,27);
	HOOK_VTABLE(SetClipper,28);
	HOOK_VTABLE(SetColorKey,29);
	HOOK_VTABLE(SetOverlayPosition,30);
	HOOK_VTABLE(SetPalette,31);
	HOOK_VTABLE(Unlock,32);
	HOOK_VTABLE(UpdateOverlay,33);
	HOOK_VTABLE(UpdateOverlayDisplay,34);
	HOOK_VTABLE(UpdateOverlayZOrder,35);

	VirtualProtect( CDirectDrawSurface_vtable, 36*4, dwOld, &dwOld );
}



void Device_HookVMT( IDirectDrawSurface* ptr )
{
	if( ptr && ptr != INVALID_HANDLE_VALUE ) {
		CDirectDrawSurface_vtable = *(VFPTR**)ptr;
		Device_HookVMTSurf( true );
	} else {
		warning( "Device_HookVMT::IDirectDrawSurface invalid PTR: 0x%x", ptr );
	}
}
