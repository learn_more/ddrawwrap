/***************************************************************
 * Project: DDrawWrap
 * File: IDirectDrawSurface3.cpp
 * Copyright � learn_more
 */

#include "../winclude.h"


class CDirectDrawSurface3 {
public:
/*** IUnknown methods ***/
HRESULT __stdcall QueryInterface( REFIID riid, LPVOID* ppvObj );
ULONG __stdcall AddRef();
ULONG __stdcall Release();
/*** IDirectDraw methods ***/
HRESULT __stdcall AddAttachedSurface( LPDIRECTDRAWSURFACE3 lpDDSAttachedSurface );
HRESULT __stdcall AddOverlayDirtyRect( LPRECT lpRect );
HRESULT __stdcall Blt( LPRECT lpDestRect, LPDIRECTDRAWSURFACE3 lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwFlags, LPDDBLTFX lpDDBltFx );
HRESULT __stdcall BltBatch( LPDDBLTBATCH lpDDBltBatch, DWORD dwCount, DWORD dwFlags );
HRESULT __stdcall BltFast( DWORD dwX, DWORD dwY, LPDIRECTDRAWSURFACE3 lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwTrans );
HRESULT __stdcall DeleteAttachedSurface( DWORD dwFlags, LPDIRECTDRAWSURFACE3 lpDDSAttachedSurface );
HRESULT __stdcall EnumAttachedSurfaces( LPVOID lpContext, LPDDENUMSURFACESCALLBACK lpEnumSurfacesCallback );
HRESULT __stdcall EnumOverlayZOrders( DWORD dwFlags, LPVOID lpContext, LPDDENUMSURFACESCALLBACK lpfnCallback );
HRESULT __stdcall Flip( LPDIRECTDRAWSURFACE3 lpDDSurfaceTargetOverride, DWORD dwFlags );
HRESULT __stdcall GetAttachedSurface( LPDDSCAPS lpDDSCaps, LPDIRECTDRAWSURFACE3* lplpDDAttachedSurface );
HRESULT __stdcall GetBltStatus( DWORD dwFlags );
HRESULT __stdcall GetCaps( LPDDSCAPS lpDDSCaps );
HRESULT __stdcall GetClipper( LPDIRECTDRAWCLIPPER* lplpDDClipper );
HRESULT __stdcall GetColorKey( DWORD dwFlags, LPDDCOLORKEY lpDDColorKey );
HRESULT __stdcall GetDC( HDC* lphDC );
HRESULT __stdcall GetFlipStatus( DWORD dwFlags );
HRESULT __stdcall GetOverlayPosition( LPLONG lplX, LPLONG lplY );
HRESULT __stdcall GetPalette( LPDIRECTDRAWPALETTE* lplpDDPalette );
HRESULT __stdcall GetPixelFormat( LPDDPIXELFORMAT lpDDPixelFormat );
HRESULT __stdcall GetSurfaceDesc( LPDDSURFACEDESC lpDDSurfaceDesc );
HRESULT __stdcall Initialize( LPDIRECTDRAW lpDD, LPDDSURFACEDESC lpDDSurfaceDesc );
HRESULT __stdcall IsLost();
HRESULT __stdcall Lock( LPRECT lpDestRect, LPDDSURFACEDESC lpDDSurfaceDesc, DWORD dwFlags, HANDLE hEvent );
HRESULT __stdcall ReleaseDC( HDC hDC );
HRESULT __stdcall Restore();
HRESULT __stdcall SetClipper( LPDIRECTDRAWCLIPPER lpDDClipper );
HRESULT __stdcall SetColorKey( DWORD dwFlags, LPDDCOLORKEY lpDDColorKey );
HRESULT __stdcall SetOverlayPosition( LONG lX, LONG lY );
HRESULT __stdcall SetPalette( LPDIRECTDRAWPALETTE lpDDPalette );
HRESULT __stdcall Unlock( LPVOID lpRect );
HRESULT __stdcall UpdateOverlay( LPRECT lpSrcRect, LPDIRECTDRAWSURFACE3 lpDDDestSurface, LPRECT lpDestRect, DWORD dwFlags, LPDDOVERLAYFX lpDDOverlayFx );
HRESULT __stdcall UpdateOverlayDisplay( DWORD dwFlags );
HRESULT __stdcall UpdateOverlayZOrder( DWORD dwFlags, LPDIRECTDRAWSURFACE3 lpDDSReference );
/*** Added in the V2 Interface ***/
HRESULT __stdcall GetDDInterface( LPVOID* lplpDD );
HRESULT __stdcall PageLock( DWORD dwFlags );
HRESULT __stdcall PageUnlock( DWORD dwFlags );
/*** Added in the V3 Interface ***/
HRESULT __stdcall SetSurfaceDesc( LPDDSURFACEDESC lpDDSurfaceDesc, DWORD dwFlags );
};


/*** IUnknown methods ***/
HRESULT (__stdcall CDirectDrawSurface3::*oQueryInterface)( REFIID riid, LPVOID* ppvObj );
ULONG (__stdcall CDirectDrawSurface3::*oAddRef)();
ULONG (__stdcall CDirectDrawSurface3::*oRelease)();
/*** IDirectDraw methods ***/
HRESULT (__stdcall CDirectDrawSurface3::*oAddAttachedSurface)( LPDIRECTDRAWSURFACE3 lpDDSAttachedSurface );
HRESULT (__stdcall CDirectDrawSurface3::*oAddOverlayDirtyRect)( LPRECT lpRect );
HRESULT (__stdcall CDirectDrawSurface3::*oBlt)( LPRECT lpDestRect, LPDIRECTDRAWSURFACE3 lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwFlags, LPDDBLTFX lpDDBltFx );
HRESULT (__stdcall CDirectDrawSurface3::*oBltBatch)( LPDDBLTBATCH lpDDBltBatch, DWORD dwCount, DWORD dwFlags );
HRESULT (__stdcall CDirectDrawSurface3::*oBltFast)( DWORD dwX, DWORD dwY, LPDIRECTDRAWSURFACE3 lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwTrans );
HRESULT (__stdcall CDirectDrawSurface3::*oDeleteAttachedSurface)( DWORD dwFlags, LPDIRECTDRAWSURFACE3 lpDDSAttachedSurface );
HRESULT (__stdcall CDirectDrawSurface3::*oEnumAttachedSurfaces)( LPVOID lpContext, LPDDENUMSURFACESCALLBACK lpEnumSurfacesCallback );
HRESULT (__stdcall CDirectDrawSurface3::*oEnumOverlayZOrders)( DWORD dwFlags, LPVOID lpContext, LPDDENUMSURFACESCALLBACK lpfnCallback );
HRESULT (__stdcall CDirectDrawSurface3::*oFlip)( LPDIRECTDRAWSURFACE3 lpDDSurfaceTargetOverride, DWORD dwFlags );
HRESULT (__stdcall CDirectDrawSurface3::*oGetAttachedSurface)( LPDDSCAPS lpDDSCaps, LPDIRECTDRAWSURFACE3* lplpDDAttachedSurface );
HRESULT (__stdcall CDirectDrawSurface3::*oGetBltStatus)( DWORD dwFlags );
HRESULT (__stdcall CDirectDrawSurface3::*oGetCaps)( LPDDSCAPS lpDDSCaps );
HRESULT (__stdcall CDirectDrawSurface3::*oGetClipper)( LPDIRECTDRAWCLIPPER* lplpDDClipper );
HRESULT (__stdcall CDirectDrawSurface3::*oGetColorKey)( DWORD dwFlags, LPDDCOLORKEY lpDDColorKey );
HRESULT (__stdcall CDirectDrawSurface3::*oGetDC)( HDC* lphDC );
HRESULT (__stdcall CDirectDrawSurface3::*oGetFlipStatus)( DWORD dwFlags );
HRESULT (__stdcall CDirectDrawSurface3::*oGetOverlayPosition)( LPLONG lplX, LPLONG lplY );
HRESULT (__stdcall CDirectDrawSurface3::*oGetPalette)( LPDIRECTDRAWPALETTE* lplpDDPalette );
HRESULT (__stdcall CDirectDrawSurface3::*oGetPixelFormat)( LPDDPIXELFORMAT lpDDPixelFormat );
HRESULT (__stdcall CDirectDrawSurface3::*oGetSurfaceDesc)( LPDDSURFACEDESC lpDDSurfaceDesc );
HRESULT (__stdcall CDirectDrawSurface3::*oInitialize)( LPDIRECTDRAW lpDD, LPDDSURFACEDESC lpDDSurfaceDesc );
HRESULT (__stdcall CDirectDrawSurface3::*oIsLost)();
HRESULT (__stdcall CDirectDrawSurface3::*oLock)( LPRECT lpDestRect, LPDDSURFACEDESC lpDDSurfaceDesc, DWORD dwFlags, HANDLE hEvent );
HRESULT (__stdcall CDirectDrawSurface3::*oReleaseDC)( HDC hDC );
HRESULT (__stdcall CDirectDrawSurface3::*oRestore)();
HRESULT (__stdcall CDirectDrawSurface3::*oSetClipper)( LPDIRECTDRAWCLIPPER lpDDClipper );
HRESULT (__stdcall CDirectDrawSurface3::*oSetColorKey)( DWORD dwFlags, LPDDCOLORKEY lpDDColorKey );
HRESULT (__stdcall CDirectDrawSurface3::*oSetOverlayPosition)( LONG lX, LONG lY );
HRESULT (__stdcall CDirectDrawSurface3::*oSetPalette)( LPDIRECTDRAWPALETTE lpDDPalette );
HRESULT (__stdcall CDirectDrawSurface3::*oUnlock)( LPVOID lpRect );
HRESULT (__stdcall CDirectDrawSurface3::*oUpdateOverlay)( LPRECT lpSrcRect, LPDIRECTDRAWSURFACE3 lpDDDestSurface, LPRECT lpDestRect, DWORD dwFlags, LPDDOVERLAYFX lpDDOverlayFx );
HRESULT (__stdcall CDirectDrawSurface3::*oUpdateOverlayDisplay)( DWORD dwFlags );
HRESULT (__stdcall CDirectDrawSurface3::*oUpdateOverlayZOrder)( DWORD dwFlags, LPDIRECTDRAWSURFACE3 lpDDSReference );
/*** Added in the V2 Interface ***/
HRESULT (__stdcall CDirectDrawSurface3::*oGetDDInterface)( LPVOID* lplpDD );
HRESULT (__stdcall CDirectDrawSurface3::*oPageLock)( DWORD dwFlags );
HRESULT (__stdcall CDirectDrawSurface3::*oPageUnlock)( DWORD dwFlags );
/*** Added in the V3 Interface ***/
HRESULT (__stdcall CDirectDrawSurface3::*oSetSurfaceDesc)( LPDDSURFACEDESC lpDDSurfaceDesc, DWORD dwFlags );


typedef void (__stdcall CDirectDrawSurface3::*VFPTR)();
VFPTR* CDirectDrawSurface3_vtable = 0;

#define HOOK_PREFIX	CDirectDrawSurface3



HRESULT CDirectDrawSurface3::QueryInterface( REFIID riid, LPVOID* ppvObj )
{
	char buf[100];
	FormatGUID( riid, buf, sizeof(buf) );
	ADD_LOG( "IDirectDrawSurface3(%p)::QueryInterface( %s )", PRINT_DEV, buf );
	return (this->*oQueryInterface)( riid, ppvObj );
}

ULONG CDirectDrawSurface3::AddRef()
{
	ULONG pRef = (this->*oAddRef)();
	ADD_LOG( "IDirectDrawSurface3(%p)::AddRef( %i )", PRINT_DEV, pRef );
	return pRef;
}

ULONG CDirectDrawSurface3::Release()
{
	ULONG uRet = (this->*oRelease)();
	ADD_LOG( "IDirectDrawSurface3(%p)::Release( %i )", PRINT_DEV, uRet );
	return uRet;
}

HRESULT CDirectDrawSurface3::AddAttachedSurface( LPDIRECTDRAWSURFACE3 lpDDSAttachedSurface )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::AddAttachedSurface()", PRINT_DEV );
	return (this->*oAddAttachedSurface)( lpDDSAttachedSurface );
}

HRESULT CDirectDrawSurface3::AddOverlayDirtyRect( LPRECT lpRect )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::AddOverlayDirtyRect()", PRINT_DEV );
	return (this->*oAddOverlayDirtyRect)( lpRect );
}

HRESULT CDirectDrawSurface3::Blt( LPRECT lpDestRect, LPDIRECTDRAWSURFACE3 lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwFlags, LPDDBLTFX lpDDBltFx )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::Blt()", PRINT_DEV );
	return (this->*oBlt)( lpDestRect, lpDDSrcSurface, lpSrcRect, dwFlags, lpDDBltFx );
}

HRESULT CDirectDrawSurface3::BltBatch( LPDDBLTBATCH lpDDBltBatch, DWORD dwCount, DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::BltBatch()", PRINT_DEV );
	return (this->*oBltBatch)( lpDDBltBatch, dwCount, dwFlags );
}

HRESULT CDirectDrawSurface3::BltFast( DWORD dwX, DWORD dwY, LPDIRECTDRAWSURFACE3 lpDDSrcSurface, LPRECT lpSrcRect, DWORD dwTrans )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::BltFast()", PRINT_DEV );
	return (this->*oBltFast)( dwX, dwY, lpDDSrcSurface, lpSrcRect, dwTrans );
}

HRESULT CDirectDrawSurface3::DeleteAttachedSurface( DWORD dwFlags, LPDIRECTDRAWSURFACE3 lpDDSAttachedSurface )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::DeleteAttachedSurface()", PRINT_DEV );
	return (this->*oDeleteAttachedSurface)( dwFlags, lpDDSAttachedSurface );
}

HRESULT CDirectDrawSurface3::EnumAttachedSurfaces( LPVOID lpContext, LPDDENUMSURFACESCALLBACK lpEnumSurfacesCallback )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::EnumAttachedSurfaces()", PRINT_DEV );
	return (this->*oEnumAttachedSurfaces)( lpContext, lpEnumSurfacesCallback );
}

HRESULT CDirectDrawSurface3::EnumOverlayZOrders( DWORD dwFlags, LPVOID lpContext, LPDDENUMSURFACESCALLBACK lpfnCallback )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::EnumOverlayZOrders()", PRINT_DEV );
	return (this->*oEnumOverlayZOrders)( dwFlags, lpContext, lpfnCallback );
}

HRESULT CDirectDrawSurface3::Flip( LPDIRECTDRAWSURFACE3 lpDDSurfaceTargetOverride, DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::Flip()", PRINT_DEV );
	return (this->*oFlip)( lpDDSurfaceTargetOverride, dwFlags );
}

HRESULT CDirectDrawSurface3::GetAttachedSurface( LPDDSCAPS lpDDSCaps, LPDIRECTDRAWSURFACE3* lplpDDAttachedSurface )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::GetAttachedSurface()", PRINT_DEV );
	return (this->*oGetAttachedSurface)( lpDDSCaps, lplpDDAttachedSurface );
}

HRESULT CDirectDrawSurface3::GetBltStatus( DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::GetBltStatus()", PRINT_DEV );
	return (this->*oGetBltStatus)( dwFlags );
}

HRESULT CDirectDrawSurface3::GetCaps( LPDDSCAPS lpDDSCaps )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::GetCaps()", PRINT_DEV );
	return (this->*oGetCaps)( lpDDSCaps );
}

HRESULT CDirectDrawSurface3::GetClipper( LPDIRECTDRAWCLIPPER* lplpDDClipper )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::GetClipper()", PRINT_DEV );
	return (this->*oGetClipper)( lplpDDClipper );
}

HRESULT CDirectDrawSurface3::GetColorKey( DWORD dwFlags, LPDDCOLORKEY lpDDColorKey )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::GetColorKey()", PRINT_DEV );
	return (this->*oGetColorKey)( dwFlags, lpDDColorKey );
}

HRESULT CDirectDrawSurface3::GetDC( HDC* lphDC )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::GetDC()", PRINT_DEV );
	return (this->*oGetDC)( lphDC );
}

HRESULT CDirectDrawSurface3::GetFlipStatus( DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::GetFlipStatus()", PRINT_DEV );
	return (this->*oGetFlipStatus)( dwFlags );
}

HRESULT CDirectDrawSurface3::GetOverlayPosition( LPLONG lplX, LPLONG lplY )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::GetOverlayPosition()", PRINT_DEV );
	return (this->*oGetOverlayPosition)( lplX, lplY );
}

HRESULT CDirectDrawSurface3::GetPalette( LPDIRECTDRAWPALETTE* lplpDDPalette )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::GetPalette()", PRINT_DEV );
	return (this->*oGetPalette)( lplpDDPalette );
}

HRESULT CDirectDrawSurface3::GetPixelFormat( LPDDPIXELFORMAT lpDDPixelFormat )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::GetPixelFormat()", PRINT_DEV );
	return (this->*oGetPixelFormat)( lpDDPixelFormat );
}

HRESULT CDirectDrawSurface3::GetSurfaceDesc( LPDDSURFACEDESC lpDDSurfaceDesc )
{
	HRESULT hRet = (this->*oGetSurfaceDesc)( lpDDSurfaceDesc );
	char szSurfDesc[1024];
	FormatSurfaceDesc( lpDDSurfaceDesc, szSurfDesc, sizeof(szSurfDesc) );
	ADD_LOG( "IDirectDrawSurface3(%p)::GetSurfaceDesc( %s ) = 0x%x", PRINT_DEV, szSurfDesc, hRet );
	return hRet;
}

HRESULT CDirectDrawSurface3::Initialize( LPDIRECTDRAW lpDD, LPDDSURFACEDESC lpDDSurfaceDesc )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::Initialize()", PRINT_DEV );
	return (this->*oInitialize)( lpDD, lpDDSurfaceDesc );
}

HRESULT CDirectDrawSurface3::IsLost()
{
	ADD_LOG( "IDirectDrawSurface3(%p)::IsLost()", PRINT_DEV );
	return (this->*oIsLost)();
}

HRESULT CDirectDrawSurface3::Lock( LPRECT lpDestRect, LPDDSURFACEDESC lpDDSurfaceDesc, DWORD dwFlags, HANDLE hEvent )
{
	char* pszRect = "{entire surface}";
	char pszFlags[512] = {0};
	if( lpDestRect ) {
		pszRect = va( "{%i,%i,%i,%i}", lpDestRect->left, lpDestRect->top, lpDestRect->right, lpDestRect->bottom );
	}
	if( dwFlags ) {
		FormatLockFlags( dwFlags, pszFlags, sizeof(pszFlags) );
	}
	ADD_LOG( "IDirectDrawSurface3(%p)::Lock( %s, %s )", PRINT_DEV, pszRect, pszFlags );
	return (this->*oLock)( lpDestRect, lpDDSurfaceDesc, dwFlags, hEvent );
}

HRESULT CDirectDrawSurface3::ReleaseDC( HDC hDC )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::ReleaseDC()", PRINT_DEV );
	return (this->*oReleaseDC)( hDC );
}

HRESULT CDirectDrawSurface3::Restore()
{
	ADD_LOG( "IDirectDrawSurface3(%p)::Restore()", PRINT_DEV );
	return (this->*oRestore)();
}

HRESULT CDirectDrawSurface3::SetClipper( LPDIRECTDRAWCLIPPER lpDDClipper )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::SetClipper()", PRINT_DEV );
	return (this->*oSetClipper)( lpDDClipper );
}

HRESULT CDirectDrawSurface3::SetColorKey( DWORD dwFlags, LPDDCOLORKEY lpDDColorKey )
{
	char pszColorFlag[512] = {0};
	char* pszColor = "{0}";
	if( lpDDColorKey ) {
		pszColor = va( "{0x%x,0x%x}", lpDDColorKey->dwColorSpaceLowValue, lpDDColorKey->dwColorSpaceHighValue );
	}
	FormatColorkeyFlags( dwFlags, pszColorFlag, sizeof(pszColorFlag) );
	ADD_LOG( "IDirectDrawSurface3(%p)::SetColorKey( %s, %s )", PRINT_DEV, pszColorFlag, pszColor );
	return (this->*oSetColorKey)( dwFlags, lpDDColorKey );
}

HRESULT CDirectDrawSurface3::SetOverlayPosition( LONG lX, LONG lY )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::SetOverlayPosition()", PRINT_DEV );
	return (this->*oSetOverlayPosition)( lX, lY );
}

HRESULT CDirectDrawSurface3::SetPalette( LPDIRECTDRAWPALETTE lpDDPalette )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::SetPalette()", PRINT_DEV );
	return (this->*oSetPalette)( lpDDPalette );
}

HRESULT CDirectDrawSurface3::Unlock( LPVOID lpVoid )
{
	LPRECT lpRect = (LPRECT)lpVoid;
	char* pszRect = "{entire surface}";
//	char pszFlags[512] = {0};
	if( lpRect ) {
		pszRect = va( "{%i,%i,%i,%i}", lpRect->left, lpRect->top, lpRect->right, lpRect->bottom );
	}
	ADD_LOG( "IDirectDrawSurface3(%p)::Unlock( %s )", PRINT_DEV, pszRect );
	return (this->*oUnlock)( lpRect );
}

HRESULT CDirectDrawSurface3::UpdateOverlay( LPRECT lpSrcRect, LPDIRECTDRAWSURFACE3 lpDDDestSurface, LPRECT lpDestRect, DWORD dwFlags, LPDDOVERLAYFX lpDDOverlayFx )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::UpdateOverlay()", PRINT_DEV );
	return (this->*oUpdateOverlay)( lpSrcRect, lpDDDestSurface, lpDestRect, dwFlags, lpDDOverlayFx );
}

HRESULT CDirectDrawSurface3::UpdateOverlayDisplay( DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::UpdateOverlayDisplay()", PRINT_DEV );
	return (this->*oUpdateOverlayDisplay)( dwFlags );
}

HRESULT CDirectDrawSurface3::UpdateOverlayZOrder( DWORD dwFlags, LPDIRECTDRAWSURFACE3 lpDDSReference )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::UpdateOverlayZOrder()", PRINT_DEV );
	return (this->*oUpdateOverlayZOrder)( dwFlags, lpDDSReference );
}

HRESULT CDirectDrawSurface3::GetDDInterface( LPVOID* lplpDD )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::GetDDInterface()", PRINT_DEV );
	return (this->*oGetDDInterface)( lplpDD );
}

HRESULT CDirectDrawSurface3::PageLock( DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::PageLock()", PRINT_DEV );
	return (this->*oPageLock)( dwFlags );
}

HRESULT CDirectDrawSurface3::PageUnlock( DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::PageUnlock()", PRINT_DEV );
	return (this->*oPageUnlock)( dwFlags );
}

HRESULT CDirectDrawSurface3::SetSurfaceDesc( LPDDSURFACEDESC lpDDSurfaceDesc, DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawSurface3(%p)::PageUnlock()", PRINT_DEV );
	return (this->*oSetSurfaceDesc)( lpDDSurfaceDesc, dwFlags );
}


void Device_HookVMTSurf3( bool first )
{
	DWORD dwOld = 0;
	VirtualProtect( CDirectDrawSurface3_vtable, 40*4, PAGE_EXECUTE_READWRITE, &dwOld );

	HOOK_VTABLE(QueryInterface,0);
	HOOK_VTABLE(AddRef,1);
	HOOK_VTABLE(Release,2);

	HOOK_VTABLE(AddAttachedSurface,3);
	HOOK_VTABLE(AddOverlayDirtyRect,4);
	HOOK_VTABLE(Blt,5);
	HOOK_VTABLE(BltBatch,6);
	HOOK_VTABLE(BltFast,7);
	HOOK_VTABLE(DeleteAttachedSurface,8);
	HOOK_VTABLE(EnumAttachedSurfaces,9);
	HOOK_VTABLE(EnumOverlayZOrders,10);
	HOOK_VTABLE(Flip,11);
	HOOK_VTABLE(GetAttachedSurface,12);
	HOOK_VTABLE(GetBltStatus,13);
	HOOK_VTABLE(GetCaps,14);
	HOOK_VTABLE(GetClipper,15);
	HOOK_VTABLE(GetColorKey,16);
	HOOK_VTABLE(GetDC,17);
	HOOK_VTABLE(GetFlipStatus,18);
	HOOK_VTABLE(GetOverlayPosition,19);
	HOOK_VTABLE(GetPalette,20);
	HOOK_VTABLE(GetPixelFormat,21);
	HOOK_VTABLE(GetSurfaceDesc,22);
	HOOK_VTABLE(Initialize,23);
	HOOK_VTABLE(IsLost,24);
	HOOK_VTABLE(Lock,25);
	HOOK_VTABLE(ReleaseDC,26);
	HOOK_VTABLE(Restore,27);
	HOOK_VTABLE(SetClipper,28);
	HOOK_VTABLE(SetColorKey,29);
	HOOK_VTABLE(SetOverlayPosition,30);
	HOOK_VTABLE(SetPalette,31);
	HOOK_VTABLE(Unlock,32);
	HOOK_VTABLE(UpdateOverlay,33);
	HOOK_VTABLE(UpdateOverlayDisplay,34);
	HOOK_VTABLE(UpdateOverlayZOrder,35);

	HOOK_VTABLE(GetDDInterface,36);
	HOOK_VTABLE(PageLock,37);
	HOOK_VTABLE(PageUnlock,38);

	HOOK_VTABLE(SetSurfaceDesc,39);

	VirtualProtect( CDirectDrawSurface3_vtable, 40*4, dwOld, &dwOld );
}


void Device_HookVMT( IDirectDrawSurface3* ptr )
{
	if( ptr && ptr != INVALID_HANDLE_VALUE ) {
		CDirectDrawSurface3_vtable = *(VFPTR**)ptr;
		Device_HookVMTSurf3( true );
	} else {
		warning( "Device_HookVMT::IDirectDrawSurface3 invalid PTR: 0x%x", ptr );
	}
}
