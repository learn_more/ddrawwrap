DDraw wrapper v3 for the following interfaces:
IDirectDraw, IDirectDraw2, IDirectDraw4, IDirectDraw7
IDirectDrawSurface, IDirectDrawSurface2, IDirectDrawSurface3, IDirectDrawSurface4, IDirectDrawSurface7
IDirectDrawPalette, IDirectDrawClipper, IDirectDrawColorControl, IDirectDrawGammaControl


The project is unfinished, most functions dont log arguments, add what you need :)
See config.h how to enable/disable logging for all functions,
 or only the wrapper (called) functions (this will still log some hooking information)

Usage: 
step 1: get lordpe (google)
step 1a: compile in Release!
step 2: copy the wrapper dll (wrapmodule.dll) to the game's executable directory
step 3: load your target app in lordpe (PE editor)
step 4: click on 'Directories', then '...' behind 'Import table'
step 5: Right mouse button on dll list --> 'Add import'
step 6: dll: 'wrapmodule.dll', api: 'load' --> click in '+'
step 7: [ok] --> [x] --> [save] --> [save]


Changes (v3):
* Complete rewrite (95%), new way of hooking and tracing objects.
* Static object trace, no 'trace' objects used --> no more crashes.
* Don't log code executed while hooking


Fixes (v2-fix):
* Clipper select bug
* Palette select bug
* Reference count out of sync with real obj
* Added minidump creation