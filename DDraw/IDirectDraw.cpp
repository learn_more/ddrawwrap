/***************************************************************
 * Project: DDrawWrap
 * File: IDirectDraw.cpp
 * Copyright � learn_more
 */

#include "../winclude.h"

class CDirectDraw {
public:
/*** IUnknown methods ***/
HRESULT __stdcall QueryInterface( REFIID riid, LPVOID* ppvObj );
ULONG __stdcall AddRef();
ULONG __stdcall Release();
/*** IDirectDraw methods ***/
HRESULT __stdcall Compact();
HRESULT __stdcall CreateClipper( DWORD dwFlags, LPDIRECTDRAWCLIPPER* lplpDDClipper, IUnknown* pUnkOuter );
HRESULT __stdcall CreatePalette( DWORD dwFlags, LPPALETTEENTRY lpDDColorArray, LPDIRECTDRAWPALETTE* lplpDDPalette, IUnknown* pUnkOuter );
HRESULT __stdcall CreateSurface( LPDDSURFACEDESC lpDDSurfaceDesc, LPDIRECTDRAWSURFACE* lplpDDSurface, IUnknown* pUnkOuter );
HRESULT __stdcall DuplicateSurface( LPDIRECTDRAWSURFACE lpDDSurface, LPDIRECTDRAWSURFACE* lplpDupDDSurface );
HRESULT __stdcall EnumDisplayModes( DWORD dwFlags, LPDDSURFACEDESC lpDDSurfaceDesc, LPVOID lpContext, LPDDENUMMODESCALLBACK lpEnumModesCallback );
HRESULT __stdcall EnumSurfaces( DWORD dwFlags, LPDDSURFACEDESC lpDDSD, LPVOID lpContext, LPDDENUMSURFACESCALLBACK lpEnumSurfacesCallback );
HRESULT __stdcall FlipToGDISurface();
HRESULT __stdcall GetCaps( LPDDCAPS lpDDDriverCaps, LPDDCAPS lpDDHELCaps );
HRESULT __stdcall GetDisplayMode( LPDDSURFACEDESC lpDDSurfaceDesc );
HRESULT __stdcall GetFourCCCodes( LPDWORD lpNumCodes, LPDWORD lpCodes );
HRESULT __stdcall GetGDISurface( LPDIRECTDRAWSURFACE* lplpGDIDDSSurface );
HRESULT __stdcall GetMonitorFrequency( LPDWORD lpdwFrequency );
HRESULT __stdcall GetScanLine( LPDWORD lpdwScanLine );
HRESULT __stdcall GetVerticalBlankStatus( LPBOOL lpbIsInVB );
HRESULT __stdcall Initialize( GUID* lpGUID );
HRESULT __stdcall RestoreDisplayMode();
HRESULT __stdcall SetCooperativeLevel( HWND hWnd, DWORD dwFlags );
HRESULT __stdcall SetDisplayMode( DWORD dwWidth, DWORD dwHeight, DWORD dwBPP );
HRESULT __stdcall WaitForVerticalBlank( DWORD dwFlags, HANDLE hEvent );
};



HRESULT (__stdcall CDirectDraw::*oQueryInterface)( REFIID riid, LPVOID* ppvObj );
ULONG (__stdcall CDirectDraw::*oAddRef)();
ULONG (__stdcall CDirectDraw::*oRelease)();
/*** IDirectDraw methods ***/
HRESULT (__stdcall CDirectDraw::*oCompact)();
HRESULT (__stdcall CDirectDraw::*oCreateClipper)( DWORD dwFlags, LPDIRECTDRAWCLIPPER* lplpDDClipper, IUnknown* pUnkOuter );
HRESULT (__stdcall CDirectDraw::*oCreatePalette)( DWORD dwFlags, LPPALETTEENTRY lpDDColorArray, LPDIRECTDRAWPALETTE* lplpDDPalette, IUnknown* pUnkOuter );
HRESULT (__stdcall CDirectDraw::*oCreateSurface)( LPDDSURFACEDESC lpDDSurfaceDesc, LPDIRECTDRAWSURFACE* lplpDDSurface, IUnknown* pUnkOuter );
HRESULT (__stdcall CDirectDraw::*oDuplicateSurface)( LPDIRECTDRAWSURFACE lpDDSurface, LPDIRECTDRAWSURFACE* lplpDupDDSurface );
HRESULT (__stdcall CDirectDraw::*oEnumDisplayModes)( DWORD dwFlags, LPDDSURFACEDESC lpDDSurfaceDesc, LPVOID lpContext, LPDDENUMMODESCALLBACK lpEnumModesCallback );
HRESULT (__stdcall CDirectDraw::*oEnumSurfaces)( DWORD dwFlags, LPDDSURFACEDESC lpDDSD, LPVOID lpContext, LPDDENUMSURFACESCALLBACK lpEnumSurfacesCallback );
HRESULT (__stdcall CDirectDraw::*oFlipToGDISurface)();
HRESULT (__stdcall CDirectDraw::*oGetCaps)( LPDDCAPS lpDDDriverCaps, LPDDCAPS lpDDHELCaps );
HRESULT (__stdcall CDirectDraw::*oGetDisplayMode)( LPDDSURFACEDESC lpDDSurfaceDesc );
HRESULT (__stdcall CDirectDraw::*oGetFourCCCodes)( LPDWORD lpNumCodes, LPDWORD lpCodes );
HRESULT (__stdcall CDirectDraw::*oGetGDISurface)( LPDIRECTDRAWSURFACE* lplpGDIDDSSurface );
HRESULT (__stdcall CDirectDraw::*oGetMonitorFrequency)( LPDWORD lpdwFrequency );
HRESULT (__stdcall CDirectDraw::*oGetScanLine)( LPDWORD lpdwScanLine );
HRESULT (__stdcall CDirectDraw::*oGetVerticalBlankStatus)( LPBOOL lpbIsInVB );
HRESULT (__stdcall CDirectDraw::*oInitialize)( GUID* lpGUID );
HRESULT (__stdcall CDirectDraw::*oRestoreDisplayMode)();
HRESULT (__stdcall CDirectDraw::*oSetCooperativeLevel)( HWND hWnd, DWORD dwFlags );
HRESULT (__stdcall CDirectDraw::*oSetDisplayMode)( DWORD dwWidth, DWORD dwHeight, DWORD dwBPP );
HRESULT (__stdcall CDirectDraw::*oWaitForVerticalBlank)( DWORD dwFlags, HANDLE hEvent );

typedef void (__stdcall CDirectDraw::*VFPTR)();
VFPTR* CDirectDraw_vtable = 0;

#define HOOK_PREFIX	CDirectDraw


HRESULT CDirectDraw::QueryInterface( REFIID riid, LPVOID* ppvObj )
{
	char buf[100];
	FormatGUID( riid, buf, sizeof(buf) );
	ADD_LOG( "IDirectDraw (%p)::QueryInterface( %s )", PRINT_DEV, buf );
	return (this->*oQueryInterface)( riid, ppvObj );
}

ULONG CDirectDraw::AddRef()
{
	ULONG pRef = (this->*oAddRef)();
	ADD_LOG( "IDirectDraw (%p)::AddRef( %i )", PRINT_DEV, pRef );
	return pRef;
}

ULONG CDirectDraw::Release()
{
	ULONG uRet = (this->*oRelease)();
	ADD_LOG( "IDirectDraw (%p)::Release( %i )", PRINT_DEV, uRet );
	return uRet;
}

HRESULT CDirectDraw::Compact()
{
	ADD_LOG( "IDirectDraw (%p)::Compact()", PRINT_DEV );
	return (this->*oCompact)();
}

HRESULT CDirectDraw::CreateClipper( DWORD dwFlags, LPDIRECTDRAWCLIPPER* lplpDDClipper, IUnknown* pUnkOuter )
{
	HRESULT hRet = (this->*oCreateClipper)( dwFlags, lplpDDClipper, pUnkOuter );
	ADD_LOG( "IDirectDraw (%p)::CreateClipper()", PRINT_DEV );
	return hRet;
}

HRESULT CDirectDraw::CreatePalette( DWORD dwFlags, LPPALETTEENTRY lpDDColorArray, LPDIRECTDRAWPALETTE* lplpDDPalette, IUnknown* pUnkOuter )
{
	HRESULT hRet = (this->*oCreatePalette)( dwFlags, lpDDColorArray, lplpDDPalette, pUnkOuter );
	ADD_LOG( "IDirectDraw (%p)::CreatePalette()", PRINT_DEV );
	return hRet;
}

HRESULT CDirectDraw::CreateSurface( LPDDSURFACEDESC lpDDSurfaceDesc, LPDIRECTDRAWSURFACE* lplpDDSurface, IUnknown* pUnkOuter )
{
	HRESULT hRet = (this->*oCreateSurface)( lpDDSurfaceDesc, lplpDDSurface, pUnkOuter );
	ADD_LOG( "IDirectDraw (%p)::CreateSurface()", PRINT_DEV );
	return hRet;
}

HRESULT CDirectDraw::DuplicateSurface( LPDIRECTDRAWSURFACE lpDDSurface, LPDIRECTDRAWSURFACE* lplpDupDDSurface )
{
	HRESULT hRet = (this->*oDuplicateSurface)( lpDDSurface, lplpDupDDSurface );
	ADD_LOG( "IDirectDraw (%p)::DuplicateSurface()", PRINT_DEV );
	return hRet;
}

HRESULT CDirectDraw::EnumDisplayModes( DWORD dwFlags, LPDDSURFACEDESC lpDDSurfaceDesc, LPVOID lpContext, LPDDENUMMODESCALLBACK lpEnumModesCallback )
{
	ADD_LOG( "IDirectDraw (%p)::EnumDisplayModes()", PRINT_DEV );
	return (this->*oEnumDisplayModes)( dwFlags, lpDDSurfaceDesc, lpContext, lpEnumModesCallback );
}

HRESULT CDirectDraw::EnumSurfaces( DWORD dwFlags, LPDDSURFACEDESC lpDDSD, LPVOID lpContext, LPDDENUMSURFACESCALLBACK lpEnumSurfacesCallback )
{
	ADD_LOG( "IDirectDraw (%p)::EnumSurfaces()", PRINT_DEV );
	return (this->*oEnumSurfaces)( dwFlags, lpDDSD, lpContext, lpEnumSurfacesCallback );
}

HRESULT CDirectDraw::FlipToGDISurface()
{
	ADD_LOG( "IDirectDraw (%p)::FlipToGDISurface()", PRINT_DEV );
	return (this->*oFlipToGDISurface)();
}

HRESULT CDirectDraw::GetCaps( LPDDCAPS lpDDDriverCaps, LPDDCAPS lpDDHELCaps )
{
	HRESULT hRet = (this->*oGetCaps)( lpDDDriverCaps, lpDDHELCaps );
	LogCaps( PRINT_DEV, "IDirectDraw ", hRet, lpDDDriverCaps, lpDDHELCaps );
	return hRet;
}

HRESULT CDirectDraw::GetDisplayMode( LPDDSURFACEDESC lpDDSurfaceDesc )
{
	ADD_LOG( "IDirectDraw (%p)::GetDisplayMode()", PRINT_DEV );
	return (this->*oGetDisplayMode)( lpDDSurfaceDesc );
}

HRESULT CDirectDraw::GetFourCCCodes( LPDWORD lpNumCodes, LPDWORD lpCodes )
{
	ADD_LOG( "IDirectDraw (%p)::GetFourCCCodes()", PRINT_DEV );
	return (this->*oGetFourCCCodes)( lpNumCodes, lpCodes );
}

HRESULT CDirectDraw::GetGDISurface( LPDIRECTDRAWSURFACE* lplpGDIDDSSurface )
{
	ADD_LOG( "IDirectDraw (%p)::GetGDISurface()", PRINT_DEV );
	return (this->*oGetGDISurface)( lplpGDIDDSSurface );
}

HRESULT CDirectDraw::GetMonitorFrequency( LPDWORD lpdwFrequency )
{
	ADD_LOG( "IDirectDraw (%p)::GetMonitorFrequency()", PRINT_DEV );
	return (this->*oGetMonitorFrequency)( lpdwFrequency );
}

HRESULT CDirectDraw::GetScanLine( LPDWORD lpdwScanLine )
{
	ADD_LOG( "IDirectDraw (%p)::GetScanLine()", PRINT_DEV );
	return (this->*oGetScanLine)( lpdwScanLine );
}

HRESULT CDirectDraw::GetVerticalBlankStatus( LPBOOL lpbIsInVB )
{
	ADD_LOG( "IDirectDraw (%p)::GetVerticalBlankStatus()", PRINT_DEV );
	return (this->*oGetVerticalBlankStatus)( lpbIsInVB );
}

HRESULT CDirectDraw::Initialize( GUID* lpGUID )
{
	ADD_LOG( "IDirectDraw (%p)::Initialize()", PRINT_DEV );
	return (this->*oInitialize)( lpGUID );
}

HRESULT CDirectDraw::RestoreDisplayMode()
{
	ADD_LOG( "IDirectDraw (%p)::RestoreDisplayMode()", PRINT_DEV );
	return (this->*oRestoreDisplayMode)();
}

HRESULT CDirectDraw::SetCooperativeLevel( HWND hWnd, DWORD dwFlags )
{
	HRESULT hRet = (this->*oSetCooperativeLevel)( hWnd, dwFlags );
	char buf[512] = {0};
	FormatCooperativeFlags( dwFlags, buf, sizeof(buf) );
	ADD_LOG( "IDirectDraw (%p)::SetCooperativeLevel( hWnd = 0x%x, dwFlags = %s ) = 0x%x",
		PRINT_DEV, hWnd, buf, hRet );
	return hRet;
}

HRESULT CDirectDraw::SetDisplayMode( DWORD dwWidth, DWORD dwHeight, DWORD dwBPP )
{
	ADD_LOG( "IDirectDraw (%p)::SetDisplayMode( dwWidth = %i, dwHeight = %i, dwBPP = %i )", PRINT_DEV, dwWidth, dwHeight, dwBPP );
	return (this->*oSetDisplayMode)( dwWidth, dwHeight, dwBPP );
}

HRESULT CDirectDraw::WaitForVerticalBlank( DWORD dwFlags, HANDLE hEvent )
{
	ADD_LOG( "IDirectDraw (%p)::WaitForVerticalBlank()", PRINT_DEV );
	return (this->*oWaitForVerticalBlank)( dwFlags, hEvent );
}

void Device_HookVMT( bool first )
{
	DWORD dwOld = 0;
	VirtualProtect( CDirectDraw_vtable, 23*4, PAGE_EXECUTE_READWRITE, &dwOld );

	HOOK_VTABLE(QueryInterface,0);
	HOOK_VTABLE(AddRef,1);
	HOOK_VTABLE(Release,2);

	HOOK_VTABLE(Compact,3);
	HOOK_VTABLE(CreateClipper,4);
	HOOK_VTABLE(CreatePalette,5);
	HOOK_VTABLE(CreateSurface,6);
	HOOK_VTABLE(DuplicateSurface,7);
	HOOK_VTABLE(EnumDisplayModes,8);
	HOOK_VTABLE(EnumSurfaces,9);
	HOOK_VTABLE(FlipToGDISurface,10);
	HOOK_VTABLE(GetCaps,11);
	HOOK_VTABLE(GetDisplayMode,12);
	HOOK_VTABLE(GetFourCCCodes,13);
	HOOK_VTABLE(GetGDISurface,14);
	HOOK_VTABLE(GetMonitorFrequency,15);
	HOOK_VTABLE(GetScanLine,16);
	HOOK_VTABLE(GetVerticalBlankStatus,17);
	HOOK_VTABLE(Initialize,18);
	HOOK_VTABLE(RestoreDisplayMode,19);
	HOOK_VTABLE(SetCooperativeLevel,20);
	HOOK_VTABLE(SetDisplayMode,21);
	HOOK_VTABLE(WaitForVerticalBlank,22);
	
	VirtualProtect( CDirectDraw_vtable, 23*4, dwOld, &dwOld );
}



void Device_HookVMT( IDirectDraw* ptr )
{
	if( ptr && ptr != INVALID_HANDLE_VALUE ) {
		CDirectDraw_vtable = *(VFPTR**)ptr;
		Device_HookVMT( true );
	} else {
		warning( "Device_HookVMT::IDirectDraw invalid PTR: 0x%x", ptr );
	}
}
