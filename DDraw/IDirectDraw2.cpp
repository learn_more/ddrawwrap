/***************************************************************
 * Project: DDrawWrap
 * File: IDirectDraw2.cpp
 * Copyright � learn_more
 */

#include "../winclude.h"

class CDirectDraw2 {
public:
/*** IUnknown methods ***/
HRESULT __stdcall QueryInterface( REFIID riid, LPVOID* ppvObj );
ULONG __stdcall AddRef();
ULONG __stdcall Release();
/*** IDirectDraw methods ***/
HRESULT __stdcall Compact();
HRESULT __stdcall CreateClipper( DWORD dwFlags, LPDIRECTDRAWCLIPPER* lplpDDClipper, IUnknown* pUnkOuter );
HRESULT __stdcall CreatePalette( DWORD dwFlags, LPPALETTEENTRY lpDDColorArray, LPDIRECTDRAWPALETTE* lplpDDPalette, IUnknown* pUnkOuter );
HRESULT __stdcall CreateSurface( LPDDSURFACEDESC lpDDSurfaceDesc, LPDIRECTDRAWSURFACE* lplpDDSurface, IUnknown* pUnkOuter );
HRESULT __stdcall DuplicateSurface( LPDIRECTDRAWSURFACE lpDDSurface, LPDIRECTDRAWSURFACE* lplpDupDDSurface );
HRESULT __stdcall EnumDisplayModes( DWORD dwFlags, LPDDSURFACEDESC lpDDSurfaceDesc, LPVOID lpContext, LPDDENUMMODESCALLBACK lpEnumModesCallback );
HRESULT __stdcall EnumSurfaces( DWORD dwFlags, LPDDSURFACEDESC lpDDSD, LPVOID lpContext, LPDDENUMSURFACESCALLBACK lpEnumSurfacesCallback );
HRESULT __stdcall FlipToGDISurface();
HRESULT __stdcall GetCaps( LPDDCAPS lpDDDriverCaps, LPDDCAPS lpDDHELCaps );
HRESULT __stdcall GetDisplayMode( LPDDSURFACEDESC lpDDSurfaceDesc );
HRESULT __stdcall GetFourCCCodes( LPDWORD lpNumCodes, LPDWORD lpCodes );
HRESULT __stdcall GetGDISurface( LPDIRECTDRAWSURFACE* lplpGDIDDSSurface );
HRESULT __stdcall GetMonitorFrequency( LPDWORD lpdwFrequency );
HRESULT __stdcall GetScanLine( LPDWORD lpdwScanLine );
HRESULT __stdcall GetVerticalBlankStatus( LPBOOL lpbIsInVB );
HRESULT __stdcall Initialize( GUID* lpGUID );
HRESULT __stdcall RestoreDisplayMode();
HRESULT __stdcall SetCooperativeLevel( HWND hWnd, DWORD dwFlags );
HRESULT __stdcall SetDisplayMode( DWORD dwWidth, DWORD dwHeight, DWORD dwBPP );
HRESULT __stdcall WaitForVerticalBlank( DWORD dwFlags, HANDLE hEvent );
/*** Added in the v2 interface ***/
HRESULT __stdcall GetAvailableVidMem( LPDDSCAPS lpDDSCaps2, LPDWORD lpdwTotal, LPDWORD lpdwFree );
};

HRESULT (__stdcall CDirectDraw2::*oQueryInterface)( REFIID riid, LPVOID* ppvObj );
ULONG (__stdcall CDirectDraw2::*oAddRef)();
ULONG (__stdcall CDirectDraw2::*oRelease)();
/*** IDirectDraw methods ***/
HRESULT (__stdcall CDirectDraw2::*oCompact)();
HRESULT (__stdcall CDirectDraw2::*oCreateClipper)( DWORD dwFlags, LPDIRECTDRAWCLIPPER* lplpDDClipper, IUnknown* pUnkOuter );
HRESULT (__stdcall CDirectDraw2::*oCreatePalette)( DWORD dwFlags, LPPALETTEENTRY lpDDColorArray, LPDIRECTDRAWPALETTE* lplpDDPalette, IUnknown* pUnkOuter );
HRESULT (__stdcall CDirectDraw2::*oCreateSurface)( LPDDSURFACEDESC lpDDSurfaceDesc, LPDIRECTDRAWSURFACE* lplpDDSurface, IUnknown* pUnkOuter );
HRESULT (__stdcall CDirectDraw2::*oDuplicateSurface)( LPDIRECTDRAWSURFACE lpDDSurface, LPDIRECTDRAWSURFACE* lplpDupDDSurface );
HRESULT (__stdcall CDirectDraw2::*oEnumDisplayModes)( DWORD dwFlags, LPDDSURFACEDESC lpDDSurfaceDesc, LPVOID lpContext, LPDDENUMMODESCALLBACK lpEnumModesCallback );
HRESULT (__stdcall CDirectDraw2::*oEnumSurfaces)( DWORD dwFlags, LPDDSURFACEDESC lpDDSD, LPVOID lpContext, LPDDENUMSURFACESCALLBACK lpEnumSurfacesCallback );
HRESULT (__stdcall CDirectDraw2::*oFlipToGDISurface)();
HRESULT (__stdcall CDirectDraw2::*oGetCaps)( LPDDCAPS lpDDDriverCaps, LPDDCAPS lpDDHELCaps );
HRESULT (__stdcall CDirectDraw2::*oGetDisplayMode)( LPDDSURFACEDESC lpDDSurfaceDesc );
HRESULT (__stdcall CDirectDraw2::*oGetFourCCCodes)( LPDWORD lpNumCodes, LPDWORD lpCodes );
HRESULT (__stdcall CDirectDraw2::*oGetGDISurface)( LPDIRECTDRAWSURFACE* lplpGDIDDSSurface );
HRESULT (__stdcall CDirectDraw2::*oGetMonitorFrequency)( LPDWORD lpdwFrequency );
HRESULT (__stdcall CDirectDraw2::*oGetScanLine)( LPDWORD lpdwScanLine );
HRESULT (__stdcall CDirectDraw2::*oGetVerticalBlankStatus)( LPBOOL lpbIsInVB );
HRESULT (__stdcall CDirectDraw2::*oInitialize)( GUID* lpGUID );
HRESULT (__stdcall CDirectDraw2::*oRestoreDisplayMode)();
HRESULT (__stdcall CDirectDraw2::*oSetCooperativeLevel)( HWND hWnd, DWORD dwFlags );
HRESULT (__stdcall CDirectDraw2::*oSetDisplayMode)( DWORD dwWidth, DWORD dwHeight, DWORD dwBPP );
HRESULT (__stdcall CDirectDraw2::*oWaitForVerticalBlank)( DWORD dwFlags, HANDLE hEvent );
/*** Added in the v2 interface ***/
HRESULT (__stdcall CDirectDraw2::*oGetAvailableVidMem)( LPDDSCAPS lpDDSCaps2, LPDWORD lpdwTotal, LPDWORD lpdwFree );


typedef void (__stdcall CDirectDraw2::*VFPTR)();
VFPTR* CDirectDraw2_vtable = 0;

#define HOOK_PREFIX	CDirectDraw2


HRESULT CDirectDraw2::QueryInterface( REFIID riid, LPVOID* ppvObj )
{
	char buf[100];
	FormatGUID( riid, buf, sizeof(buf) );
	ADD_LOG( "IDirectDraw2(%p)::QueryInterface( %s )", PRINT_DEV, buf );
	return (this->*oQueryInterface)( riid, ppvObj );
}

ULONG CDirectDraw2::AddRef()
{
	ULONG pRef = (this->*oAddRef)();
	ADD_LOG( "IDirectDraw2(%p)::AddRef( %i )", PRINT_DEV, pRef );
	return pRef;
}

ULONG CDirectDraw2::Release()
{
	ULONG uRet = (this->*oRelease)();
	ADD_LOG( "IDirectDraw2(%p)::Release( %i )", PRINT_DEV, uRet );
	return uRet;
}

HRESULT CDirectDraw2::Compact()
{
	ADD_LOG( "IDirectDraw2(%p)::Compact()", PRINT_DEV );
	return (this->*oCompact)();
}

HRESULT CDirectDraw2::CreateClipper( DWORD dwFlags, LPDIRECTDRAWCLIPPER* lplpDDClipper, IUnknown* pUnkOuter )
{
	HRESULT hRet = (this->*oCreateClipper)( dwFlags, lplpDDClipper, pUnkOuter );
	ADD_LOG( "IDirectDraw2(%p)::CreateClipper()", PRINT_DEV );
	return hRet;
}

HRESULT CDirectDraw2::CreatePalette( DWORD dwFlags, LPPALETTEENTRY lpDDColorArray, LPDIRECTDRAWPALETTE* lplpDDPalette, IUnknown* pUnkOuter )
{
	HRESULT hRet = (this->*oCreatePalette)( dwFlags, lpDDColorArray, lplpDDPalette, pUnkOuter );
	ADD_LOG( "IDirectDraw2(%p)::CreatePalette()", PRINT_DEV );
	return hRet;
}

HRESULT CDirectDraw2::CreateSurface( LPDDSURFACEDESC lpDDSurfaceDesc, LPDIRECTDRAWSURFACE* lplpDDSurface, IUnknown* pUnkOuter )
{
	HRESULT hRet = (this->*oCreateSurface)( lpDDSurfaceDesc, lplpDDSurface, pUnkOuter );
	ADD_LOG( "IDirectDraw2(%p)::CreateSurface()", PRINT_DEV );
	return hRet;
}

HRESULT CDirectDraw2::DuplicateSurface( LPDIRECTDRAWSURFACE lpDDSurface, LPDIRECTDRAWSURFACE* lplpDupDDSurface )
{
	HRESULT hRet = (this->*oDuplicateSurface)( lpDDSurface, lplpDupDDSurface );
	ADD_LOG( "IDirectDraw2(%p)::DuplicateSurface()", PRINT_DEV );
	return hRet;
}

HRESULT CDirectDraw2::EnumDisplayModes( DWORD dwFlags, LPDDSURFACEDESC lpDDSurfaceDesc, LPVOID lpContext, LPDDENUMMODESCALLBACK lpEnumModesCallback )
{
	ADD_LOG( "IDirectDraw2(%p)::EnumDisplayModes()", PRINT_DEV );
	return (this->*oEnumDisplayModes)( dwFlags, lpDDSurfaceDesc, lpContext, lpEnumModesCallback );
}

HRESULT CDirectDraw2::EnumSurfaces( DWORD dwFlags, LPDDSURFACEDESC lpDDSD, LPVOID lpContext, LPDDENUMSURFACESCALLBACK lpEnumSurfacesCallback )
{
	ADD_LOG( "IDirectDraw2(%p)::EnumSurfaces()", PRINT_DEV );
	return (this->*oEnumSurfaces)( dwFlags, lpDDSD, lpContext, lpEnumSurfacesCallback );
}

HRESULT CDirectDraw2::FlipToGDISurface()
{
	ADD_LOG( "IDirectDraw2(%p)::FlipToGDISurface()", PRINT_DEV );
	return (this->*oFlipToGDISurface)();
}

HRESULT CDirectDraw2::GetCaps( LPDDCAPS lpDDDriverCaps, LPDDCAPS lpDDHELCaps )
{
	HRESULT hRet = (this->*oGetCaps)( lpDDDriverCaps, lpDDHELCaps );
	LogCaps( PRINT_DEV, "IDirectDraw2", hRet, lpDDDriverCaps, lpDDHELCaps );
	return hRet;
}

HRESULT CDirectDraw2::GetDisplayMode( LPDDSURFACEDESC lpDDSurfaceDesc )
{
	ADD_LOG( "IDirectDraw2(%p)::GetDisplayMode()", PRINT_DEV );
	return (this->*oGetDisplayMode)( lpDDSurfaceDesc );
}

HRESULT CDirectDraw2::GetFourCCCodes( LPDWORD lpNumCodes, LPDWORD lpCodes )
{
	ADD_LOG( "IDirectDraw2(%p)::GetFourCCCodes()", PRINT_DEV );
	return (this->*oGetFourCCCodes)( lpNumCodes, lpCodes );
}

HRESULT CDirectDraw2::GetGDISurface( LPDIRECTDRAWSURFACE* lplpGDIDDSSurface )
{
	ADD_LOG( "IDirectDraw2(%p)::GetGDISurface()", PRINT_DEV );
	return (this->*oGetGDISurface)( lplpGDIDDSSurface );
}

HRESULT CDirectDraw2::GetMonitorFrequency( LPDWORD lpdwFrequency )
{
	ADD_LOG( "IDirectDraw2(%p)::GetMonitorFrequency()", PRINT_DEV );
	return (this->*oGetMonitorFrequency)( lpdwFrequency );
}

HRESULT CDirectDraw2::GetScanLine( LPDWORD lpdwScanLine )
{
	ADD_LOG( "IDirectDraw2(%p)::GetScanLine()", PRINT_DEV );
	return (this->*oGetScanLine)( lpdwScanLine );
}

HRESULT CDirectDraw2::GetVerticalBlankStatus( LPBOOL lpbIsInVB )
{
	ADD_LOG( "IDirectDraw2(%p)::GetVerticalBlankStatus()", PRINT_DEV );
	return (this->*oGetVerticalBlankStatus)( lpbIsInVB );
}

HRESULT CDirectDraw2::Initialize( GUID* lpGUID )
{
	ADD_LOG( "IDirectDraw2(%p)::Initialize()", PRINT_DEV );
	return (this->*oInitialize)( lpGUID );
}

HRESULT CDirectDraw2::RestoreDisplayMode()
{
	ADD_LOG( "IDirectDraw2(%p)::RestoreDisplayMode()", PRINT_DEV );
	return (this->*oRestoreDisplayMode)();
}

HRESULT CDirectDraw2::SetCooperativeLevel( HWND hWnd, DWORD dwFlags )
{
	HRESULT hRet = (this->*oSetCooperativeLevel)( hWnd, dwFlags );
	char buf[512] = {0};
	FormatCooperativeFlags( dwFlags, buf, sizeof(buf) );
	ADD_LOG( "IDirectDraw2(%p)::SetCooperativeLevel( hWnd = 0x%x, dwFlags = %s ) = 0x%x",
		PRINT_DEV, hWnd, buf, hRet );
	return hRet;
}

HRESULT CDirectDraw2::SetDisplayMode( DWORD dwWidth, DWORD dwHeight, DWORD dwBPP )
{
	ADD_LOG( "IDirectDraw2(%p)::SetDisplayMode( dwWidth = %i, dwHeight = %i, dwBPP = %i )", PRINT_DEV, dwWidth, dwHeight, dwBPP );
	return (this->*oSetDisplayMode)( dwWidth, dwHeight, dwBPP );
}

HRESULT CDirectDraw2::WaitForVerticalBlank( DWORD dwFlags, HANDLE hEvent )
{
	ADD_LOG( "IDirectDraw2(%p)::WaitForVerticalBlank()", PRINT_DEV );
	return (this->*oWaitForVerticalBlank)( dwFlags, hEvent );
}

HRESULT CDirectDraw2::GetAvailableVidMem( LPDDSCAPS lpDDSCaps2, LPDWORD lpdwTotal, LPDWORD lpdwFree )
{
	ADD_LOG( "IDirectDraw2(%p)::GetAvailableVidMem()", PRINT_DEV );
	return (this->*oGetAvailableVidMem)( lpDDSCaps2, lpdwTotal, lpdwFree );
}


void Device_HookVMT2( bool first )
{
	DWORD dwOld = 0;
	VirtualProtect( CDirectDraw2_vtable, 24*4, PAGE_EXECUTE_READWRITE, &dwOld );

	HOOK_VTABLE(QueryInterface,0);
	HOOK_VTABLE(AddRef,1);
	HOOK_VTABLE(Release,2);

	HOOK_VTABLE(Compact,3);
	HOOK_VTABLE(CreateClipper,4);
	HOOK_VTABLE(CreatePalette,5);
	HOOK_VTABLE(CreateSurface,6);
	HOOK_VTABLE(DuplicateSurface,7);
	HOOK_VTABLE(EnumDisplayModes,8);
	HOOK_VTABLE(EnumSurfaces,9);
	HOOK_VTABLE(FlipToGDISurface,10);
	HOOK_VTABLE(GetCaps,11);
	HOOK_VTABLE(GetDisplayMode,12);
	HOOK_VTABLE(GetFourCCCodes,13);
	HOOK_VTABLE(GetGDISurface,14);
	HOOK_VTABLE(GetMonitorFrequency,15);
	HOOK_VTABLE(GetScanLine,16);
	HOOK_VTABLE(GetVerticalBlankStatus,17);
	HOOK_VTABLE(Initialize,18);
	HOOK_VTABLE(RestoreDisplayMode,19);
	HOOK_VTABLE(SetCooperativeLevel,20);
	HOOK_VTABLE(SetDisplayMode,21);
	HOOK_VTABLE(WaitForVerticalBlank,22);

	HOOK_VTABLE(GetAvailableVidMem,23);

	VirtualProtect( CDirectDraw2_vtable, 24*4, dwOld, &dwOld );
}



void Device_HookVMT( IDirectDraw2* ptr )
{
	if( ptr && ptr != INVALID_HANDLE_VALUE ) {
		CDirectDraw2_vtable = *(VFPTR**)ptr;
		Device_HookVMT2( true );
	} else {
		warning( "Device_HookVMT::IDirectDraw2 invalid PTR: 0x%x", ptr );
	}
}
