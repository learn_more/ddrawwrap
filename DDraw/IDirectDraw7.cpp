/***************************************************************
 * Project: DDrawWrap
 * File: IDirectDraw7.cpp
 * Copyright � learn_more
 */

#include "../winclude.h"

class CDirectDraw7 {
public:
/*** IUnknown methods ***/
HRESULT __stdcall QueryInterface( REFIID riid, LPVOID* ppvObj );
ULONG __stdcall AddRef();
ULONG __stdcall Release();
/*** IDirectDraw methods ***/
HRESULT __stdcall Compact();
HRESULT __stdcall CreateClipper( DWORD dwFlags, LPDIRECTDRAWCLIPPER* lplpDDClipper, IUnknown* pUnkOuter );
HRESULT __stdcall CreatePalette( DWORD dwFlags, LPPALETTEENTRY lpDDColorArray, LPDIRECTDRAWPALETTE* lplpDDPalette, IUnknown* pUnkOuter );
HRESULT __stdcall CreateSurface( LPDDSURFACEDESC2 lpDDSurfaceDesc2, LPDIRECTDRAWSURFACE7* lplpDDSurface, IUnknown* pUnkOuter );
HRESULT __stdcall DuplicateSurface( LPDIRECTDRAWSURFACE7 lpDDSurface, LPDIRECTDRAWSURFACE7* lplpDupDDSurface );
HRESULT __stdcall EnumDisplayModes( DWORD dwFlags, LPDDSURFACEDESC2 lpDDSurfaceDesc2, LPVOID lpContext, LPDDENUMMODESCALLBACK2 lpEnumModesCallback );
HRESULT __stdcall EnumSurfaces( DWORD dwFlags, LPDDSURFACEDESC2 lpDDSD2, LPVOID lpContext, LPDDENUMSURFACESCALLBACK7 lpEnumSurfacesCallback );
HRESULT __stdcall FlipToGDISurface();
HRESULT __stdcall GetCaps( LPDDCAPS lpDDDriverCaps, LPDDCAPS lpDDHELCaps );
HRESULT __stdcall GetDisplayMode( LPDDSURFACEDESC2 lpDDSurfaceDesc2 );
HRESULT __stdcall GetFourCCCodes( LPDWORD lpNumCodes, LPDWORD lpCodes );
HRESULT __stdcall GetGDISurface( LPDIRECTDRAWSURFACE7* lplpGDIDDSSurface );
HRESULT __stdcall GetMonitorFrequency( LPDWORD lpdwFrequency );
HRESULT __stdcall GetScanLine( LPDWORD lpdwScanLine );
HRESULT __stdcall GetVerticalBlankStatus( LPBOOL lpbIsInVB );
HRESULT __stdcall Initialize( GUID* lpGUID );
HRESULT __stdcall RestoreDisplayMode();
HRESULT __stdcall SetCooperativeLevel( HWND hWnd, DWORD dwFlags );
HRESULT __stdcall SetDisplayMode( DWORD dwWidth, DWORD dwHeight, DWORD dwBPP, DWORD dwRefreshRate, DWORD dwFlags );
HRESULT __stdcall WaitForVerticalBlank( DWORD dwFlags, HANDLE hEvent );
/*** Added in the v2 interface ***/
HRESULT __stdcall GetAvailableVidMem( LPDDSCAPS2 lpDDSCaps2, LPDWORD lpdwTotal, LPDWORD lpdwFree );
/*** Added in the V4 Interface ***/
HRESULT __stdcall GetSurfaceFromDC( HDC hdc, LPDIRECTDRAWSURFACE7* lpDDS );
HRESULT __stdcall RestoreAllSurfaces();
HRESULT __stdcall TestCooperativeLevel();
HRESULT __stdcall GetDeviceIdentifier( LPDDDEVICEIDENTIFIER lpdddi, DWORD dwFlags );
/*** Added in the V7 Interface ***/
HRESULT __stdcall StartModeTest( LPSIZE lpModesToTest, DWORD dwNumEntries, DWORD dwFlags );
HRESULT __stdcall EvaluateMode( DWORD dwFlags, DWORD* pSecondsUntilTimeout );
};


/*** IUnknown methods ***/
HRESULT (__stdcall CDirectDraw7::*oQueryInterface)( REFIID riid, LPVOID* ppvObj );
ULONG (__stdcall CDirectDraw7::*oAddRef)();
ULONG (__stdcall CDirectDraw7::*oRelease)();
/*** IDirectDraw methods ***/
HRESULT (__stdcall CDirectDraw7::*oCompact)();
HRESULT (__stdcall CDirectDraw7::*oCreateClipper)( DWORD dwFlags, LPDIRECTDRAWCLIPPER* lplpDDClipper, IUnknown* pUnkOuter );
HRESULT (__stdcall CDirectDraw7::*oCreatePalette)( DWORD dwFlags, LPPALETTEENTRY lpDDColorArray, LPDIRECTDRAWPALETTE* lplpDDPalette, IUnknown* pUnkOuter );
HRESULT (__stdcall CDirectDraw7::*oCreateSurface)( LPDDSURFACEDESC2 lpDDSurfaceDesc2, LPDIRECTDRAWSURFACE7* lplpDDSurface, IUnknown* pUnkOuter );
HRESULT (__stdcall CDirectDraw7::*oDuplicateSurface)( LPDIRECTDRAWSURFACE7 lpDDSurface, LPDIRECTDRAWSURFACE7* lplpDupDDSurface );
HRESULT (__stdcall CDirectDraw7::*oEnumDisplayModes)( DWORD dwFlags, LPDDSURFACEDESC2 lpDDSurfaceDesc2, LPVOID lpContext, LPDDENUMMODESCALLBACK2 lpEnumModesCallback );
HRESULT (__stdcall CDirectDraw7::*oEnumSurfaces)( DWORD dwFlags, LPDDSURFACEDESC2 lpDDSD2, LPVOID lpContext, LPDDENUMSURFACESCALLBACK7 lpEnumSurfacesCallback );
HRESULT (__stdcall CDirectDraw7::*oFlipToGDISurface)();
HRESULT (__stdcall CDirectDraw7::*oGetCaps)( LPDDCAPS lpDDDriverCaps, LPDDCAPS lpDDHELCaps );
HRESULT (__stdcall CDirectDraw7::*oGetDisplayMode)( LPDDSURFACEDESC2 lpDDSurfaceDesc2 );
HRESULT (__stdcall CDirectDraw7::*oGetFourCCCodes)( LPDWORD lpNumCodes, LPDWORD lpCodes );
HRESULT (__stdcall CDirectDraw7::*oGetGDISurface)( LPDIRECTDRAWSURFACE7* lplpGDIDDSSurface );
HRESULT (__stdcall CDirectDraw7::*oGetMonitorFrequency)( LPDWORD lpdwFrequency );
HRESULT (__stdcall CDirectDraw7::*oGetScanLine)( LPDWORD lpdwScanLine );
HRESULT (__stdcall CDirectDraw7::*oGetVerticalBlankStatus)( LPBOOL lpbIsInVB );
HRESULT (__stdcall CDirectDraw7::*oInitialize)( GUID* lpGUID );
HRESULT (__stdcall CDirectDraw7::*oRestoreDisplayMode)();
HRESULT (__stdcall CDirectDraw7::*oSetCooperativeLevel)( HWND hWnd, DWORD dwFlags );
HRESULT (__stdcall CDirectDraw7::*oSetDisplayMode)( DWORD dwWidth, DWORD dwHeight, DWORD dwBPP, DWORD dwRefreshRate, DWORD dwFlags );
HRESULT (__stdcall CDirectDraw7::*oWaitForVerticalBlank)( DWORD dwFlags, HANDLE hEvent );
/*** Added in the v2 interface ***/
HRESULT (__stdcall CDirectDraw7::*oGetAvailableVidMem)( LPDDSCAPS2 lpDDSCaps2, LPDWORD lpdwTotal, LPDWORD lpdwFree );
/*** Added in the V4 Interface ***/
HRESULT (__stdcall CDirectDraw7::*oGetSurfaceFromDC)( HDC hdc, LPDIRECTDRAWSURFACE7* lpDDS );
HRESULT (__stdcall CDirectDraw7::*oRestoreAllSurfaces)();
HRESULT (__stdcall CDirectDraw7::*oTestCooperativeLevel)();
HRESULT (__stdcall CDirectDraw7::*oGetDeviceIdentifier)( LPDDDEVICEIDENTIFIER lpdddi, DWORD dwFlags );
/*** Added in the V7 Interface ***/
HRESULT (__stdcall CDirectDraw7::*oStartModeTest)( LPSIZE lpModesToTest, DWORD dwNumEntries, DWORD dwFlags );
HRESULT (__stdcall CDirectDraw7::*oEvaluateMode)( DWORD dwFlags, DWORD* pSecondsUntilTimeout );


typedef void (__stdcall CDirectDraw7::*VFPTR)();
VFPTR* CDirectDraw7_vtable = 0;

#define HOOK_PREFIX	CDirectDraw7


HRESULT CDirectDraw7::QueryInterface( REFIID riid, LPVOID* ppvObj )
{
	char buf[100];
	FormatGUID( riid, buf, sizeof(buf) );
	ADD_LOG( "IDirectDraw7(%p)::QueryInterface( %s )", PRINT_DEV, buf );
	return (this->*oQueryInterface)( riid, ppvObj );
}

ULONG CDirectDraw7::AddRef()
{
	ULONG pRef = (this->*oAddRef)();
	ADD_LOG( "IDirectDraw7(%p)::AddRef( %i )", PRINT_DEV, pRef );
	return pRef;
}

ULONG CDirectDraw7::Release()
{
	ULONG uRet = (this->*oRelease)();
	ADD_LOG( "IDirectDraw7(%p)::Release( %i )", PRINT_DEV, uRet );
	return uRet;
}

HRESULT CDirectDraw7::Compact()
{
	ADD_LOG( "IDirectDraw7(%p)::Compact()", PRINT_DEV );
	return (this->*oCompact)();
}

HRESULT CDirectDraw7::CreateClipper( DWORD dwFlags, LPDIRECTDRAWCLIPPER* lplpDDClipper, IUnknown* pUnkOuter )
{
	HRESULT hRet = (this->*oCreateClipper)( dwFlags, lplpDDClipper, pUnkOuter );
	ADD_LOG( "IDirectDraw7(%p)::CreateClipper()", PRINT_DEV );
	return hRet;
}

HRESULT CDirectDraw7::CreatePalette( DWORD dwFlags, LPPALETTEENTRY lpDDColorArray, LPDIRECTDRAWPALETTE* lplpDDPalette, IUnknown* pUnkOuter )
{
	HRESULT hRet = (this->*oCreatePalette)( dwFlags, lpDDColorArray, lplpDDPalette, pUnkOuter );
	ADD_LOG( "IDirectDraw7(%p)::CreatePalette()", PRINT_DEV );
	return hRet;
}

HRESULT CDirectDraw7::CreateSurface( LPDDSURFACEDESC2 lpDDSurfaceDesc, LPDIRECTDRAWSURFACE7* lplpDDSurface, IUnknown* pUnkOuter )
{
	HRESULT hRet = (this->*oCreateSurface)( lpDDSurfaceDesc, lplpDDSurface, pUnkOuter );
	ADD_LOG( "IDirectDraw7(%p)::CreateSurface()", PRINT_DEV );
	return hRet;
}

HRESULT CDirectDraw7::DuplicateSurface( LPDIRECTDRAWSURFACE7 lpDDSurface, LPDIRECTDRAWSURFACE7* lplpDupDDSurface )
{
	HRESULT hRet = (this->*oDuplicateSurface)( lpDDSurface, lplpDupDDSurface );
	ADD_LOG( "IDirectDraw7(%p)::DuplicateSurface()", PRINT_DEV );
	return hRet;
}

HRESULT CDirectDraw7::EnumDisplayModes( DWORD dwFlags, LPDDSURFACEDESC2 lpDDSurfaceDesc, LPVOID lpContext, LPDDENUMMODESCALLBACK2 lpEnumModesCallback )
{
	ADD_LOG( "IDirectDraw7(%p)::EnumDisplayModes()", PRINT_DEV );
	return (this->*oEnumDisplayModes)( dwFlags, lpDDSurfaceDesc, lpContext, lpEnumModesCallback );
}

HRESULT CDirectDraw7::EnumSurfaces( DWORD dwFlags, LPDDSURFACEDESC2 lpDDSD, LPVOID lpContext, LPDDENUMSURFACESCALLBACK7 lpEnumSurfacesCallback )
{
	ADD_LOG( "IDirectDraw7(%p)::EnumSurfaces()", PRINT_DEV );
	return (this->*oEnumSurfaces)( dwFlags, lpDDSD, lpContext, lpEnumSurfacesCallback );
}

HRESULT CDirectDraw7::FlipToGDISurface()
{
	ADD_LOG( "IDirectDraw7(%p)::FlipToGDISurface()", PRINT_DEV );
	return (this->*oFlipToGDISurface)();
}

HRESULT CDirectDraw7::GetCaps( LPDDCAPS lpDDDriverCaps, LPDDCAPS lpDDHELCaps )
{
	HRESULT hRet = (this->*oGetCaps)( lpDDDriverCaps, lpDDHELCaps );
	LogCaps( PRINT_DEV, "IDirectDraw7", hRet, lpDDDriverCaps, lpDDHELCaps );
	return hRet;
}

HRESULT CDirectDraw7::GetDisplayMode( LPDDSURFACEDESC2 lpDDSurfaceDesc )
{
	ADD_LOG( "IDirectDraw7(%p)::GetDisplayMode()", PRINT_DEV );
	return (this->*oGetDisplayMode)( lpDDSurfaceDesc );
}

HRESULT CDirectDraw7::GetFourCCCodes( LPDWORD lpNumCodes, LPDWORD lpCodes )
{
	ADD_LOG( "IDirectDraw7(%p)::GetFourCCCodes()", PRINT_DEV );
	return (this->*oGetFourCCCodes)( lpNumCodes, lpCodes );
}

HRESULT CDirectDraw7::GetGDISurface( LPDIRECTDRAWSURFACE7* lplpGDIDDSSurface )
{
	ADD_LOG( "IDirectDraw7(%p)::GetGDISurface()", PRINT_DEV );
	return (this->*oGetGDISurface)( lplpGDIDDSSurface );
}

HRESULT CDirectDraw7::GetMonitorFrequency( LPDWORD lpdwFrequency )
{
	ADD_LOG( "IDirectDraw7(%p)::GetMonitorFrequency()", PRINT_DEV );
	return (this->*oGetMonitorFrequency)( lpdwFrequency );
}

HRESULT CDirectDraw7::GetScanLine( LPDWORD lpdwScanLine )
{
	ADD_LOG( "IDirectDraw7(%p)::GetScanLine()", PRINT_DEV );
	return (this->*oGetScanLine)( lpdwScanLine );
}

HRESULT CDirectDraw7::GetVerticalBlankStatus( LPBOOL lpbIsInVB )
{
	ADD_LOG( "IDirectDraw7(%p)::GetVerticalBlankStatus()", PRINT_DEV );
	return (this->*oGetVerticalBlankStatus)( lpbIsInVB );
}

HRESULT CDirectDraw7::Initialize( GUID* lpGUID )
{
	ADD_LOG( "IDirectDraw7(%p)::Initialize()", PRINT_DEV );
	return (this->*oInitialize)( lpGUID );
}

HRESULT CDirectDraw7::RestoreDisplayMode()
{
	ADD_LOG( "IDirectDraw7(%p)::RestoreDisplayMode()", PRINT_DEV );
	return (this->*oRestoreDisplayMode)();
}

HRESULT CDirectDraw7::SetCooperativeLevel( HWND hWnd, DWORD dwFlags )
{
	HRESULT hRet = (this->*oSetCooperativeLevel)( hWnd, dwFlags );
	char buf[512] = {0};
	FormatCooperativeFlags( dwFlags, buf, sizeof(buf) );
	ADD_LOG( "IDirectDraw7(%p)::SetCooperativeLevel( hWnd = 0x%x, dwFlags = %s ) = 0x%x",
		PRINT_DEV, hWnd, buf, hRet );
	return hRet;
}

HRESULT CDirectDraw7::SetDisplayMode( DWORD dwWidth, DWORD dwHeight, DWORD dwBPP, DWORD dwRefreshRate, DWORD dwFlags )
{
	ADD_LOG( "IDirectDraw7(%p)::SetDisplayMode( dwWidth = %i, dwHeight = %i, dwBPP = %i, dwRefreshRate = %i, dwFlags = 0x%x )", 
		PRINT_DEV, dwWidth, dwHeight, dwBPP, dwRefreshRate, dwFlags );
	return (this->*oSetDisplayMode)( dwWidth, dwHeight, dwBPP, dwRefreshRate, dwFlags );
}

HRESULT CDirectDraw7::WaitForVerticalBlank( DWORD dwFlags, HANDLE hEvent )
{
	ADD_LOG( "IDirectDraw7(%p)::WaitForVerticalBlank()", PRINT_DEV );
	return (this->*oWaitForVerticalBlank)( dwFlags, hEvent );
}

HRESULT CDirectDraw7::GetAvailableVidMem( LPDDSCAPS2 lpDDSCaps2, LPDWORD lpdwTotal, LPDWORD lpdwFree )
{
	ADD_LOG( "IDirectDraw7(%p)::GetAvailableVidMem()", PRINT_DEV );
	return (this->*oGetAvailableVidMem)( lpDDSCaps2, lpdwTotal, lpdwFree );
}

HRESULT CDirectDraw7::GetSurfaceFromDC( HDC hdc, LPDIRECTDRAWSURFACE7* lpDDS )
{
	ADD_LOG( "IDirectDraw7(%p)::GetSurfaceFromDC()", PRINT_DEV );
	return (this->*oGetSurfaceFromDC)( hdc, lpDDS );
}

HRESULT CDirectDraw7::RestoreAllSurfaces()
{
	ADD_LOG( "IDirectDraw7(%p)::RestoreAllSurfaces()", PRINT_DEV );
	return (this->*oRestoreAllSurfaces)();
}

HRESULT CDirectDraw7::TestCooperativeLevel()
{
	ADD_LOG( "IDirectDraw7(%p)::TestCooperativeLevel()", PRINT_DEV );
	return (this->*oTestCooperativeLevel)();
}

HRESULT CDirectDraw7::GetDeviceIdentifier( LPDDDEVICEIDENTIFIER lpdddi, DWORD dwFlags )
{
	ADD_LOG( "IDirectDraw7(%p)::GetDeviceIdentifier()", PRINT_DEV );
	return (this->*oGetDeviceIdentifier)( lpdddi, dwFlags );
}

HRESULT CDirectDraw7::StartModeTest( LPSIZE lpModesToTest, DWORD dwNumEntries, DWORD dwFlags )
{
	ADD_LOG( "IDirectDraw7(%p)::StartModeTest()", PRINT_DEV );
	return (this->*oStartModeTest)( lpModesToTest, dwNumEntries, dwFlags );
}

HRESULT CDirectDraw7::EvaluateMode( DWORD dwFlags, DWORD* pSecondsUntilTimeout )
{
	ADD_LOG( "IDirectDraw7(%p)::EvaluateMode()", PRINT_DEV );
	return (this->*oEvaluateMode)( dwFlags, pSecondsUntilTimeout );
}



void Device_HookVMT7( bool first )
{
	DWORD dwOld = 0;
	VirtualProtect( CDirectDraw7_vtable, 30*4, PAGE_EXECUTE_READWRITE, &dwOld );

	HOOK_VTABLE(QueryInterface,0);
	HOOK_VTABLE(AddRef,1);
	HOOK_VTABLE(Release,2);

	HOOK_VTABLE(Compact,3);
	HOOK_VTABLE(CreateClipper,4);
	HOOK_VTABLE(CreatePalette,5);
	HOOK_VTABLE(CreateSurface,6);
	HOOK_VTABLE(DuplicateSurface,7);
	HOOK_VTABLE(EnumDisplayModes,8);
	HOOK_VTABLE(EnumSurfaces,9);
	HOOK_VTABLE(FlipToGDISurface,10);
	HOOK_VTABLE(GetCaps,11);
	HOOK_VTABLE(GetDisplayMode,12);
	HOOK_VTABLE(GetFourCCCodes,13);
	HOOK_VTABLE(GetGDISurface,14);
	HOOK_VTABLE(GetMonitorFrequency,15);
	HOOK_VTABLE(GetScanLine,16);
	HOOK_VTABLE(GetVerticalBlankStatus,17);
	HOOK_VTABLE(Initialize,18);
	HOOK_VTABLE(RestoreDisplayMode,19);
	HOOK_VTABLE(SetCooperativeLevel,20);
	HOOK_VTABLE(SetDisplayMode,21);
	HOOK_VTABLE(WaitForVerticalBlank,22);

	HOOK_VTABLE(GetAvailableVidMem,23);

	HOOK_VTABLE(GetSurfaceFromDC,24);
	HOOK_VTABLE(RestoreAllSurfaces,25);
	HOOK_VTABLE(TestCooperativeLevel,26);
	HOOK_VTABLE(GetDeviceIdentifier,27);

	HOOK_VTABLE(StartModeTest,28);
	HOOK_VTABLE(EvaluateMode,29);

	VirtualProtect( CDirectDraw7_vtable, 30*4, dwOld, &dwOld );
}



void Device_HookVMT( IDirectDraw7* ptr )
{
	if( ptr && ptr != INVALID_HANDLE_VALUE ) {
		CDirectDraw7_vtable = *(VFPTR**)ptr;
		Device_HookVMT7( true );
	} else {
		warning( "Device_HookVMT::IDirectDraw7 invalid PTR: 0x%x", ptr );
	}
}
