/***************************************************************
 * Project: DDrawWrap
 * File: IDirectDraw4.cpp
 * Copyright � learn_more
 */

#include "../winclude.h"

class CDirectDraw4 {
public:
/*** IUnknown methods ***/
HRESULT __stdcall QueryInterface( REFIID riid, LPVOID* ppvObj );
ULONG __stdcall AddRef();
ULONG __stdcall Release();
/*** IDirectDraw methods ***/
HRESULT __stdcall Compact();
HRESULT __stdcall CreateClipper( DWORD dwFlags, LPDIRECTDRAWCLIPPER* lplpDDClipper, IUnknown* pUnkOuter );
HRESULT __stdcall CreatePalette( DWORD dwFlags, LPPALETTEENTRY lpDDColorArray, LPDIRECTDRAWPALETTE* lplpDDPalette, IUnknown* pUnkOuter );
HRESULT __stdcall CreateSurface( LPDDSURFACEDESC2 lpDDSurfaceDesc2, LPDIRECTDRAWSURFACE4* lplpDDSurface, IUnknown* pUnkOuter );
HRESULT __stdcall DuplicateSurface( LPDIRECTDRAWSURFACE4 lpDDSurface, LPDIRECTDRAWSURFACE4* lplpDupDDSurface );
HRESULT __stdcall EnumDisplayModes( DWORD dwFlags, LPDDSURFACEDESC2 lpDDSurfaceDesc2, LPVOID lpContext, LPDDENUMMODESCALLBACK2 lpEnumModesCallback );
HRESULT __stdcall EnumSurfaces( DWORD dwFlags, LPDDSURFACEDESC2 lpDDSD2, LPVOID lpContext, LPDDENUMSURFACESCALLBACK2 lpEnumSurfacesCallback );
HRESULT __stdcall FlipToGDISurface();
HRESULT __stdcall GetCaps( LPDDCAPS lpDDDriverCaps, LPDDCAPS lpDDHELCaps );
HRESULT __stdcall GetDisplayMode( LPDDSURFACEDESC2 lpDDSurfaceDesc2 );
HRESULT __stdcall GetFourCCCodes( LPDWORD lpNumCodes, LPDWORD lpCodes );
HRESULT __stdcall GetGDISurface( LPDIRECTDRAWSURFACE4* lplpGDIDDSSurface );
HRESULT __stdcall GetMonitorFrequency( LPDWORD lpdwFrequency );
HRESULT __stdcall GetScanLine( LPDWORD lpdwScanLine );
HRESULT __stdcall GetVerticalBlankStatus( LPBOOL lpbIsInVB );
HRESULT __stdcall Initialize( GUID* lpGUID );
HRESULT __stdcall RestoreDisplayMode();
HRESULT __stdcall SetCooperativeLevel( HWND hWnd, DWORD dwFlags );
HRESULT __stdcall SetDisplayMode( DWORD dwWidth, DWORD dwHeight, DWORD dwBPP );
HRESULT __stdcall WaitForVerticalBlank( DWORD dwFlags, HANDLE hEvent );
/*** Added in the v2 interface ***/
HRESULT __stdcall GetAvailableVidMem( LPDDSCAPS2 lpDDSCaps2, LPDWORD lpdwTotal, LPDWORD lpdwFree );
/*** Added in the V4 Interface ***/
HRESULT __stdcall GetSurfaceFromDC( HDC hdc, LPDIRECTDRAWSURFACE4* lpDDS );
HRESULT __stdcall RestoreAllSurfaces();
HRESULT __stdcall TestCooperativeLevel();
HRESULT __stdcall GetDeviceIdentifier( LPDDDEVICEIDENTIFIER lpdddi, DWORD dwFlags );
};


/*** IUnknown methods ***/
HRESULT (__stdcall CDirectDraw4::*oQueryInterface)( REFIID riid, LPVOID* ppvObj );
ULONG (__stdcall CDirectDraw4::*oAddRef)();
ULONG (__stdcall CDirectDraw4::*oRelease)();
/*** IDirectDraw methods ***/
HRESULT (__stdcall CDirectDraw4::*oCompact)();
HRESULT (__stdcall CDirectDraw4::*oCreateClipper)( DWORD dwFlags, LPDIRECTDRAWCLIPPER* lplpDDClipper, IUnknown* pUnkOuter );
HRESULT (__stdcall CDirectDraw4::*oCreatePalette)( DWORD dwFlags, LPPALETTEENTRY lpDDColorArray, LPDIRECTDRAWPALETTE* lplpDDPalette, IUnknown* pUnkOuter );
HRESULT (__stdcall CDirectDraw4::*oCreateSurface)( LPDDSURFACEDESC2 lpDDSurfaceDesc2, LPDIRECTDRAWSURFACE4* lplpDDSurface, IUnknown* pUnkOuter );
HRESULT (__stdcall CDirectDraw4::*oDuplicateSurface)( LPDIRECTDRAWSURFACE4 lpDDSurface, LPDIRECTDRAWSURFACE4* lplpDupDDSurface );
HRESULT (__stdcall CDirectDraw4::*oEnumDisplayModes)( DWORD dwFlags, LPDDSURFACEDESC2 lpDDSurfaceDesc2, LPVOID lpContext, LPDDENUMMODESCALLBACK2 lpEnumModesCallback );
HRESULT (__stdcall CDirectDraw4::*oEnumSurfaces)( DWORD dwFlags, LPDDSURFACEDESC2 lpDDSD2, LPVOID lpContext, LPDDENUMSURFACESCALLBACK2 lpEnumSurfacesCallback );
HRESULT (__stdcall CDirectDraw4::*oFlipToGDISurface)();
HRESULT (__stdcall CDirectDraw4::*oGetCaps)( LPDDCAPS lpDDDriverCaps, LPDDCAPS lpDDHELCaps );
HRESULT (__stdcall CDirectDraw4::*oGetDisplayMode)( LPDDSURFACEDESC2 lpDDSurfaceDesc2 );
HRESULT (__stdcall CDirectDraw4::*oGetFourCCCodes)( LPDWORD lpNumCodes, LPDWORD lpCodes );
HRESULT (__stdcall CDirectDraw4::*oGetGDISurface)( LPDIRECTDRAWSURFACE4* lplpGDIDDSSurface );
HRESULT (__stdcall CDirectDraw4::*oGetMonitorFrequency)( LPDWORD lpdwFrequency );
HRESULT (__stdcall CDirectDraw4::*oGetScanLine)( LPDWORD lpdwScanLine );
HRESULT (__stdcall CDirectDraw4::*oGetVerticalBlankStatus)( LPBOOL lpbIsInVB );
HRESULT (__stdcall CDirectDraw4::*oInitialize)( GUID* lpGUID );
HRESULT (__stdcall CDirectDraw4::*oRestoreDisplayMode)();
HRESULT (__stdcall CDirectDraw4::*oSetCooperativeLevel)( HWND hWnd, DWORD dwFlags );
HRESULT (__stdcall CDirectDraw4::*oSetDisplayMode)( DWORD dwWidth, DWORD dwHeight, DWORD dwBPP );
HRESULT (__stdcall CDirectDraw4::*oWaitForVerticalBlank)( DWORD dwFlags, HANDLE hEvent );
/*** Added in the v2 interface ***/
HRESULT (__stdcall CDirectDraw4::*oGetAvailableVidMem)( LPDDSCAPS2 lpDDSCaps2, LPDWORD lpdwTotal, LPDWORD lpdwFree );
/*** Added in the V4 Interface ***/
HRESULT (__stdcall CDirectDraw4::*oGetSurfaceFromDC)( HDC hdc, LPDIRECTDRAWSURFACE4* lpDDS );
HRESULT (__stdcall CDirectDraw4::*oRestoreAllSurfaces)();
HRESULT (__stdcall CDirectDraw4::*oTestCooperativeLevel)();
HRESULT (__stdcall CDirectDraw4::*oGetDeviceIdentifier)( LPDDDEVICEIDENTIFIER lpdddi, DWORD dwFlags );


typedef void (__stdcall CDirectDraw4::*VFPTR)();
VFPTR* CDirectDraw4_vtable = 0;

#define HOOK_PREFIX	CDirectDraw4


HRESULT CDirectDraw4::QueryInterface( REFIID riid, LPVOID* ppvObj )
{
	char buf[100];
	FormatGUID( riid, buf, sizeof(buf) );
	ADD_LOG( "IDirectDraw4(%p)::QueryInterface( %s )", PRINT_DEV, buf );
	return (this->*oQueryInterface)( riid, ppvObj );
}

ULONG CDirectDraw4::AddRef()
{
	ULONG pRef = (this->*oAddRef)();
	ADD_LOG( "IDirectDraw4(%p)::AddRef( %i )", PRINT_DEV, pRef );
	return pRef;
}

ULONG CDirectDraw4::Release()
{
	ULONG uRet = (this->*oRelease)();
	ADD_LOG( "IDirectDraw4(%p)::Release( %i )", PRINT_DEV, uRet );
	return uRet;
}

HRESULT CDirectDraw4::Compact()
{
	ADD_LOG( "IDirectDraw4(%p)::Compact()", PRINT_DEV );
	return (this->*oCompact)();
}

HRESULT CDirectDraw4::CreateClipper( DWORD dwFlags, LPDIRECTDRAWCLIPPER* lplpDDClipper, IUnknown* pUnkOuter )
{
	HRESULT hRet = (this->*oCreateClipper)( dwFlags, lplpDDClipper, pUnkOuter );
	ADD_LOG( "IDirectDraw4(%p)::CreateClipper()", PRINT_DEV );
	return hRet;
}

HRESULT CDirectDraw4::CreatePalette( DWORD dwFlags, LPPALETTEENTRY lpDDColorArray, LPDIRECTDRAWPALETTE* lplpDDPalette, IUnknown* pUnkOuter )
{
	HRESULT hRet = (this->*oCreatePalette)( dwFlags, lpDDColorArray, lplpDDPalette, pUnkOuter );
	ADD_LOG( "IDirectDraw4(%p)::CreatePalette()", PRINT_DEV );
	return hRet;
}

HRESULT CDirectDraw4::CreateSurface( LPDDSURFACEDESC2 lpDDSurfaceDesc, LPDIRECTDRAWSURFACE4* lplpDDSurface, IUnknown* pUnkOuter )
{
	HRESULT hRet = (this->*oCreateSurface)( lpDDSurfaceDesc, lplpDDSurface, pUnkOuter );
	ADD_LOG( "IDirectDraw4(%p)::CreateSurface()", PRINT_DEV );
	return hRet;
}

HRESULT CDirectDraw4::DuplicateSurface( LPDIRECTDRAWSURFACE4 lpDDSurface, LPDIRECTDRAWSURFACE4* lplpDupDDSurface )
{
	HRESULT hRet = (this->*oDuplicateSurface)( lpDDSurface, lplpDupDDSurface );
	ADD_LOG( "IDirectDraw4(%p)::DuplicateSurface()", PRINT_DEV );
	return hRet;
}

HRESULT CDirectDraw4::EnumDisplayModes( DWORD dwFlags, LPDDSURFACEDESC2 lpDDSurfaceDesc, LPVOID lpContext, LPDDENUMMODESCALLBACK2 lpEnumModesCallback )
{
	ADD_LOG( "IDirectDraw4(%p)::EnumDisplayModes()", PRINT_DEV );
	return (this->*oEnumDisplayModes)( dwFlags, lpDDSurfaceDesc, lpContext, lpEnumModesCallback );
}

HRESULT CDirectDraw4::EnumSurfaces( DWORD dwFlags, LPDDSURFACEDESC2 lpDDSD, LPVOID lpContext, LPDDENUMSURFACESCALLBACK2 lpEnumSurfacesCallback )
{
	ADD_LOG( "IDirectDraw4(%p)::EnumSurfaces()", PRINT_DEV );
	return (this->*oEnumSurfaces)( dwFlags, lpDDSD, lpContext, lpEnumSurfacesCallback );
}

HRESULT CDirectDraw4::FlipToGDISurface()
{
	ADD_LOG( "IDirectDraw4(%p)::FlipToGDISurface()", PRINT_DEV );
	return (this->*oFlipToGDISurface)();
}

HRESULT CDirectDraw4::GetCaps( LPDDCAPS lpDDDriverCaps, LPDDCAPS lpDDHELCaps )
{
	HRESULT hRet = (this->*oGetCaps)( lpDDDriverCaps, lpDDHELCaps );
	LogCaps( PRINT_DEV, "IDirectDraw4", hRet, lpDDDriverCaps, lpDDHELCaps );
	return hRet;
}

HRESULT CDirectDraw4::GetDisplayMode( LPDDSURFACEDESC2 lpDDSurfaceDesc )
{
	ADD_LOG( "IDirectDraw4(%p)::GetDisplayMode()", PRINT_DEV );
	return (this->*oGetDisplayMode)( lpDDSurfaceDesc );
}

HRESULT CDirectDraw4::GetFourCCCodes( LPDWORD lpNumCodes, LPDWORD lpCodes )
{
	ADD_LOG( "IDirectDraw4(%p)::GetFourCCCodes()", PRINT_DEV );
	return (this->*oGetFourCCCodes)( lpNumCodes, lpCodes );
}

HRESULT CDirectDraw4::GetGDISurface( LPDIRECTDRAWSURFACE4* lplpGDIDDSSurface )
{
	ADD_LOG( "IDirectDraw4(%p)::GetGDISurface()", PRINT_DEV );
	return (this->*oGetGDISurface)( lplpGDIDDSSurface );
}

HRESULT CDirectDraw4::GetMonitorFrequency( LPDWORD lpdwFrequency )
{
	ADD_LOG( "IDirectDraw4(%p)::GetMonitorFrequency()", PRINT_DEV );
	return (this->*oGetMonitorFrequency)( lpdwFrequency );
}

HRESULT CDirectDraw4::GetScanLine( LPDWORD lpdwScanLine )
{
	ADD_LOG( "IDirectDraw4(%p)::GetScanLine()", PRINT_DEV );
	return (this->*oGetScanLine)( lpdwScanLine );
}

HRESULT CDirectDraw4::GetVerticalBlankStatus( LPBOOL lpbIsInVB )
{
	ADD_LOG( "IDirectDraw4(%p)::GetVerticalBlankStatus()", PRINT_DEV );
	return (this->*oGetVerticalBlankStatus)( lpbIsInVB );
}

HRESULT CDirectDraw4::Initialize( GUID* lpGUID )
{
	ADD_LOG( "IDirectDraw4(%p)::Initialize()", PRINT_DEV );
	return (this->*oInitialize)( lpGUID );
}

HRESULT CDirectDraw4::RestoreDisplayMode()
{
	ADD_LOG( "IDirectDraw4(%p)::RestoreDisplayMode()", PRINT_DEV );
	return (this->*oRestoreDisplayMode)();
}

HRESULT CDirectDraw4::SetCooperativeLevel( HWND hWnd, DWORD dwFlags )
{
	HRESULT hRet = (this->*oSetCooperativeLevel)( hWnd, dwFlags );
	char buf[512] = {0};
	FormatCooperativeFlags( dwFlags, buf, sizeof(buf) );
	ADD_LOG( "IDirectDraw4(%p)::SetCooperativeLevel( hWnd = 0x%x, dwFlags = %s ) = 0x%x",
		PRINT_DEV, hWnd, buf, hRet );
	return hRet;
}

HRESULT CDirectDraw4::SetDisplayMode( DWORD dwWidth, DWORD dwHeight, DWORD dwBPP )
{
	ADD_LOG( "IDirectDraw4(%p)::SetDisplayMode( dwWidth = %i, dwHeight = %i, dwBPP = %i )", PRINT_DEV, dwWidth, dwHeight, dwBPP );
	return (this->*oSetDisplayMode)( dwWidth, dwHeight, dwBPP );
}

HRESULT CDirectDraw4::WaitForVerticalBlank( DWORD dwFlags, HANDLE hEvent )
{
	ADD_LOG( "IDirectDraw4(%p)::WaitForVerticalBlank()", PRINT_DEV );
	return (this->*oWaitForVerticalBlank)( dwFlags, hEvent );
}

HRESULT CDirectDraw4::GetAvailableVidMem( LPDDSCAPS2 lpDDSCaps2, LPDWORD lpdwTotal, LPDWORD lpdwFree )
{
	ADD_LOG( "IDirectDraw4(%p)::GetAvailableVidMem()", PRINT_DEV );
	return (this->*oGetAvailableVidMem)( lpDDSCaps2, lpdwTotal, lpdwFree );
}

HRESULT CDirectDraw4::GetSurfaceFromDC( HDC hdc, LPDIRECTDRAWSURFACE4* lpDDS )
{
	ADD_LOG( "IDirectDraw4(%p)::GetSurfaceFromDC()", PRINT_DEV );
	return (this->*oGetSurfaceFromDC)( hdc, lpDDS );
}

HRESULT CDirectDraw4::RestoreAllSurfaces()
{
	ADD_LOG( "IDirectDraw4(%p)::RestoreAllSurfaces()", PRINT_DEV );
	return (this->*oRestoreAllSurfaces)();
}

HRESULT CDirectDraw4::TestCooperativeLevel()
{
	ADD_LOG( "IDirectDraw4(%p)::TestCooperativeLevel()", PRINT_DEV );
	return (this->*oTestCooperativeLevel)();
}

HRESULT CDirectDraw4::GetDeviceIdentifier( LPDDDEVICEIDENTIFIER lpdddi, DWORD dwFlags )
{
	ADD_LOG( "IDirectDraw4(%p)::GetDeviceIdentifier()", PRINT_DEV );
	return (this->*oGetDeviceIdentifier)( lpdddi, dwFlags );
}


void Device_HookVMT4( bool first )
{
	DWORD dwOld = 0;
	VirtualProtect( CDirectDraw4_vtable, 28*4, PAGE_EXECUTE_READWRITE, &dwOld );

	HOOK_VTABLE(QueryInterface,0);
	HOOK_VTABLE(AddRef,1);
	HOOK_VTABLE(Release,2);

	HOOK_VTABLE(Compact,3);
	HOOK_VTABLE(CreateClipper,4);
	HOOK_VTABLE(CreatePalette,5);
	HOOK_VTABLE(CreateSurface,6);
	HOOK_VTABLE(DuplicateSurface,7);
	HOOK_VTABLE(EnumDisplayModes,8);
	HOOK_VTABLE(EnumSurfaces,9);
	HOOK_VTABLE(FlipToGDISurface,10);
	HOOK_VTABLE(GetCaps,11);
	HOOK_VTABLE(GetDisplayMode,12);
	HOOK_VTABLE(GetFourCCCodes,13);
	HOOK_VTABLE(GetGDISurface,14);
	HOOK_VTABLE(GetMonitorFrequency,15);
	HOOK_VTABLE(GetScanLine,16);
	HOOK_VTABLE(GetVerticalBlankStatus,17);
	HOOK_VTABLE(Initialize,18);
	HOOK_VTABLE(RestoreDisplayMode,19);
	HOOK_VTABLE(SetCooperativeLevel,20);
	HOOK_VTABLE(SetDisplayMode,21);
	HOOK_VTABLE(WaitForVerticalBlank,22);

	HOOK_VTABLE(GetAvailableVidMem,23);

	HOOK_VTABLE(GetSurfaceFromDC,24);
	HOOK_VTABLE(RestoreAllSurfaces,25);
	HOOK_VTABLE(TestCooperativeLevel,26);
	HOOK_VTABLE(GetDeviceIdentifier,27);

	VirtualProtect( CDirectDraw4_vtable, 28*4, dwOld, &dwOld );
}



void Device_HookVMT( IDirectDraw4* ptr )
{
	if( ptr && ptr != INVALID_HANDLE_VALUE ) {
		CDirectDraw4_vtable = *(VFPTR**)ptr;
		Device_HookVMT4( true );
	} else {
		warning( "Device_HookVMT::IDirectDraw4 invalid PTR: 0x%x", ptr );
	}
}
