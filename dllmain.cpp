/***************************************************************
 * Project: DDrawWrap
 * File: dllmain.cpp
 * Copyright � learn_more
 */

#include "winclude.h"
#pragma comment( lib, "dxguid" )
#include <vector>
#include <algorithm>
using namespace std;

HANDLE hLogFile = 0;
bool bInitializing = true;
void __cdecl add_log( char* arg, ... )
{
	if( bInitializing ) return;
#ifdef LOG_FUNCTION_ENABLED
	if( !hLogFile ) return;
	SAVE_STATE
	char msg[512], *argptr;
	va_start( argptr, arg );
	_vsnprintf_s( msg, sizeof( msg ), arg, argptr );
	va_end( argptr );
	DWORD dwWritten;
	WriteFile( hLogFile, msg, (DWORD)strlen(msg), &dwWritten, NULL );
	WriteFile( hLogFile, "\n", 1, &dwWritten, NULL );
	RESTORE_STATE
#endif
	
}

//for logging warnings :)
void __cdecl warning( char* arg, ... )
{
#ifdef LOG_FUNCTION_ENABLED
	if( !hLogFile ) return;
	SAVE_STATE
	char msg[512], *argptr;
	va_start( argptr, arg );
	_vsnprintf_s( msg, sizeof( msg ), arg, argptr );
	va_end( argptr );
	DWORD dwWritten;
	WriteFile( hLogFile, msg, (DWORD)strlen(msg), &dwWritten, NULL );
	WriteFile( hLogFile, "\n", 1, &dwWritten, NULL );
	RESTORE_STATE
#endif
}

char* VA( char* file, int line, char* arg, ... )
{
	static char msg[2][128];
	static int index = 0;
	char* argptr;
	SAVE_STATE
	index ^= 1;
	va_start( argptr, arg );
	_vsnprintf_s( msg[index], sizeof( msg[index] ), arg, argptr );
	va_end( argptr );
	RESTORE_STATE
	return msg[index];
}

void init_log( HMODULE hLib )
{
#ifdef LOG_FUNCTION_ENABLED
	wchar_t buf[512];
	GetModuleFileNameW( hLib, buf, 512 );
	wchar_t* p = wcsrchr( buf, L'\\' );
	if( p ){
		*p = 0;
		wcscat_s( buf, L"\\log.txt" );
	}
	hLogFile = CreateFileW( buf, FILE_WRITE_DATA, FILE_SHARE_WRITE, NULL, CREATE_ALWAYS, NULL, NULL );
#ifdef _DEBUG
	warning( "DDraw Wrapper v3 by learn_more (Compiled in Debug)" );
#pragma message("WARNING: Compiled in Debug, please switch to Release")
#else
	warning( "DDraw Wrapper v3 by learn_more (Compiled in Release)" );
#endif
	warning( "Started logging\n" );
#endif
}



void close_log()
{
#ifdef LOG_FUNCTION_ENABLED
	warning( "Stopped logging" );
	HANDLE hTmp = hLogFile;
	hLogFile = 0;
	Sleep( 100 );
	CloseHandle( hTmp );
#endif
}


//void init_swat_hooks();



//export the symbol 'load' to force the dll into the process...
extern "C" __declspec(dllexport) int load = 0;


//setup our stuff, dont support dynamic unload (only exitprocess)
BOOL APIENTRY DllMain( HMODULE hModule, DWORD dwReason, LPVOID lpReserved )
{
	if( dwReason == DLL_PROCESS_ATTACH ) {
		//SetMinidumpFilter( hModule );
		init_log( hModule );
		init_ddhooks();
		//init_swat_hooks();
		bInitializing = false;
	} else if( dwReason == DLL_PROCESS_DETACH ) {
		if( lpReserved == 0 ) {
			return FALSE;	//dont support eject
		}
		//else process is closing, then we shutdown logging
		close_log();
	}
	return TRUE;
}