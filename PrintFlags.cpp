/***************************************************************
 * Project: DDrawWrap
 * File: PrintFlags.cpp
 * Copyright � learn_more
 */

#include "winclude.h"

#define LOGFLAG(x) {if((dwFlags&x)&&(buf[0]))strcat_s(buf,bufsize," | "); if(dwFlags&x)strcat_s(buf,bufsize,#x );}



#undef DEFINE_GUID
#define DEFINE_GUID(name, l, w1, w2, b1, b2, b3, b4, b5, b6, b7, b8) \
        EXTERN_C const GUID DECLSPEC_SELECTANY name \
                = { l, w1, w2, { b1, b2,  b3,  b4,  b5,  b6,  b7,  b8 } }

DEFINE_GUID(IID_IDirect3DRampDevice,0xF2086B20,0x259F,0x11CF,0xA3,0x1A,0x00,0xAA,0x00,0xB9,0x33,0x56);
DEFINE_GUID(IID_IDirect3DRGBDevice,0xA4665C60,0x2673,0x11CF,0xA3,0x1A,0x00,0xAA,0x00,0xB9,0x33,0x56);
DEFINE_GUID(IID_IDirect3DHALDevice,0x84E63dE0,0x46AA,0x11CF,0x81,0x6F,0x00,0x00,0xC0,0x20,0x15,0x6E);
DEFINE_GUID(IID_IDirect3DMMXDevice,0x881949a1,0xd6f3,0x11d0,0x89,0xab,0x00,0xa0,0xc9,0x05,0x41,0x29);
DEFINE_GUID(IID_IDirect3DRefDevice,0x50936643,0x13e9,0x11d1,0x89,0xaa,0x0,0xa0,0xc9,0x5,0x41,0x29);
DEFINE_GUID(IID_IDirect3DNullDevice,0x8767df22,0xbacc,0x11d1,0x89,0x69,0x0,0xa0,0xc9,0x6,0x29,0xa8);


void FormatGUID( REFIID iid, char* buf, size_t bufsize )
{
	SAVE_STATE
	if( iid == IID_IDirectDraw ) {
		sprintf_s( buf, bufsize, "IID_IDirectDraw" );
	} else if( iid == IID_IDirectDraw2 ) {
		sprintf_s( buf, bufsize, "IID_IDirectDraw2" );
	} else if( iid == IID_IDirectDraw4 ) {
		sprintf_s( buf, bufsize, "IID_IDirectDraw4" );
	} else if( iid == IID_IDirectDraw7 ) {
		sprintf_s( buf, bufsize, "IID_IDirectDraw7" );

	/************ DirectDraw Surface **************/
	} else if( iid == IID_IDirectDrawSurface ) {
		sprintf_s( buf, bufsize, "IID_IDirectDrawSurface" );
	} else if( iid == IID_IDirectDrawSurface2 ) {
		sprintf_s( buf, bufsize, "IID_IDirectDrawSurface2" );
	} else if( iid == IID_IDirectDrawSurface3 ) {
		sprintf_s( buf, bufsize, "IID_IDirectDrawSurface3" );
	} else if( iid == IID_IDirectDrawSurface4 ) {
		sprintf_s( buf, bufsize, "IID_IDirectDrawSurface4" );
	} else if( iid == IID_IDirectDrawSurface7 ) {
		sprintf_s( buf, bufsize, "IID_IDirectDrawSurface7" );

	/************ DirectDraw Misc **************/
	} else if( iid == IID_IDirectDrawPalette ) {
		sprintf_s( buf, bufsize, "IID_IDirectDrawPalette" );
	} else if( iid == IID_IDirectDrawClipper ) {
		sprintf_s( buf, bufsize, "IID_IDirectDrawClipper" );
	} else if( iid == IID_IDirectDrawColorControl ) {
		sprintf_s( buf, bufsize, "IID_IDirectDrawColorControl" );
	} else if( iid == IID_IDirectDrawGammaControl ) {
		sprintf_s( buf, bufsize, "IID_IDirectDrawGammaControl" );
	} else if( iid == IID_IDirect3DRampDevice ) {
		sprintf_s( buf, bufsize, "IID_IDirect3DRampDevice" );
	} else if( iid == IID_IDirect3DRGBDevice ) {
		sprintf_s( buf, bufsize, "IID_IDirect3DRGBDevice" );
	} else if( iid == IID_IDirect3DHALDevice ) {
		sprintf_s( buf, bufsize, "IID_IDirect3DHALDevice" );
	} else if( iid == IID_IDirect3DMMXDevice ) {
		sprintf_s( buf, bufsize, "IID_IDirect3DMMXDevice" );
	} else if( iid == IID_IDirect3DRefDevice ) {
		sprintf_s( buf, bufsize, "IID_IDirect3DRefDevice" );
	} else if( iid == IID_IDirect3DNullDevice ) {
		sprintf_s( buf, bufsize, "IID_IDirect3DNullDevice" );
	} else {
		sprintf_s( buf, bufsize, "IID_UNKNOWN{0x%x,0x%x,0x%x,0x%x,0x%x,0x%x,"
			"0x%x,0x%x,0x%x,0x%x,0x%x}", iid.Data1, iid.Data2, iid.Data2,
			iid.Data4[0], iid.Data4[1], iid.Data4[2], iid.Data4[3], iid.Data4[4],
			iid.Data4[5], iid.Data4[6], iid.Data4[7] );
	}
	RESTORE_STATE
}

void FormatCooperativeFlags( DWORD dwFlags, char* buf, size_t bufsize )
{
	SAVE_STATE
	LOGFLAG( DDSCL_FULLSCREEN );
	LOGFLAG( DDSCL_ALLOWREBOOT );
	LOGFLAG( DDSCL_NOWINDOWCHANGES );
	LOGFLAG( DDSCL_NORMAL );
	LOGFLAG( DDSCL_EXCLUSIVE );
	LOGFLAG( DDSCL_ALLOWMODEX );
	LOGFLAG( DDSCL_SETFOCUSWINDOW );
	LOGFLAG( DDSCL_SETDEVICEWINDOW );
	LOGFLAG( DDSCL_CREATEDEVICEWINDOW );
	LOGFLAG( DDSCL_MULTITHREADED );
	LOGFLAG( DDSCL_FPUSETUP );
	LOGFLAG( DDSCL_FPUPRESERVE );
	RESTORE_STATE
}

void FormatLockFlags( DWORD dwFlags, char* buf, size_t bufsize )
{
	SAVE_STATE
	LOGFLAG( DDLOCK_DONOTWAIT );
	LOGFLAG( DDLOCK_EVENT );
	LOGFLAG( DDLOCK_NOOVERWRITE );
	LOGFLAG( DDLOCK_NOSYSLOCK );
	LOGFLAG( DDLOCK_DISCARDCONTENTS );
	LOGFLAG( DDLOCK_READONLY );
	LOGFLAG( DDLOCK_SURFACEMEMORYPTR );
	LOGFLAG( DDLOCK_WAIT );
	LOGFLAG( DDLOCK_WRITEONLY );
	RESTORE_STATE
}

void FormatColorkeyFlags( DWORD dwFlags, char* buf, size_t bufsize )
{
	SAVE_STATE
	LOGFLAG( DDCKEY_COLORSPACE );
	LOGFLAG( DDCKEY_DESTBLT );
	LOGFLAG( DDCKEY_DESTOVERLAY );
	LOGFLAG( DDCKEY_SRCBLT );
	LOGFLAG( DDCKEY_SRCOVERLAY );
	RESTORE_STATE
}


void LogCaps( LPVOID obj, char* devname, HRESULT hRet, LPDDCAPS lpDDDriverCaps, LPDDCAPS lpDDHELCaps )
{
	SAVE_STATE
	ADD_LOG( "%s(%p)::GetCaps( %p, %p ) = 0x%x", devname, obj, lpDDDriverCaps, lpDDHELCaps, hRet );
	if( lpDDDriverCaps ) {		//make sure its no NULLPTR
		//lpDDDriverCaps == hardware caps
		switch( lpDDDriverCaps->dwSize ) {
			case sizeof(DDCAPS_DX1):
				ADD_LOG( "lpDDDriverCaps: DDCAPS_DX1" );
				break;
			case sizeof(DDCAPS_DX3):
				ADD_LOG( "lpDDDriverCaps: DDCAPS_DX3" );
				break;
			case sizeof(DDCAPS_DX5):
				ADD_LOG( "lpDDDriverCaps: DDCAPS_DX5" );
				break;
			case sizeof(DDCAPS_DX7):	//same size as 6 so only check 7
				ADD_LOG( "lpDDDriverCaps: DDCAPS_DX7" );
				break;
			default:
				ADD_LOG( "ERR: lpDDDriverCaps: unknown size" );
				break;
		}
	}
	if( lpDDHELCaps ) {			//make sure its no NULLPTR
		//lpDDHELCaps == software caps
		//do the same here
	}
	RESTORE_STATE
}

void FormatSurfaceDesc( LPDDSURFACEDESC lpSurf, char* buf, size_t bufsize )
{
	SAVE_STATE
	if( !lpSurf ) {
		strcpy_s( buf, bufsize, "{0}" );
		return;
	}
	if( lpSurf->dwFlags == DDSD_ALL ) {
		strcpy_s( buf, bufsize, "{ DDSD_ALL" );
	} else {
		strcpy_s( buf, bufsize, "{ DDSD_" );
	}

	if( lpSurf->dwFlags & DDSD_HEIGHT ) {
		strcat_s( buf, bufsize, va( ", dwHeight: %i", lpSurf->dwHeight ) );
	}
	if( lpSurf->dwFlags & DDSD_WIDTH ) {
		strcat_s( buf, bufsize, va( ", dwWidth: %i", lpSurf->dwWidth ) );
	}
	if( lpSurf->dwFlags & DDSD_PITCH ) {
		strcat_s( buf, bufsize, va( ", lPitch: %i", lpSurf->lPitch ) );
	}
	if( lpSurf->dwFlags & DDSD_BACKBUFFERCOUNT ) {
		strcat_s( buf, bufsize, va( ", dwBackBufferCount: %i", lpSurf->dwBackBufferCount ) );
	}
	if( lpSurf->dwFlags & DDSD_MIPMAPCOUNT ) {
		strcat_s( buf, bufsize, va( ", dwMipMapCount: %i", lpSurf->dwMipMapCount ) );
	}
	if( lpSurf->dwFlags & DDSD_ZBUFFERBITDEPTH ) {
		strcat_s( buf, bufsize, va( ", dwZBufferBitDepth: %i", lpSurf->dwZBufferBitDepth ) );
	}
	if( lpSurf->dwFlags & DDSD_REFRESHRATE ) {
		strcat_s( buf, bufsize, va( ", dwRefreshRate: %i", lpSurf->dwRefreshRate ) );
	}
	if( lpSurf->dwFlags & DDSD_ALPHABITDEPTH ) {
		strcat_s( buf, bufsize, va( ", dwAlphaBitDepth: %i", lpSurf->dwAlphaBitDepth ) );
	}
	if( lpSurf->dwFlags & DDSD_LPSURFACE ) {
		strcat_s( buf, bufsize, va( ", lpSurface: 0x%x", lpSurf->lpSurface ) );
	}
	if( lpSurf->dwFlags & DDSD_CKDESTOVERLAY ) {
		strcat_s( buf, bufsize, va( ", ddckCKDestOverlay: {0x%x,0x%x)", lpSurf->ddckCKDestOverlay.dwColorSpaceLowValue, lpSurf->ddckCKDestOverlay.dwColorSpaceHighValue ) );
	}
	if( lpSurf->dwFlags & DDSD_CKDESTBLT ) {
		strcat_s( buf, bufsize, va( ", ddckCKDestBlt: {0x%x,0x%x)", lpSurf->ddckCKDestBlt.dwColorSpaceLowValue, lpSurf->ddckCKDestBlt.dwColorSpaceHighValue ) );
	}
	if( lpSurf->dwFlags & DDSD_CKSRCOVERLAY ) {
		strcat_s( buf, bufsize, va( ", ddckCKSrcOverlay: {0x%x,0x%x)", lpSurf->ddckCKSrcOverlay.dwColorSpaceLowValue, lpSurf->ddckCKSrcOverlay.dwColorSpaceHighValue ) );
	}
	if( lpSurf->dwFlags & DDSD_CKSRCBLT ) {
		strcat_s( buf, bufsize, va( ", ddckCKSrcBlt: {0x%x,0x%x)", lpSurf->ddckCKSrcBlt.dwColorSpaceLowValue, lpSurf->ddckCKSrcBlt.dwColorSpaceHighValue ) );
	}
	if( lpSurf->dwFlags & DDSD_PIXELFORMAT ) {
		strcat_s( buf, bufsize, va( ", ddpfPixelFormat: TODO" ) );	//TODO:
	}
	if( lpSurf->dwFlags & DDSD_CAPS ) {
		strcat_s( buf, bufsize, va( ", ddsCaps: TODO" ) );	//TODO:
	}
	RESTORE_STATE
}


void FormatSurfaceDesc2( LPDDSURFACEDESC2 lpSurf, char* buf, size_t bufsize )
{
	SAVE_STATE
	if( !lpSurf ) {
		strcpy_s( buf, bufsize, "{0}" );
		return;
	}
	if( lpSurf->dwFlags == DDSD_ALL ) {
		strcpy_s( buf, bufsize, "{ DDSD_ALL" );
	} else {
		strcpy_s( buf, bufsize, "{ DDSD_" );
	}

	if( lpSurf->dwFlags & DDSD_HEIGHT ) {
		strcat_s( buf, bufsize, va( ", dwHeight: %i", lpSurf->dwHeight ) );
	}
	if( lpSurf->dwFlags & DDSD_WIDTH ) {
		strcat_s( buf, bufsize, va( ", dwWidth: %i", lpSurf->dwWidth ) );
	}
	if( lpSurf->dwFlags & DDSD_PITCH ) {
		strcat_s( buf, bufsize, va( ", lPitch: %i", lpSurf->lPitch ) );
	}
	if( lpSurf->dwFlags & DDSD_LINEARSIZE ) {
		strcat_s( buf, bufsize, va( ", dwLinearSize: %i", lpSurf->dwLinearSize ) );
	}
	if( lpSurf->dwFlags & DDSD_BACKBUFFERCOUNT ) {
		strcat_s( buf, bufsize, va( ", dwBackBufferCount: %i", lpSurf->dwBackBufferCount ) );
	}
	if( lpSurf->dwFlags & DDSD_DEPTH ) {
		strcat_s( buf, bufsize, va( ", dwDepth: %i", lpSurf->dwDepth ) );
	}
	if( lpSurf->dwFlags & DDSD_MIPMAPCOUNT ) {
		strcat_s( buf, bufsize, va( ", dwMipMapCount: %i", lpSurf->dwMipMapCount ) );
	}
	if( lpSurf->dwFlags & DDSD_REFRESHRATE ) {
		strcat_s( buf, bufsize, va( ", dwRefreshRate: %i", lpSurf->dwRefreshRate ) );
	}
	if( lpSurf->dwFlags & DDSD_SRCVBHANDLE ) {
		strcat_s( buf, bufsize, va( ", dwSrcVBHandle: %i", lpSurf->dwSrcVBHandle ) );
	}
	if( lpSurf->dwFlags & DDSD_ALPHABITDEPTH ) {
		strcat_s( buf, bufsize, va( ", dwAlphaBitDepth: %i", lpSurf->dwAlphaBitDepth ) );
	}
	if( lpSurf->dwFlags & DDSD_LPSURFACE ) {
		strcat_s( buf, bufsize, va( ", lpSurface: 0x%x", lpSurf->lpSurface ) );
	}
	if( lpSurf->dwFlags & DDSD_CKDESTOVERLAY ) {
		strcat_s( buf, bufsize, va( ", ddckCKDestOverlay: {0x%x,0x%x)", lpSurf->ddckCKDestOverlay.dwColorSpaceLowValue, lpSurf->ddckCKDestOverlay.dwColorSpaceHighValue ) );
	}
	if( lpSurf->dwFlags & DDSD_CKDESTBLT ) {
		strcat_s( buf, bufsize, va( ", ddckCKDestBlt: {0x%x,0x%x)", lpSurf->ddckCKDestBlt.dwColorSpaceLowValue, lpSurf->ddckCKDestBlt.dwColorSpaceHighValue ) );
	}
	if( lpSurf->dwFlags & DDSD_CKSRCOVERLAY ) {
		strcat_s( buf, bufsize, va( ", ddckCKSrcOverlay: {0x%x,0x%x)", lpSurf->ddckCKSrcOverlay.dwColorSpaceLowValue, lpSurf->ddckCKSrcOverlay.dwColorSpaceHighValue ) );
	}
	if( lpSurf->dwFlags & DDSD_CKSRCBLT ) {
		strcat_s( buf, bufsize, va( ", ddckCKSrcBlt: {0x%x,0x%x)", lpSurf->ddckCKSrcBlt.dwColorSpaceLowValue, lpSurf->ddckCKSrcBlt.dwColorSpaceHighValue ) );
	}
	if( lpSurf->dwFlags & DDSD_PIXELFORMAT ) {
		strcat_s( buf, bufsize, va( ", ddpfPixelFormat: TODO" ) );	//TODO:
	}
	if( lpSurf->dwFlags & DDSD_FVF ) {
		strcat_s( buf, bufsize, va( ", dwFVF: 0x%x", lpSurf->dwFVF ) );
	}
	if( lpSurf->dwFlags & DDSD_CAPS ) {
		strcat_s( buf, bufsize, va( ", ddsCaps: TODO" ) );	//TODO:
	}
	if( lpSurf->dwFlags & DDSD_TEXTURESTAGE ) {
		strcat_s( buf, bufsize, va( ", dwTextureStage: %i", lpSurf->dwTextureStage ) );
	}
	RESTORE_STATE
}