/***************************************************************
 * Project: DDrawWrap
 * File: IDirectDrawGammaControl.cpp
 * Copyright � learn_more
 */


#include "../winclude.h"

class CDirectDrawGammaControl {
public:
/*** IUnknown methods ***/
HRESULT __stdcall QueryInterface( REFIID riid, LPVOID* ppvObj );
ULONG __stdcall AddRef();
ULONG __stdcall Release();
/*** IDirectDrawGammaControl methods ***/
HRESULT __stdcall GetGammaRamp( DWORD dwFlags, LPDDGAMMARAMP lpRampData );
HRESULT __stdcall SetGammaRamp( DWORD dwFlags, LPDDGAMMARAMP lpRampData );
};


/*** IUnknown methods ***/
HRESULT (__stdcall CDirectDrawGammaControl::*oQueryInterface)( REFIID riid, LPVOID* ppvObj );
ULONG (__stdcall CDirectDrawGammaControl::*oAddRef)();
ULONG (__stdcall CDirectDrawGammaControl::*oRelease)();
/*** IDirectDrawGammaControl methods ***/
HRESULT (__stdcall CDirectDrawGammaControl::*oGetGammaRamp)( DWORD dwFlags, LPDDGAMMARAMP lpRampData );
HRESULT (__stdcall CDirectDrawGammaControl::*oSetGammaRamp)( DWORD dwFlags, LPDDGAMMARAMP lpRampData );


typedef void (__stdcall CDirectDrawGammaControl::*VFPTR)();
VFPTR* CDirectDrawGammaControl_vtable = 0;

#define HOOK_PREFIX	CDirectDrawGammaControl


HRESULT CDirectDrawGammaControl::QueryInterface( REFIID riid, LPVOID* ppvObj )
{
	char buf[100];
	FormatGUID( riid, buf, sizeof(buf) );
	ADD_LOG( "IDirectDrawGammaControl(%p)::QueryInterface( %s )", PRINT_DEV, buf );
	return (this->*oQueryInterface)( riid, ppvObj );
}

ULONG CDirectDrawGammaControl::AddRef()
{
	ULONG pRef = (this->*oAddRef)();
	ADD_LOG( "IDirectDrawGammaControl(%p)::AddRef( %i )", PRINT_DEV, pRef );
	return pRef;
}

ULONG CDirectDrawGammaControl::Release()
{
	ULONG uRet = (this->*oRelease)();
	ADD_LOG( "IDirectDrawGammaControl(%p)::Release( %i )", PRINT_DEV, uRet );
	return uRet;
}

HRESULT CDirectDrawGammaControl::GetGammaRamp( DWORD dwFlags, LPDDGAMMARAMP lpRampData )
{
	ADD_LOG( "IDirectDrawGammaControl(%p)::GetGammaRamp()", PRINT_DEV );
	return (this->*oGetGammaRamp)( dwFlags, lpRampData );
}

HRESULT CDirectDrawGammaControl::SetGammaRamp( DWORD dwFlags, LPDDGAMMARAMP lpRampData )
{
	ADD_LOG( "IDirectDrawGammaControl(%p)::SetGammaRamp()", PRINT_DEV );
	return (this->*oSetGammaRamp)( dwFlags, lpRampData );
}

void Device_HookVMTGam( bool first )
{
	DWORD dwOld = 0;
	VirtualProtect( CDirectDrawGammaControl_vtable, 5*4, PAGE_EXECUTE_READWRITE, &dwOld );

	HOOK_VTABLE(QueryInterface,0);
	HOOK_VTABLE(AddRef,1);
	HOOK_VTABLE(Release,2);

	HOOK_VTABLE(GetGammaRamp,3);
	HOOK_VTABLE(SetGammaRamp,4);

	VirtualProtect( CDirectDrawGammaControl_vtable, 5*4, dwOld, &dwOld );
}


void Device_HookVMT( IDirectDrawGammaControl* ptr )
{
	if( ptr && ptr != INVALID_HANDLE_VALUE ) {
		CDirectDrawGammaControl_vtable = *(VFPTR**)ptr;
		Device_HookVMTGam( true );
	} else {
		warning( "Device_HookVMT::IDirectDrawGammaControl invalid PTR: 0x%x", ptr );
	}
}
