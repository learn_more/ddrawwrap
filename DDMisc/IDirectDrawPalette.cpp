/***************************************************************
 * Project: DDrawWrap
 * File: IDirectDrawPalette.cpp
 * Copyright � learn_more
 */


#include "../winclude.h"

class CDirectDrawPalette {
public:
/*** IUnknown methods ***/
HRESULT __stdcall QueryInterface( REFIID riid, LPVOID* ppvObj );
ULONG __stdcall AddRef();
ULONG __stdcall Release();
/*** IDirectDrawPalette methods ***/
HRESULT __stdcall GetCaps( LPDWORD lpdwCaps );
HRESULT __stdcall GetEntries( DWORD dwFlags, DWORD dwBase, DWORD dwNumEntries, LPPALETTEENTRY lpEntries );
HRESULT __stdcall Initialize( LPDIRECTDRAW lpDD, DWORD dwFlags, LPPALETTEENTRY lpDDColorTable );
HRESULT __stdcall SetEntries( DWORD dwFlags, DWORD dwStartingEntry, DWORD dwCount, LPPALETTEENTRY lpEntries );
};

/*** IUnknown methods ***/
HRESULT (__stdcall CDirectDrawPalette::*oQueryInterface)( REFIID riid, LPVOID* ppvObj );
ULONG (__stdcall CDirectDrawPalette::*oAddRef)();
ULONG (__stdcall CDirectDrawPalette::*oRelease)();
/*** IDirectDrawPalette methods ***/
HRESULT (__stdcall CDirectDrawPalette::*oGetCaps)( LPDWORD lpdwCaps );
HRESULT (__stdcall CDirectDrawPalette::*oGetEntries)( DWORD dwFlags, DWORD dwBase, DWORD dwNumEntries, LPPALETTEENTRY lpEntries );
HRESULT (__stdcall CDirectDrawPalette::*oInitialize)( LPDIRECTDRAW lpDD, DWORD dwFlags, LPPALETTEENTRY lpDDColorTable );
HRESULT (__stdcall CDirectDrawPalette::*oSetEntries)( DWORD dwFlags, DWORD dwStartingEntry, DWORD dwCount, LPPALETTEENTRY lpEntries );


typedef void (__stdcall CDirectDrawPalette::*VFPTR)();
VFPTR* CDirectDrawPalette_vtable = 0;

#define HOOK_PREFIX	CDirectDrawPalette


HRESULT CDirectDrawPalette::QueryInterface( REFIID riid, LPVOID* ppvObj )
{
	char buf[100];
	FormatGUID( riid, buf, sizeof(buf) );
	ADD_LOG( "IDirectDrawPalette(%p)::QueryInterface( %s )", PRINT_DEV, buf );
	return (this->*oQueryInterface)( riid, ppvObj );
}

ULONG CDirectDrawPalette::AddRef()
{
	ULONG pRef = (this->*oAddRef)();
	ADD_LOG( "IDirectDrawPalette(%p)::AddRef( %i )", PRINT_DEV, pRef );
	return pRef;
}

ULONG CDirectDrawPalette::Release()
{
	ULONG uRet = (this->*oRelease)();
	ADD_LOG( "IDirectDrawPalette(%p)::Release( %i )", PRINT_DEV, uRet );
	return uRet;
}

HRESULT CDirectDrawPalette::GetCaps( LPDWORD lpdwCaps )
{
	ADD_LOG( "IDirectDrawPalette(%p)::GetCaps()", PRINT_DEV );
	return (this->*oGetCaps)( lpdwCaps );
}

HRESULT CDirectDrawPalette::GetEntries( DWORD dwFlags, DWORD dwBase, DWORD dwNumEntries, LPPALETTEENTRY lpEntries )
{
	ADD_LOG( "IDirectDrawPalette(%p)::GetEntries()", PRINT_DEV );
	return (this->*oGetEntries)( dwFlags, dwBase, dwNumEntries, lpEntries );
}

HRESULT CDirectDrawPalette::Initialize( LPDIRECTDRAW lpDD, DWORD dwFlags, LPPALETTEENTRY lpDDColorTable )
{
	ADD_LOG( "IDirectDrawPalette(%p)::Initialize()", PRINT_DEV );
	return (this->*oInitialize)( lpDD, dwFlags, lpDDColorTable );
}

HRESULT CDirectDrawPalette::SetEntries( DWORD dwFlags, DWORD dwStartingEntry, DWORD dwCount, LPPALETTEENTRY lpEntries )
{
	ADD_LOG( "IDirectDrawPalette(%p)::SetEntries()", PRINT_DEV );
	return (this->*oSetEntries)( dwFlags, dwStartingEntry, dwCount, lpEntries );
}


void Device_HookVMTPal( bool first )
{
	DWORD dwOld = 0;
	VirtualProtect( CDirectDrawPalette_vtable, 7*4, PAGE_EXECUTE_READWRITE, &dwOld );

	HOOK_VTABLE(QueryInterface,0);
	HOOK_VTABLE(AddRef,1);
	HOOK_VTABLE(Release,2);

	HOOK_VTABLE(GetCaps,3);
	HOOK_VTABLE(GetEntries,4);
	HOOK_VTABLE(Initialize,5);
	HOOK_VTABLE(SetEntries,6);

	VirtualProtect( CDirectDrawPalette_vtable, 7*4, dwOld, &dwOld );
}


void Device_HookVMT( IDirectDrawPalette* ptr )
{
	if( ptr && ptr != INVALID_HANDLE_VALUE ) {
		CDirectDrawPalette_vtable = *(VFPTR**)ptr;
		Device_HookVMTPal( true );
	} else {
		warning( "Device_HookVMT::IDirectDrawPalette invalid PTR: 0x%x", ptr );
	}
}
