/***************************************************************
 * Project: DDrawWrap
 * File: IDirectDrawColorControl.cpp
 * Copyright � learn_more
 */


#include "../winclude.h"

class CDirectDrawColorControl {
public:
/*** IUnknown methods ***/
HRESULT __stdcall QueryInterface( REFIID riid, LPVOID* ppvObj );
ULONG __stdcall AddRef();
ULONG __stdcall Release();
/*** IDirectDrawColorControl methods ***/
HRESULT __stdcall GetColorControls( LPDDCOLORCONTROL lpColorControl );
HRESULT __stdcall SetColorControls( LPDDCOLORCONTROL lpColorControl );
};

/*** IUnknown methods ***/
HRESULT (__stdcall CDirectDrawColorControl::*oQueryInterface)( REFIID riid, LPVOID* ppvObj );
ULONG (__stdcall CDirectDrawColorControl::*oAddRef)();
ULONG (__stdcall CDirectDrawColorControl::*oRelease)();
/*** IDirectDrawColorControl methods ***/
HRESULT (__stdcall CDirectDrawColorControl::*oGetColorControls)( LPDDCOLORCONTROL lpColorControl );
HRESULT (__stdcall CDirectDrawColorControl::*oSetColorControls)( LPDDCOLORCONTROL lpColorControl );


typedef void (__stdcall CDirectDrawColorControl::*VFPTR)();
VFPTR* CDirectDrawColorControl_vtable = 0;

#define HOOK_PREFIX	CDirectDrawColorControl


HRESULT CDirectDrawColorControl::QueryInterface( REFIID riid, LPVOID* ppvObj )
{
	char buf[100];
	FormatGUID( riid, buf, sizeof(buf) );
	ADD_LOG( "IDirectDrawColorControl(%p)::QueryInterface( %s )", PRINT_DEV, buf );
	return (this->*oQueryInterface)( riid, ppvObj );
}

ULONG CDirectDrawColorControl::AddRef()
{
	ULONG pRef = (this->*oAddRef)();
	ADD_LOG( "IDirectDrawColorControl(%p)::AddRef( %i )", PRINT_DEV, pRef );
	return pRef;
}

ULONG CDirectDrawColorControl::Release()
{
	ULONG uRet = (this->*oRelease)();
	ADD_LOG( "IDirectDrawColorControl(%p)::Release( %i )", PRINT_DEV, uRet );
	return uRet;
}

HRESULT CDirectDrawColorControl::GetColorControls( LPDDCOLORCONTROL lpColorControl )
{
	ADD_LOG( "IDirectDrawColorControl(%p)::GetColorControls()", PRINT_DEV );
	return (this->*oGetColorControls)( lpColorControl );
}

HRESULT CDirectDrawColorControl::SetColorControls( LPDDCOLORCONTROL lpColorControl )
{
	ADD_LOG( "IDirectDrawColorControl(%p)::SetColorControls()", PRINT_DEV );
	return (this->*oSetColorControls)( lpColorControl );
}


void Device_HookVMTCol( bool first )
{
	DWORD dwOld = 0;
	VirtualProtect( CDirectDrawColorControl_vtable, 5*4, PAGE_EXECUTE_READWRITE, &dwOld );

	HOOK_VTABLE(QueryInterface,0);
	HOOK_VTABLE(AddRef,1);
	HOOK_VTABLE(Release,2);

	HOOK_VTABLE(GetColorControls,3);
	HOOK_VTABLE(SetColorControls,4);

	VirtualProtect( CDirectDrawColorControl_vtable, 5*4, dwOld, &dwOld );
}


void Device_HookVMT( IDirectDrawColorControl* ptr )
{
	if( ptr && ptr != INVALID_HANDLE_VALUE ) {
		CDirectDrawColorControl_vtable = *(VFPTR**)ptr;
		Device_HookVMTCol( true );
	} else {
		warning( "Device_HookVMT::IDirectDrawColorControl invalid PTR: 0x%x", ptr );
	}
}
