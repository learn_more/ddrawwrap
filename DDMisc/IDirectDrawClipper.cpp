/***************************************************************
 * Project: DDrawWrap
 * File: IDirectDrawClipper.cpp
 * Copyright � learn_more
 */


#include "../winclude.h"

class CDirectDrawClipper {
public:
/*** IUnknown methods ***/
HRESULT __stdcall QueryInterface( REFIID riid, LPVOID* ppvObj );
ULONG __stdcall AddRef();
ULONG __stdcall Release();
/*** IDirectDrawClipper methods ***/
HRESULT __stdcall GetClipList( LPRECT lpRect, LPRGNDATA lpClipList, LPDWORD lpdwSize );
HRESULT __stdcall GetHWnd( HWND* lphWnd );
HRESULT __stdcall Initialize( LPDIRECTDRAW lpDD, DWORD dwFlags );
HRESULT __stdcall IsClipListChanged( BOOL* lpbChanged );
HRESULT __stdcall SetClipList( LPRGNDATA lpClipList, DWORD dwFlags );
HRESULT __stdcall SetHWnd( DWORD dwFlags, HWND hWnd );
};

/*** IUnknown methods ***/
HRESULT (__stdcall CDirectDrawClipper::*oQueryInterface)( REFIID riid, LPVOID* ppvObj );
ULONG (__stdcall CDirectDrawClipper::*oAddRef)();
ULONG (__stdcall CDirectDrawClipper::*oRelease)();
/*** IDirectDrawClipper methods ***/
HRESULT (__stdcall CDirectDrawClipper::*oGetClipList)( LPRECT lpRect, LPRGNDATA lpClipList, LPDWORD lpdwSize );
HRESULT (__stdcall CDirectDrawClipper::*oGetHWnd)( HWND* lphWnd );
HRESULT (__stdcall CDirectDrawClipper::*oInitialize)( LPDIRECTDRAW lpDD, DWORD dwFlags );
HRESULT (__stdcall CDirectDrawClipper::*oIsClipListChanged)( BOOL* lpbChanged );
HRESULT (__stdcall CDirectDrawClipper::*oSetClipList)( LPRGNDATA lpClipList, DWORD dwFlags );
HRESULT (__stdcall CDirectDrawClipper::*oSetHWnd)( DWORD dwFlags, HWND hWnd );


typedef void (__stdcall CDirectDrawClipper::*VFPTR)();
VFPTR* CDirectDrawClipper_vtable = 0;

#define HOOK_PREFIX	CDirectDrawClipper



HRESULT CDirectDrawClipper::QueryInterface( REFIID riid, LPVOID* ppvObj )
{
	char buf[100];
	FormatGUID( riid, buf, sizeof(buf) );
	ADD_LOG( "IDirectDrawClipper(%p)::QueryInterface( %s )", PRINT_DEV, buf );
	return (this->*oQueryInterface)( riid, ppvObj );
}

ULONG CDirectDrawClipper::AddRef()
{
	ULONG pRef = (this->*oAddRef)();
	ADD_LOG( "IDirectDrawClipper(%p)::AddRef( %i )", PRINT_DEV, pRef );
	return pRef;
}

ULONG CDirectDrawClipper::Release()
{
	ULONG uRet = (this->*oRelease)();
	ADD_LOG( "IDirectDrawClipper(%p)::Release( %i )", PRINT_DEV, uRet );
	return uRet;
}

HRESULT CDirectDrawClipper::GetClipList( LPRECT lpRect, LPRGNDATA lpClipList, LPDWORD lpdwSize )
{
	ADD_LOG( "IDirectDrawClipper(%p)::GetClipList()", PRINT_DEV );
	return (this->*oGetClipList)( lpRect, lpClipList, lpdwSize );
}

HRESULT CDirectDrawClipper::GetHWnd( HWND* lphWnd )
{
	ADD_LOG( "IDirectDrawClipper(%p)::GetHWnd()", PRINT_DEV );
	return (this->*oGetHWnd)( lphWnd );
}

HRESULT CDirectDrawClipper::Initialize( LPDIRECTDRAW lpDD, DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawClipper(%p)::Initialize()", PRINT_DEV );
	return (this->*oInitialize)( lpDD, dwFlags );
}

HRESULT CDirectDrawClipper::IsClipListChanged( BOOL* lpbChanged )
{
	ADD_LOG( "IDirectDrawClipper(%p)::IsClipListChanged()", PRINT_DEV );
	return (this->*oIsClipListChanged)( lpbChanged );
}

HRESULT CDirectDrawClipper::SetClipList( LPRGNDATA lpClipList, DWORD dwFlags )
{
	ADD_LOG( "IDirectDrawClipper(%p)::SetClipList()", PRINT_DEV );
	return (this->*oSetClipList)( lpClipList, dwFlags );
}

HRESULT CDirectDrawClipper::SetHWnd( DWORD dwFlags, HWND hWnd )
{
	ADD_LOG( "IDirectDrawClipper(%p)::SetHWnd()", PRINT_DEV );
	return (this->*oSetHWnd)( dwFlags, hWnd );
}


void Device_HookVMTClip( bool first )
{
	DWORD dwOld = 0;
	VirtualProtect( CDirectDrawClipper_vtable, 9*4, PAGE_EXECUTE_READWRITE, &dwOld );

	HOOK_VTABLE(QueryInterface,0);
	HOOK_VTABLE(AddRef,1);
	HOOK_VTABLE(Release,2);

	HOOK_VTABLE(GetClipList,3);
	HOOK_VTABLE(GetHWnd,4);
	HOOK_VTABLE(Initialize,5);
	HOOK_VTABLE(IsClipListChanged,6);
	HOOK_VTABLE(SetClipList,7);
	HOOK_VTABLE(SetHWnd,8);

	VirtualProtect( CDirectDrawClipper_vtable, 9*4, dwOld, &dwOld );
}


void Device_HookVMT( IDirectDrawClipper* ptr )
{
	if( ptr && ptr != INVALID_HANDLE_VALUE ) {
		CDirectDrawClipper_vtable = *(VFPTR**)ptr;
		Device_HookVMTClip( true );
	} else {
		warning( "Device_HookVMT::IDirectDrawClipper invalid PTR: 0x%x", ptr );
	}
}
