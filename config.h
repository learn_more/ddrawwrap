/***************************************************************
 * Project: DDrawWrap
 * File: config.h
 * Copyright � learn_more
 */

#ifndef __CONFIG_H__
#define __CONFIG_H__


#define LOG_FUNCTION_ENABLED	//comment this to disable all logging


#ifdef LOG_FUNCTION_ENABLED
#define LOG_ALL_CALLS			//comment this to disable all function calls to be logged
#endif


#define WINVER 0x0501
#define _WIN32_WINNT 0x0501
#define _WIN32_WINDOWS 0x0410
#define _WIN32_IE 0x0600

#endif //__CONFIG_H__