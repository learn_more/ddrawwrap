/***************************************************************
 * Project: DDrawWrap
 * File: dllmain.cpp
 * Copyright � learn_more
 */

#include "winclude.h"
#include <dbghelp.h>
#include <tchar.h>


TCHAR crash_buf[512];

LONG WINAPI UnhandledExFilter( PEXCEPTION_POINTERS ExPtr )
{
    BOOL (WINAPI* pMiniDumpWriteDump)( IN HANDLE hProcess, IN DWORD ProcessId, IN HANDLE hFile, IN MINIDUMP_TYPE DumpType, IN CONST PMINIDUMP_EXCEPTION_INFORMATION ExceptionParam, OPTIONAL IN CONST PMINIDUMP_USER_STREAM_INFORMATION UserStreamParam, OPTIONAL IN CONST PMINIDUMP_CALLBACK_INFORMATION CallbackParam OPTIONAL ) = NULL;

    HMODULE hLib = LoadLibrary( _T("dbghelp") );
    if( hLib )
        *(void**)&pMiniDumpWriteDump = (void*)GetProcAddress( hLib, "MiniDumpWriteDump" );
    
    TCHAR buf[MAX_PATH];

    if( pMiniDumpWriteDump ) {
        int rnd;
        __asm push edx
        __asm rdtsc
        __asm pop edx
        __asm mov rnd, eax
        rnd &= 0xFFFF;
        wsprintf( buf, _T("Crash_%x.dmp"), rnd );
		_tcscat_s( crash_buf, 512, buf );
        HANDLE hFile = CreateFile( crash_buf, GENERIC_READ | GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL ); 

        if( hFile != INVALID_HANDLE_VALUE ) {
            MINIDUMP_EXCEPTION_INFORMATION md; 
            md.ThreadId           = GetCurrentThreadId(); 
            md.ExceptionPointers  = ExPtr; 
            md.ClientPointers     = FALSE; 
            BOOL win = pMiniDumpWriteDump( GetCurrentProcess(), GetCurrentProcessId(), hFile, MiniDumpNormal, &md, 0, 0 ); 

            if( !win) 
                wsprintf( buf, _T("MiniDumpWriteDump failed. Error: %u \n(%s)"), GetLastError(), crash_buf ); 
            else 
                wsprintf( buf, _T("Minidump created:\n%s"), crash_buf ); 
            CloseHandle( hFile ); 

        } else {
            wsprintf( buf, _T("Could not create minidump:\n%s"), crash_buf );
        }
    } else {
        wsprintf( buf, _T("Could not load dbghelp") );
    }
    MessageBox( NULL, buf, _T("Unhandled exception"), MB_OK|MB_ICONERROR|MB_SERVICE_NOTIFICATION );
    ExitProcess( 0 );    //do whatever u want here
}

void SetMinidumpFilter( HMODULE hMod )
{
	GetModuleFileName( hMod, crash_buf, 512 );
	TCHAR* p = _tcsrchr( crash_buf, _T('\\') );
	if( p ) *(++p) = 0;

	SetUnhandledExceptionFilter( UnhandledExFilter );
}