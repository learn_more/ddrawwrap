/***************************************************************
 * Project: DDrawWrap
 * File: ddhooks.cpp
 * Copyright � learn_more
 */


#include "winclude.h"
#include "detours.h"	//detours 1.5: http://www.ucdownloads.com/downloads/downloads.php?do=file&id=2776


void HookSurfaces( LPDIRECTDRAW lpDD )
{
	HRESULT hRet;
	LPDIRECTDRAWSURFACE lpSurf = 0;
	DDSURFACEDESC ddsd = {0};
	ddsd.dwSize = sizeof(ddsd);
	ddsd.dwFlags = DDSD_CAPS;
	ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE;

	lpDD->SetCooperativeLevel( NULL, DDSCL_NORMAL );	// Without this, a surface cannot be created.
	lpDD->SetDisplayMode( 320, 230, 16 );				// To make sure we get a decent surface.
	if( (hRet = lpDD->CreateSurface( &ddsd, &lpSurf, NULL )) == DD_OK ) {
		LPDIRECTDRAWSURFACE2 lpSurf2;
		LPDIRECTDRAWSURFACE3 lpSurf3;
		LPDIRECTDRAWSURFACE4 lpSurf4;
		LPDIRECTDRAWSURFACE7 lpSurf7;
		
		Device_HookVMT( lpSurf );
		if( (hRet = lpSurf->QueryInterface( IID_IDirectDrawSurface2, (LPVOID*)&lpSurf2 )) == DD_OK ) {
			Device_HookVMT( lpSurf2 );
			lpSurf2->Release();
		} else {
			warning( "WARNING: Could not create IDirectDrawSurface2 object (HRES=0x%x)", hRet );
		}
		if( (hRet = lpSurf->QueryInterface( IID_IDirectDrawSurface3, (LPVOID*)&lpSurf3 )) == DD_OK ) {
			Device_HookVMT( lpSurf3 );
			lpSurf3->Release();
		} else {
			warning( "WARNING: Could not create IDirectDrawSurface3 object (HRES=0x%x)", hRet );
		}
		if( (hRet = lpSurf->QueryInterface( IID_IDirectDrawSurface4, (LPVOID*)&lpSurf4 )) == DD_OK ) {
			
			DDSCAPS2 ddsc = {0};
			lpSurf4->GetCaps( &ddsc );
			if( ddsc.dwCaps2 & DDCAPS2_COLORCONTROLPRIMARY ) {
				LPDIRECTDRAWCOLORCONTROL lpDDCC;
				hRet = lpSurf4->QueryInterface( IID_IDirectDrawColorControl, (LPVOID*)&lpDDCC );
				if( hRet == DD_OK ) {
					Device_HookVMT( lpDDCC );
					lpDDCC->Release();
				} else {
					warning( "Could not create a ColorControl (HRES=0x%x)", hRet );
				}
			} else {
				warning( "No ColorControl supported on primary surface :(" );
			}

			Device_HookVMT( lpSurf4 );
			lpSurf4->Release();
		} else {
			warning( "WARNING: Could not create IDirectDrawSurface4 object (HRES=0x%x)", hRet );
		}
		if( (hRet = lpSurf->QueryInterface( IID_IDirectDrawSurface7, (LPVOID*)&lpSurf7 )) == DD_OK ) {
			Device_HookVMT( lpSurf7 );
			lpSurf7->Release();
		} else {
			warning( "WARNING: Could not create IDirectDrawSurface7 object (HRES=0x%x)", hRet );
		}

		LPDIRECTDRAWGAMMACONTROL lpDDGC;
		if( (hRet = lpSurf->QueryInterface( IID_IDirectDrawGammaControl, (LPVOID*)&lpDDGC )) == DD_OK ) {
			Device_HookVMT( lpDDGC );
			lpDDGC->Release();
		} else {
			warning( "Could not create a GammaControl (HRES=0x%x)", hRet );
		}

	} else {
		warning( "WARNING: Could not create a IDirectDrawSurface object (HRES=0x%x)", hRet );
	}
}

void HookMisc( LPDIRECTDRAW lpDD )
{
	HRESULT hRet;
	LPDIRECTDRAWCLIPPER lpClip = 0;
	if( (hRet = lpDD->CreateClipper( NULL, &lpClip, NULL )) == DD_OK ) {
		Device_HookVMT( lpClip );
		lpClip->Release();
	} else {
		warning( "Could not create a Clipper (HRES=0x%x)", hRet );
	}
	LPDIRECTDRAWPALETTE lpPal = 0;
	PALETTEENTRY m_pal[2] = {{0,0,0,PC_NOCOLLAPSE},{255,255,255,PC_NOCOLLAPSE}};
	if( (hRet = lpDD->CreatePalette( DDPCAPS_1BIT, m_pal, &lpPal, NULL )) == DD_OK ) {
		Device_HookVMT( lpPal );
		lpPal->Release();
	} else {
		warning( "Could not create a Pallette (HRES=0x%x)", hRet );
	}
}

void HookDevices( LPDIRECTDRAW lpDD )
{
	HRESULT hRet;
	LPDIRECTDRAW2 lpDD2;
	LPDIRECTDRAW4 lpDD4;
	LPDIRECTDRAW7 lpDD7;

	Device_HookVMT( lpDD );


	if( (hRet = lpDD->QueryInterface( IID_IDirectDraw2, (LPVOID*)&lpDD2 )) == DD_OK ) {
		Device_HookVMT( lpDD2 );
		lpDD2->Release();
	} else {
		warning( "WARNING: Could not create IDirectDraw2 object (HRES=0x%x)", hRet );
	}
	if( (hRet = lpDD->QueryInterface( IID_IDirectDraw4, (LPVOID*)&lpDD4 )) == DD_OK ) {
		Device_HookVMT( lpDD4 );
		lpDD4->Release();
	} else {
		warning( "WARNING: Could not create IDirectDraw4 object (HRES=0x%x)", hRet );
	}
	if( (hRet = lpDD->QueryInterface( IID_IDirectDraw7, (LPVOID*)&lpDD7 )) == DD_OK ) {
		Device_HookVMT( lpDD7 );
		lpDD7->Release();
	} else {
		warning( "WARNING: Could not create IDirectDraw7 object (HRES=0x%x)", hRet );
	}

	HookSurfaces( lpDD );
	HookMisc( lpDD );
}


#undef ADDHOOK
#define ADDHOOK(x)	*(void**)&o##x = (void*)DetourFunction( (PBYTE)GetProcAddress(hDDraw,#x), (PBYTE)my##x ); \
	if( !o##x ) warning( "Failed hooking "#x );




HRESULT (WINAPI* oDirectDrawCreate)( GUID *lpGUID, LPDIRECTDRAW *lplpDD, IUnknown *pUnkOuter ) = 0;
HRESULT WINAPI myDirectDrawCreate( GUID *lpGUID, LPDIRECTDRAW *lplpDD, IUnknown *pUnkOuter )
{
	HRESULT hRet = oDirectDrawCreate( lpGUID, lplpDD, pUnkOuter );
	char buf[100] = "{NULLPTR}";
	if( lpGUID ) FormatGUID( *lpGUID, buf, 100 );
	add_log( "DirectDrawCreate( %s, 0x%x, ) = 0x%x", buf, (lplpDD?*lplpDD:0), hRet );
	return hRet;
}

HRESULT (WINAPI* oDirectDrawCreateEx)( GUID * lpGuid, LPVOID *lplpDD, REFIID iid, IUnknown *pUnkOuter ) = 0;
HRESULT WINAPI myDirectDrawCreateEx( GUID * lpGuid, LPVOID *lplpDD, REFIID iid, IUnknown *pUnkOuter )
{
	HRESULT hRet = oDirectDrawCreateEx( lpGuid, lplpDD, iid, pUnkOuter );
	char buf[100] = "{NULLPTR}";
	if( lpGuid ) FormatGUID( *lpGuid, buf, 100 );
	add_log( "DirectDrawCreateEx( %s, 0x%x, , ) = 0x%x", buf, (lplpDD?*lplpDD:0), hRet );
	return hRet;
}

HRESULT (WINAPI* oDirectDrawCreateClipper)( DWORD dwFlags, LPDIRECTDRAWCLIPPER *lplpDDClipper, IUnknown *pUnkOuter ) = 0;
HRESULT WINAPI myDirectDrawCreateClipper( DWORD dwFlags, LPDIRECTDRAWCLIPPER *lplpDDClipper, IUnknown *pUnkOuter )
{
	HRESULT hRet = oDirectDrawCreateClipper( dwFlags, lplpDDClipper, pUnkOuter );
	add_log( "DirectDrawCreateClipper( 0x%x, 0x%x, ) = 0x%x", dwFlags, (lplpDDClipper?*lplpDDClipper:0), hRet );
	return hRet;
}

HRESULT (WINAPI* real_DirectDrawCreate)( GUID FAR *lpGUID, LPDIRECTDRAW FAR *lplpDD, IUnknown FAR *pUnkOuter ) = 0;

void init_ddhooks()
{
	if( real_DirectDrawCreate ) return;
	HMODULE hDDraw = LoadLibrary( TEXT("ddraw.dll") );
	if( !hDDraw ) {
		warning( "ERROR: Could not load ddraw.dll (LastErr=0x%x)", GetLastError() );
		return;
	}
	*(FARPROC*)&real_DirectDrawCreate = GetProcAddress( hDDraw, "DirectDrawCreate" );

	LPDIRECTDRAW lpDD = 0;
	HRESULT hRet = real_DirectDrawCreate( NULL, &lpDD, NULL );
	if( hRet == DD_OK ) {
		HookDevices( lpDD );
		lpDD->Release();
	} else {
		warning( "ERROR: Could not create a IDirectDraw object (HRES=0x%x)", hRet );
	}

	ADDHOOK(DirectDrawCreate);
	ADDHOOK(DirectDrawCreateEx);
	ADDHOOK(DirectDrawCreateClipper);
}